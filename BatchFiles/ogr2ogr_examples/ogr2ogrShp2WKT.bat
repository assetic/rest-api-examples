echo off
rem Create WKT import file from shp file with polygons
rem Will reproject/transform to EPSG:4326 - Geography data
rem Will get polygon centroid and include in export
rem Uses ogr2ogr and SQLite (https://www.gdal.org/ogr_sql_sqlite.html)

rem make sure GDAL library is set and path to ogr2ogr
set GDAL_DATA=C:\Program Files\QGIS 2.18\share\gdal
set OGR_PATH=C:\Program Files\QGIS 2.18\bin

rem directory and filename to write data exchange import file to.  Must exist.
set output_dir=c:\temp\wkt_files
set output_wkt_file=playground_import.csv

rem set the following source information.  SRS are the EPSG codes for projection
set input_shp_file=C:\Projects\ShapeFiles\Playground_Area.shp
set asset_id_field=asset_id
set input_srs=28354

rem output projection needs to be 4326
set output_srs=4326

rem build some variables
for %%f in (%input_shp_file%) do set input_table=%%~nf
set pwd=%cd%
echo on
rem step 1. change directory to the output directory
cd %output_dir%

rem step 2. create temp file with polygon centroid as a string, wont be reprojected
"%OGR_PATH%\ogr2ogr.exe" "tmp.csv" -s_srs EPSG:%input_srs% -t_srs EPSG:%output_srs% -sql "SELECT geometry, ST_AsText(ST_Centroid(geometry)) as point_str, %asset_id_field% FROM %input_table%" -dialect sqlite -f CSV %input_shp_file% -lco GEOMETRY=AS_WKT

rem step 3. read temp file and reproject centroid, and set column names
"%OGR_PATH%\ogr2ogr.exe" %output_wkt_file% -s_srs EPSG:%input_srs% -t_srs EPSG:%output_srs% -sql "select ST_PointFromText(point_str, %input_srs%), ST_AsText(geometry) as Polygon, %asset_id_field% as [Complex Asset Id] FROM tmp" -dialect sqlite -f CSV "tmp.csv" -lco GEOMETRY=AS_WKT -lco GEOMETRY_NAME=Point

rem step 4. remove temp file
del tmp.csv

rem strp 5. change directory back to starting directory
cd %pwd%

pause