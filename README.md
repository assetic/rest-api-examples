# README #


### What is this repository for? ###

Sample scripts to utilise Assetic API's for integration purposes
Scripts provided in Python and C#.

These scripts current use the Assetic API's created for the Assetic front end.

Currently using Python 2.7 & 3.5

Currently using .Net 4.0 for c#

### How do I get set up? ###

Need Python and libraries used by these scripts.
Some libraries may need to be downloaded and installed.

### Who do I talk to? ###

Kevin Wilton - kwilton@assetic.com