﻿using System;
using System.Configuration;
using Assetic.RESTapi.SDK.Client;

namespace AsseticSamples
{
    /// <summary>
    /// A collection of functions to initialise the Assetic.RESTapi.SDK and allow
    /// user to execute once of the API samples
    /// </summary>
    class Program
    {
        /// <summary>
        /// Gets or Sets Assetic.RESTapi.SDK.Client.Configuration
        /// </summary>
        static public Assetic.RESTapi.SDK.Client.Configuration asseticConfiguration { get; set; }

        /// <summary>
        /// Gets or Sets the URI of the target Assetic environment
        /// </summary>
        public static string asseticURI { get; private set; }

        /// <summary>
        /// Gets or Sets the Assetic login name used to authenticate to Assetic
        /// </summary>
        public static string asseticUserName { get; private set; }

        /// <summary>
        /// Gets or Sets the Assetic authorisation token for the given user name 
        /// </summary>
        public static string asseticUserToken { get; private set; }

        /// <summary>
        /// Main function initialised when cosole application is started
        /// </summary>
        /// <param></param>
        /// <returns>0 if success</returns>
        static void Main(string[] args)
        {
            //read the config file to get and apply settings
            ApplySettings();
            //initialise the Assetic SDK client
            SetupAsseticClient();

            Console.WriteLine("Enter the number corresponding to the API to run:");
            Console.WriteLine("1. Get Assetic Version");
            Console.WriteLine("2. Get Asset");
            int iSelection;
            if (int.TryParse(Console.ReadLine(), out iSelection))
            {
                switch (iSelection)
                {
                    case 1:
                        {
                            //get the current version as a string
                            var version = AsseticSamples.Version.GetVersion();
                            Console.WriteLine(version);
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Enter Asset Id");
                            string assetId = Console.ReadLine();
                            var asset = AsseticSamples.Asset.GetAsset(assetId);
                            Console.WriteLine(asset);
                            break;
                        }
                    default:
                        break;
                }


            }
            else
            {
                //user gave an illegal input. Handle it here.
                Console.WriteLine("Invalid selection.");
                Environment.Exit(-1);
            }


        }
        static bool SetupAsseticClient()
        {
            var client = new ApiClient(asseticURI);
            asseticConfiguration = new Assetic.RESTapi.SDK.Client.Configuration();
            asseticConfiguration.Username = asseticUserName;
            asseticConfiguration.Password = asseticUserToken;
            client.Configuration = asseticConfiguration;
            return true;
        }
        static bool ApplySettings()
        {
            var appSettings = ConfigurationManager.AppSettings;
            try
            {
                if (asseticURI == null) { asseticURI = appSettings["AsseticURI"]; }
                if (asseticUserName == null) { asseticUserName = appSettings["AsseticUserName"]; }
                if (asseticUserToken == null) { asseticUserToken = appSettings["AsseticUserToken"]; }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading settings: " + ex.InnerException);
                Environment.Exit(-1);
            }
            return true;
        }
    }
}
