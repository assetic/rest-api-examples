﻿using System;
using Assetic.RESTapi.SDK.Api;
using Assetic.RESTapi.SDK.Client;
using Assetic.RESTapi.SDK.Model;
using System.Collections.Generic;

namespace AsseticSamples
{
    /// <summary>
    /// A collection of methods to operate on Assets
    /// </summary>
    public class Asset
    {
        /// <summary>
        /// For a given asset ID, get the asset details
        /// </summary>
        /// <param name="assetId">Assetic Asset Id</param>
        /// <returns>name of asset</returns>
        public static string GetAsset(string assetId)
        {
            var assetAPI = new AssetApi(Program.asseticConfiguration);

            //illustrate how to include non-core attributes in the response 
            var attributes = new List<string>();
            attributes.Add("Zone");

            var asset = new Assetic3IntegrationRepresentationsComplexAssetRepresentation();
            try
            {
                asset = assetAPI.AssetGet(assetId, attributes);
            }
            catch (ApiException ex)
            {
                var err = ex.ToString();
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
            }

            string strAsset = asset.AssetName;
            return strAsset;
        }
    }
}
