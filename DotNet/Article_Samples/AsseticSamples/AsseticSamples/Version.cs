﻿using System;
using Assetic.RESTapi.SDK.Api;
using Assetic.RESTapi.SDK.Client;
using Assetic.RESTapi.SDK.Model;

namespace AsseticSamples
{
    /// <summary>
    /// A collection of methods to operate on the Version API
    /// </summary>
    public class Version
    {
        /// <summary>
        /// Get the current Assetic version
        /// </summary>
        /// <returns>Assetic version</returns>
        public static string GetVersion()
        {
            var versionAPI = new VersionApi(Program.asseticConfiguration);
            SystemVersion version;
            try
            {
                version = versionAPI.VersionGet();
            }
            catch (ApiException ex)
            {
                var err = ex.ToString();
                return null;
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
                return null;
            }

            string strVersion = version.Major.ToString() + '.' + version.Minor.ToString() + '.' + version.Build.ToString() + '.' + version.Revision.ToString();
            return strVersion;
        }

    }
}
