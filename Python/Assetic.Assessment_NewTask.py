"""
    Example script to create an assessment task
    (Assetic.Assessment_NewTask.py)

    1. create (POST) a new task agaisnt an asset for a specified form.
"""
import assetic
from datetime import datetime
from datetime import timedelta
asseticsdk = assetic.AsseticSDK(None,None,'Debug')

##need some identifiers. These may come from GET /api/v2/assessmenttask
##which is in the AssessmentTaskApi, use method assessment_task_get which
##returns all tasks assigned to the logged in user
taskguid = "375f575c-2b6b-e611-9469-06edd62954d7"
formguid = "e3244b32-286b-e611-9469-06edd62954d7"
resourceguid = "36bee8ab-4e39-4f1d-b713-4eefa521fcd8"
projectid = "9ab5c27b-296b-e611-9469-06edd62954d7"
assetguid = "43dc0aa4-3e15-4368-a50e-4de9b8edb82d"
assettype = "ComplexAsset"
assessmentlevel = "SelectedAsset"
task_id = "4556443j"
location_wkt = "POINT (145.7793239 -38.2715024)"
task_name = "CRM number"
comments = "CRM description & contact details?"
##create an api instance
assesstaskapi = assetic.AssessmentTaskApi()

#define the assessor resource (need both guid & name)
resource = assetic.Assetic3IntegrationRepresentationsRsResourceRepresentation()
resource.id = resourceguid
resource.display_name = 'Kevin Wilton'

#build the object to create the task
##specify form to use
taskform = assetic.Assetic3IntegrationRepresentationsAsmtTaskFormRepresentation()
taskform.form_id = formguid
taskform.maximum = 1
taskform.minimum = 1
##specify asset to use
taskasset = assetic.Assetic3IntegrationRepresentationsAsmtTaskObjectRepresentation()
taskasset.id = assetguid
taskasset.type = assettype
taskasset.location_wkt = location_wkt
#finalise task
task = assetic.Assetic3IntegrationRepresentationsAsmtTaskRepresentation()
task.asmt_project_id = projectid
task.asmt_task_forms = [taskform]
task.assessment_level = assessmentlevel
task.asset_object = taskasset
task.comments = comments
task.end_date = datetime.isoformat(datetime.now() + timedelta(days=+10))
task.name = task_name
task.priority = None
task.rs_resource_id_assigned_to = resource
task.start_date = datetime.isoformat(datetime.now())
task.status = "Open"
task.task_type_id = 1
task.task_id = task_id
##execute the post
try:
    response = assesstaskapi.assessment_task_post(task)
except assetic.rest.ApiException as e:
    ##Log exception and exit
    asseticsdk.logger.error("Status {0}, Reason: {1}{2}".format(
        e.status,e.reason,e.body))
    exit();    
##check for error messages
if response.get('Errors') != None:
    print(response.get('Errors'))
    exit();

taskid = response['Data'][0]['Id']
print(taskid)
taskdata = response["Data"][0]
taskdata["Status"] = "Completed"
#taskdata["AssetObject"]["Id"] = "c74cca42-71a5-e611-946c-06edd62954d7"

##execute the put
try:
    response = assesstaskapi.assessment_task_put(taskid,taskdata)
except assetic.rest.ApiException as e:
    ##Log exception and exit
    asseticsdk.logger.error("Status {0}, Reason: {1}{2}".format(
        e.status,e.reason,e.body))
    exit();    
##check for error messages
print(response)


