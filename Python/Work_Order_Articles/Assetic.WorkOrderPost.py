"""
    Example script to create work order (Assetic.WorkOrderPost.py)
"""
import assetic


# create an authentication connection with the Assetic environment
asseticsdk = assetic.AsseticSDK(None, None, 'Info')
asseticsdk.logger_format = "%(asctime)s %(name)-12s %(levelname)-8s %(message)s"

# create the API connection to the maintenance work order endpoints
work_order_api = assetic.WorkOrderApi()

# create the Work Order model object
maint_wko = assetic.models.MaintenanceWorkOrder()
maint_wko.status = "INPRG"
maint_wko.brief_description = "Potholes in Sussex Street"
maint_wko.priority_id = 2
maint_wko.estimated_duration = 3
maint_wko.location_description = "20, Sussex Street, Glenorchy, TAS, 7010, Australia"
maint_wko.point_string = "147.26314791653294 -42.832066858923795"
maint_wko.creator_id = "36bee8ab-4e39-4f1d-b713-4eefa521fcd8"
maint_wko.requestor_id = "8d05ef3b-5da4-e511-9451-06edd62954d7"
 
# Specify the work type to the Work Order
wko_type = assetic.models.WorkType()
wko_type.id = 6
maint_wko.work_order_type = wko_type
 
# based on work type requirements, specify the FCR codes
maint_wko.failure_sub_code_id = 1
maint_wko.cause_sub_code_id = 1
maint_wko.remedy_code_id = 2

# assign an Assetic asset by referencing its internal ID (GUID)
maint_wko.asset_id = "6216b184-62b0-e611-946c-06edd62954d7"
maint_wko.work_order_work_group = "Indoor Crew 1"
 
# add 1 (or more) supporting information comments as a list to the work order
suppinfolist = list()
suppinfo = assetic.models.SupportingInformation()
suppinfo.description = "Long description 1 goes here."
suppinfolist.append(suppinfo)

suppinfo = assetic.models.SupportingInformation()
suppinfo.description = "Long description 2 goes here."
suppinfolist.append(suppinfo)
maint_wko.supporting_information = suppinfolist
 
# add a labour item to the work order
labour_list = list()
labour = assetic.models.MaintenanceLabour()
labour.planned_group_craft_id = "5379a3df-feb1-e611-946c-06edd62954d7"
labour.quantity_required = 1
labour.hours_required = 3

# create a maintenance resource object for the maintenance resource properties
maint_resource_list = list()
maint_resource = assetic.models.MaintenanceResource()

# specify an Assetic resource entity to be assigned to the maintenance resourcelabour
resource = assetic.models.ResourceRepresentation()
resource.id = "66554e3b-ed60-4036-8552-1cdac380fffb"
maint_resource.resource = resource

# set the resources work status (assign/completed), which labor they are assigned to, etc
maint_resource.status_id = 1
maint_resource.assigned_group_craft_id = "5379a3df-feb1-e611-946c-06edd62954d7"
# maint_resource.actual_start = "2017-11-19T16:04:56"
# maint_resource.actual_finish = "2017-11-20T16:04:56"
maint_resource_list.append(maint_resource)
labour.maintenance_resources = maint_resource_list
labour_list.append(labour)
maint_wko.labours = labour_list
 
# Add target and scheduling dates to the work order
schedule = assetic.models.MaintenanceScheduling()
schedule.target_start = "2024-08-04T09:04:56"
schedule.target_finish = "2024-08-09T12:04:56"
schedule.scheduled_start = "2024-08-05T00:00:00"
schedule.scheduled_finish = "2024-08-09T00:00:00"

# if creating a work order that is in a completed state, set the actual starts and finishes below
# schedule.actual_start = "2017-11-19T16:04:56"
# schedule.actual_finish = "2017-11-20T16:04:56"

maint_wko.scheduling = schedule
 
# add materials to the work order
materials_list = list()
material = assetic.models.MaintenanceMaterial()
material.bill_of_material_id = "2b4f13ce-673d-41da-ac72-74f05460fab5"
material.is_material_available = True
material.planned_quantity = 1
materials_list.append(material)
maint_wko.maintenance_materials = materials_list
 
# add any services to work order
services_list = list()
service = assetic.models.MaintenanceService()
service.service_activity_id = "ab08efe6-b41e-4e60-8f7b-9ab4daaf6ecc"
service.description = "Pothole repair"
# service.unit_type = "Each"
service.planned_quantity = 2
service.planned_cost = 100
service.actual_quantity = 5
service.actual_unit_rate = 15
services_list.append(service)
maint_wko.maintenance_services = services_list
 
# add a work task related to the work order labor item
tasks_list = list()
task = assetic.models.MaintenanceWorkTask()
task.friendly_id = 10
task.brief_description = "Task to patch pothole"
task.status = 5
tasks_list.append(task)

# add an asessment work task to the work order with an attached custom assessment form
task = assetic.models.MaintenanceWorkTask()
task.friendly_id = 20
task.brief_description = "Assessment task with checklist following repair"
task.status = 5
task.type_id = 201
task_forms = list()
form = assetic.models.AsmtTaskFormRepresentation()
form.form_id = "71806f28-d1ee-42e6-a1d2-40f6f955c54d"
form.form_result_count = 0
form.minimum = 1
form.maximum = 1
form.order = 0
task_forms.append(form)
task.asmt_task_forms = task_forms
tasks_list.append(task)

maint_wko.work_tasks = tasks_list

# initiate the request to the Assetic REST API
try:
    response = work_order_api.work_order_integration_api_post(maint_wko)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error('Status {0}, Reason: {1}'.format(e.status,e.reason))
except Exception as e:
    asseticsdk.logger.error(str(e))
else:
    asseticsdk.logger.info(str(response))
