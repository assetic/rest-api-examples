"""
Get a list of assets (Assetic.AssetsGet.py)
Illustrate the use of get asset by GUID and by asset ID
by using the last returned asset from the initial get request
"""
import assetic
#Define location of configuration file, log file, log level
asseticsdk=assetic.AsseticSDK("C:/users/kwilton/assetic.ini",
                              None,"Debug")
assetapi = assetic.ComplexAssetApi()

#api will always return a predefined set of core fields
#define additional asset attribute fields, must define at least one
#attribute field names are the internal field names, get using metadata api
attributes = list()
attributes.append("Zone")
attributes.append("Locality")

##define page size (no of records) and page number to get
pagesize = 5
#searchfilter = ["AssetSubType='Roads'","AssetType='200mm'"]
searchfilter = "AssetSubType='Roads'"
sortorder = "AssetName-desc"
kwargs = {"request_params_page":1,
          "request_params_page_size":pagesize,
          "request_params_sorts":sortorder,
          "request_params_filters":searchfilter}

##now execute the request
try:
    response = assetapi.complex_asset_get_0(attributes,**kwargs)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()

if response == None:
    asseticsdk.logger.info("Empty response")
    exit()

totalresults = response.get("TotalResults")
if totalresults < 1:
    asseticsdk.logger.info("Empty response")
    exit()

##display the returned asset data
assets = response.get("ResourceList")
for asset in assets:
    assetid = asset.get("AssetId")
    assetguid = asset.get("Id")
    assetname = asset.get("AssetName")
    ##additional attributes returned in "Attributes" array
    attributes = asset.get("Attributes")
    zone = attributes.get("Zone")
    locality = attributes.get("Locality")
    
    msg = "guid: {0}, asset {1}, name {2}, zone {3}, locality {4}".format(
        assetguid,assetid,assetname,zone,locality)
    asseticsdk.logger.info(msg)

##now get the last asset by specifying asset guid
try:
    response = assetapi.complex_asset_get(assetguid,["Zone","Locality"])
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()
if response == None:
    asseticsdk.logger.info("Empty response")
    exit()

#display the returned info
assetid = asset.get("AssetId")
assetguid = asset.get("Id")
assetname = asset.get("AssetName")
attributes = asset.get("Attributes")
zone = attributes.get("Zone")
locality = attributes.get("Locality")
asseticsdk.logger.info("Now get the last asset in the list by asset guid")
msg = "guid: {0}, asset {1}, name {2}, zone {3}, locality {4}".format(
    assetguid,assetid,assetname,zone,locality)
asseticsdk.logger.info(msg)

##get the last asset by specifying user friendly asset id
try:
    response = assetapi.complex_asset_get(assetid,["Zone","Locality"])
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()
if response == None:
    asseticsdk.logger.info("Empty response")
    exit()

#display the returned info
assetid = asset.get("AssetId")
assetguid = asset.get("Id")
assetname = asset.get("AssetName")
attributes = asset.get("Attributes")
zone = attributes.get("Zone")
locality = attributes.get("Locality")
msg = "Now get the last asset in the list by user friendly asset ID"
asseticsdk.logger.info(msg)
msg = "guid: {0}, asset {1}, name {2}, zone {3}, locality {4}".format(
    assetguid,assetid,assetname,zone,locality)
asseticsdk.logger.info(msg)

