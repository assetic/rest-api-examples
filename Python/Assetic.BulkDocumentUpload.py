"""
    Script to automate document/photo upload in bulk fashion (Assetic.BulkDocumentUpload.py)
    Requirements:
    1. Create a folder with following sub-folders: ToLoad, Processed, Error and a excel file named as DataToImport.xlsx
    2. Update assetic.ini file's [bulk_document_upload] value with location of folder created in Step: 1
    3. Excel file named DataToImport.xlsx MUST have following columns in exact order
    ComplexAssetId | FileName | DocumentGroupId | DocumentCategoryId | DocumentSubCatgoryId | IsKeyPhoto

    4. Load/move all document/photos you wish to upload in ToLoad directory of folder defined in Step: 1
    5. Run and Oberserve script output

    Output of this script explained:
    1. Document upload requests which are processed/uploaded successfully are moved to Processed directory of folder defined in Step: 1
    2. Document upload requested in error state are moved to Error directory of folder defined in Step: 1
"""

import assetic
import os
import shutil
import time
import sys
import base64
import openpyxl #to read excel files

try:    
    import configparser as configparser
except:
    try:
        #V2 python?
        import ConfigParser as configparser
    except:
        print ("Unable to load configparser module")
        sys.exit()
        
# Read ini
settingsRelativePath = os.path.join(os.path.dirname(__file__), 'assetic.ini')
settings = configparser.ConfigParser()
settings.read(settingsRelativePath)

# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
conf.bulk_document_upload_init_directory = settings.get('bulk_document_upload', 'init_directory')
conf.bulk_dataexchange_init_directory = settings.get('bulk_data_exchange', 'init_directory')
auth = conf.get_basic_auth_token()

#Debug settings
conf.logger_file = conf.bulk_document_upload_init_directory + '//BulkDocumentImportLog.txt'
conf.debug = True

client = assetic.ApiClient(conf.host,"Authorization",auth)
docapi = assetic.DocumentApi(client)
dataexchangeapi = assetic.DataExchangeJobApi(client)

excelFileToImport = conf.bulk_document_upload_init_directory + "//Documents_Photos_DataToImport.xlsx"

wb = openpyxl.load_workbook(excelFileToImport)
sheet = wb.active
sheet['D' + str(1)] = 'Migrated Document Id'
sheet['E' + str(1)] = 'Operation Performed On'
sheet['F' + str(1)] = 'Status'
wb.save(excelFileToImport)

#sheet = wb.get_sheet_by_name('Population by Census Tract')
##for row in sheet.iter_rows(row_offset = 1):    
##    for cell in row:
##        print 'Column: {0} Value: {1}'.format(cell.coordinate, cell.value)

for row in range(2, sheet.max_row + 1):
    complexAssetId  = sheet['A' + str(row)].value
##    documentGroupId = sheet['B' + str(row)].value
##    documentCategoryId = sheet['C' + str(row)].value
##    documentSubCategoryId   = sheet['D' + str(row)].value
            
    fileToUploadFullPath   = sheet['B' + str(row)].value
    isKeyPhoto = sheet['C' + str(row)].value
    
    print ('Processing row entry: {2}(from excel file); Complex Asset Id: {0}; File path: {1}'.format(complexAssetId, fileToUploadFullPath, row))

    if (os.path.exists(fileToUploadFullPath)):
        #fileExtention = fileToUploadFullPath[fileToUploadFullPath.rindex('.') + 1:]
        fileExtention = os.path.splitext(fileToUploadFullPath)[1][1:]
        with open(fileToUploadFullPath, "rb") as f:
            filename = f.name        
            data = f.read()
            
            if sys.version_info < (3,0):
                filecontents = data.encode("base64")
            else:
                filecontents = base64.b64encode(data)
                filecontents = filecontents.decode(encoding="utf-8", errors="strict")
                            
            filesize = os.path.getsize(fileToUploadFullPath)
            print ('Reading File name: {0}; File Size: {2}; Extracted file extention: {1}'.format(filename, fileExtention, filesize))

            validImageExtensions = {".jpg", ".png", ".gif"} 
            isFileAnImage = any(filename.endswith(ext) for ext in validImageExtensions)
            documentGroupId = 7 if isFileAnImage == True else 10            

            file_properties = Assetic3WebApiModelsFilePropertiesVM()
            file_properties.name = filename
            file_properties.file_size = filesize
            file_properties.mimetype = fileExtention
            file_properties.filecontent = filecontents
            filearray = [file_properties]

            document = Assetic3IntegrationApiRepresentationsDocumentRepresentation()
            document.key_photo = isKeyPhoto
            document.doc_group_id = documentGroupId
##            document.doc_sub_category_id = documentSubCategoryId
##            document.doc_category_id = documentCategoryId            
            document.parent_type = 'ComplexAsset'
            document.parent_id = complexAssetId            
            document.label = 'Importing complex asset document/photo - ' + filename + ' on ' + time.strftime("%d/%m/%Y %H:%M:%S")
            
            document.file_property = filearray
            document.mime_type = fileExtention
        
            doc = docapi.document_post(document)
            docid = doc[0].get('Id')
            print 'Processed document/photo upload request; Response received: [{0}]'.format(docid)                     
            
            isDocumentUploadSuccessful = True if docid != '' else False
            if isDocumentUploadSuccessful:                
                sheet['D' + str(row)] = docid
                sheet['E' + str(row)] = time.strftime("%c")
                sheet['F' + str(row)] = 'SUCCESS'
            else:
                sheet['D' + str(row)] = 'N/A'
                sheet['E' + str(row)] = time.strftime("%c")
                sheet['F' + str(row)] = 'Error; Please check integration application logs for details'
                    
            wb.save(excelFileToImport)
    else:
        print 'File: [{0}] is non-existing\n'.format(fileToUploadFullPath)  
        


    
    
    
