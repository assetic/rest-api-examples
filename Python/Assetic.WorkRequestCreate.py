"""
    Example script to create a request (Assetic.WorkRequestCreate.py)
    Creates a customer request in Assetic
"""
import assetic

##Assetic SDK instance.
asseticsdk = assetic.AsseticSDK("c:/users/kwilton/assetic.ini",None,"Info")
##work request API
wrapi = assetic.WorkRequestApi()

#create object for work request
wr = assetic.Assetic3IntegrationRepresentationsWorkRequest()
wr.external_identifier = "12345"
wr.work_request_source_id = 3   #3="External CRM"
wr.work_request_priority_id = 1
wr.location = "Outside 7-Eleven Store"
wr.feedback_method_id = 1
wr.feedback_required = 0
wr.description = "Sample API request generation - please fix pothole"

##Assign asset to work request using asset ID
wr.asset_id = "AP01"

##create address object and fill in address detail
wraddr = assetic.Assetic3IntegrationRepresentationsCustomAddress()
wraddr.street_number = "257"
wraddr.street_address = "Collins Street"
wraddr.city_suburb = "Melbourne"
wraddr.state = "Victoria"
wraddr.zip_postcode = "3000"
wraddr.country = "Australia"

##create object for location which holds address and assign to work request
wrfl = assetic.Assetic3IntegrationRepresentationsWorkRequestPhysicalLocation()
wrfl.address = wraddr
wr.work_request_physical_location = wrfl

##create object for spatial location and assign to work request object
wrsl = assetic.Assetic3IntegrationRepresentationsWorkRequestSpatialLocation()
wrsl.point_string = "POINT (145.247206 -37.857442)"
wr.work_request_spatial_location = wrsl

#create object for person that lodged the request.  Set first & family name
wrreq = assetic.Assetic3IntegrationRepresentationsRequestorRepresentation()
wrreq.first_name = "Miles"
wrreq.surname = "Moncrief"
##assign requestor to work request
wr.requestor = wrreq

##Now create the work request
try:
    wrguid = wrapi.work_request_post(wr)    #returns guid of work request
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()

if wrguid == None:
    asseticsdk.logger.warning("No work request GUID returned")
    exit()
##now get request back so we can see the generated user friendly Request ID
try:
    wrget = wrapi.work_request_get(wrguid)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()
asseticsdk.logger.info("Work Request GUID: {0}, Friendly ID {1}".format(
    wrguid,wrget.get("FriendlyIdStr")))
