"""
Update an asset (Assetic.AssetsUpdate.py)
"""
import assetic
#Define location of configuration file, log file, log level
asseticsdk=assetic.AsseticSDK("C:/users/kwilton/assetic.ini",
                              None,"Debug")
assetapi = assetic.AssetTools()

##create instance of asset object
asset = assetic.Assetic3IntegrationRepresentationsComplexAssetRepresentation()
##make the changes
#asset.asset_id = 'RINT05'
asset.id = "3a2b0c41-2ce1-e611-946c-06edd62954d7"
asset.asset_maintenance_type = "Road"
asset.asset_maintenance_sub_type = "Arterial Road"
#asset.asset_maintenance_type = "Local Road"
#asset.asset_maintenance_sub_type = "Sealed"
#asset.asset_work_group = "RMPC"
##"Zone":"East",
attributes = {"Comment":"a new comment","Link":"www.assetic.com"}
asset.attributes = attributes
##now execute the update
try:
    response = assetapi.update_complex_asset(asset)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()
print(response)
