"""
Assetic.BulkExportFinalise.py

Finalises the bulk export process.

When this py script is run it checks to see if there are any outstanding bulk
exports awaiting download.  This is indicated by document links in Assetic that
follow a specific structure
The Assetic API's are used to check if the export is ready for download and if
so, the data is downloaded to csv.
Full documentation: https://assetic.zendesk.com/knowledge/articles/360000716655
Author: Kevin Wilton (Assetic)
"""
import assetic
import os
# get current working dir.  Will reset at end
cwd = os.getcwd()
# set output folder location by changing working dir
os.chdir(r'c:\temp\export_to_csv')

inifile = None
asseticsdk=assetic.AsseticSDK(inifile, None, "Info")
sync = assetic.SyncToLocalProcesses()
# run process to finalise the exports
sync.finalise_export_task_to_file(True)
# reset working directory
os.chdir(cwd)
