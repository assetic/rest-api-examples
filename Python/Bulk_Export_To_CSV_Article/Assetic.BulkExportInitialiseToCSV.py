"""
Assetic.BulkExportInitialiseToCSV.py

Initialises the bulk export process to CSV.
Define a set of search profiles to be exported
When this py script is run it uses the Assetic 'export all' api to initiate the
export, which uses the Assetic background worker to queue and execute the export
A second 'finalise' step run via a separate py script to check to see if the
export is complete, and if so downloads the exported data
Full documentation: https://assetic.zendesk.com/knowledge/articles/360000716655
Author: Kevin Wilton (Assetic)
"""
import assetic
import os
# get current working dir.  Will reset at end
cwd = os.getcwd()
# set output folder location by changing working dir
os.chdir(r'c:\temp\export_to_csv')
# Define location of configuration file, log file, log level
inifile = None
asseticsdk=assetic.AsseticSDK(inifile, None, "Info")

sync = assetic.SyncToLocalProcesses()
profiles = list()
# Profile for 'Assets Sync'
profilemapping = assetic.SearchProfileRepresentation()
profilemapping.profileguid = "d9a350d7-8078-e611-946c-06edd62954d7"
profilemapping.profilename = "Water Pressure Pipes"
profilemapping.keyfield = "[Asset Id]"
profilemapping.background_export = True
profiles.append(profilemapping)

# Profile for 'Component Sync'
profilemapping = assetic.SearchProfileRepresentation()
profilemapping.profileguid = "65ffd995-4e76-e611-946c-06edd62954d7"
profilemapping.profilename = "All Components"
profilemapping.keyfield = "[Component Id]"
profilemapping.background_export = False
profiles.append(profilemapping)

# Initialise the exports.
for profile in profiles:
    chk = sync.initiate_export_task_to_file(profile, True, 2, 2, 10010,
                                            False, False)
    print(chk)

# Change working directory back to initial directory
os.chdir(cwd)
