import webbrowser
def OpenLink ( [WO_ID] ):
  ##WO_ID is the field passed in by ArcGIS.  
  ##This is configured in the layer properties for the hyperlink
  ##change site.assetic.net to your Assetic site name
  path = 'https://site.assetic.net/Maintenance/Operational/Plan/EditWO/?Id=' + [WO_ID]
  webbrowser.open(path)
  return