import webbrowser
def OpenLink ( [AssetID] ):
  ##AssetID is the field passed in by ArcGIS.  
  ##This is configured in the layer properties for the hyperlink
  ##change site.assetic.net to your Assetic site name
  path = 'https://site.assetic.net/Assets/ComplexAsset/' + [AssetID]
  webbrowser.open(path)
  return