"""
    Example script to run a search profile (Assetic.SearchGetWithFilter.py)
    Search parameter include filter and order by
"""
import assetic

##Assetic SDK instance 
asseticsdk=assetic.AsseticSDK("C:/users/kwilton/assetic.ini",None,"Info")
sapi = assetic.SearchApi()

profileid = "f9149c1b-0a43-e611-945f-06edd62954d7" #demo

#set pagesize (number of results returned per request
pagesize = 500
##Run requests, looping through pages until all records returned
flag = True
cnt = 1

##create a filter by last modified date AND 'Asset Type'
##filter names are the internal names (refer to 'mappings' in response)
searchfilter = "ComplexAssetLastModified>=2016-08-03T13:16:07,ComplexAssetAssetCategory=Buildings"

while (flag == True):

    kw = {'request_params_id': profileid,
        'request_params_page':cnt,
        'request_params_page_size':pagesize,
        'request_params_sorts':'ComplexAssetName-desc',
        'request_params_filters':searchfilter}
    try:
        sg = sapi.search_get(**kw)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error("Status {0}, Reason: {1}".format(
            e.status,e.reason))
        exit()

    resourcelist = sg.get('ResourceList')
    resource = resourcelist[0]
    data = resource.get('Data')
    totalresults=sg.get('TotalResults')
    numpages = sg.get('TotalPages')
    if cnt > numpages:
        flag = False
    else:
        asseticsdk.logger.info('Total Results: {0}, Pages {1}, Page: {2}'.format(totalresults,numpages,cnt))
        cnt = cnt + 1
        for row in data:
            aname = row.get('Asset Name')
            atype = row.get('Asset Category')
            datemod = row.get('Asset Last Modified')
            asseticsdk.logger.info("'{0}' '{1}' '{2}'".format(aname,atype,datemod))
