"""
    Assetic.SearchSyncToDB.py
    Sample script to extract assets via search and write to DB
    Updates existing records, inserts new records based on assetid as key
"""
import pypyodbc
import sys
import assetic   #This is an SDK to wrap the REST endpoints
import datetime
import dateutil.parser
import configparser as configparser
import logging
import math
import functools
from contextlib import contextmanager
# Script Variables
# Read ini
settings = configparser.ConfigParser()
settings.read('C:/users/kwilton/assetic.ini')

# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
#Debug settings
conf.logger_file = 'c:/temp/create_request.txt'
conf.debug = False
if conf.debug == True:
    logging.basicConfig() 
    logging.getLogger().setLevel(logging.DEBUG)

##Database details
dbserver = "Quicksilver"
dbname = "scratch"

##search profile ID.
searchguid = "de68eb28-1f59-e611-9469-06edd62954d7"

##This example script does not write all fields from the search to the DB
##If you are just doing a dump of all fields to a table with field names
##that match search names then this dictionary is not required
##search column to table column mapping
columndict = {'Asset Id': '[F1_Asset_Number]',
            'Asset  Name': '[F2_Asset_Description]',
              'Asset Category': '[F4_Function_Description]',
              'Asset Class':'[F6_Activity_Description]',
            'Asset Type': '[F7_Asset_Type]',
            'Asset Status': '[F10_Active_Status]',
            'Asset Last Modified': '[datemodified]'}

##Initiate Assetic SDK instance
auth = conf.get_basic_auth_token()
client = assetic.ApiClient(conf.host,"Authorization",auth)
conf.api_client = client
    

##Define functions.  End of this script initiates the process that uses these functions 

@contextmanager
def open_db_connection(connection_string, commit=False):
    """
    Use contextmanager to manage connections so we cleanup connection
    as we go to avoid database locking issues on exception
    """
    connection = pypyodbc.connect(connection_string)
    cursor = connection.cursor()
    try:
        yield cursor
    except pypyodbc.DatabaseError as err:
        cursor.execute("ROLLBACK")
        raise err
    except Exception as ex:
        print(ex)
    else:
        if commit:
            cursor.execute("COMMIT")
        else:
            cursor.execute("ROLLBACK")
    finally:
        connection.close()

####End of functions################################

#Set pagesize of search.  500 is max
numpagesize = 500

##Get datetime this was last run.  Will only seach for assets with last modified since then
lastdate = None
# can use UID=user;PWD=password instead of trusted connection.
connstr = 'Driver={SQL Server};' + 'Server={0};Database={1};Trusted_Connection=True;APP=Assetic Integration for Assets to ECM'.format(dbserver,dbname)

sqlSelect = ("select max(datemodified) from assetic2db")
try:
    with open_db_connection(connstr) as cursor:
        cursor.execute(sqlSelect)
        rows = cursor.fetchall()
except Exception as ex:
    print(ex)
    exit()

lastdate = None
for row in rows:
    lastdate = row[0]
    
if lastdate == None:
    lastdate = "2015-09-26T12:00:00"  ##seed date

#make sure there is a 'T' in date string (required format by API)
lastdatestr = str(lastdate)
lastdatestr = lastdatestr.replace(' ','T')

#create search api instance and define search parameters
sapi = assetic.SearchApi(client)
kw = {'request_params_id':searchguid,
    'request_params_page':1,
    'request_params_page_size':numpagesize,
    'request_params_filters':'ComplexAssetLastModified>={0}'.format(lastdatestr)}

print('ComplexAssetLastModified>={0}'.format(lastdate))
##Execute search to get first page of results
sg = sapi.search_get(**kw)

totalresults=sg.get('TotalResults')
if totalresults == 0:
    print('No records found')
    exit()
numpages = math.ceil(totalresults/float(numpagesize))
print('Total Pages: {0}'.format(numpages))

##get actual data from nested output
resourcelist = sg.get('ResourceList')
resource = resourcelist[0]
data = resource.get('Data')

##Copy as alldata becuase we will be appending to this
alldata = data

##Get a list of columns
##TODO - could use new 'Mappings' object in reponse to get column list
columns = map( lambda x: x.keys(), data )
if sys.version_info < (3,0):
    columns = reduce( lambda x,y: x+y, columns )
else:
    columns = functools.reduce( lambda x,y: x|y, columns )

##Write list of columns to the 'all' list as column list may grow
##this is becuase the search does not return columns where all records in
##the page have a NULL value for that column.  Subsequent pages may return
##that column    
allcolumns = columns

##Now loop through remaining pages
if numpages > 1:
    for pagenum in range(2,int(numpages) + 1):
        ##set page number to get
        kw['request_params_page']=pagenum
        print('Page: {0}'.format(kw['request_params_page']))

        ##Now get results for this page
        sg = sapi.search_get(**kw)

        ##get actual data from nested output
        resourcelist = sg.get('ResourceList')
        resource = resourcelist[0]
        data = resource.get('Data')

        #alldata = alldata + data
        #Get column list for this page - there may be new columns
        columns = map( lambda x: x.keys(), data )
        if sys.version_info < (3,0):
            columns = reduce( lambda x,y: x+y, columns )
            ##Add new column list to 'allcolumns', will get unqiue list later
            allcolumns = allcolumns + columns
            #append new data to 'alldata'
            alldata = alldata + data
        else:
            columns = functools.reduce( lambda x,y: x|y, columns )
            ##merge column list sets
            allcolumns = allcolumns | columns
            #append new data to 'alldata'
            alldata.extend(data)

        if pagenum > 10:
            ##catchall escape loop (useful for testing)
            break

if sys.version_info < (3,0):
    #get unique list of columns
    columns = list( set( allcolumns ) )
else:
    columns = list(allcolumns)

##insert into tmp table
##First get the database table column names that match the search column names
##remove columns that are not mapped from the column list
##work in reverse order so that unwanted columns can be removed from the list
insertcolumns = list(columns)
parammark = list(columns)
for column in reversed(columns):
    try:
        mapped_col = columndict.get(column)
        if mapped_col == None:
            #column not required
            columns.remove(column)
            insertcolumns.remove(column)
            parammark.remove(column)
        else:
            insertcolumns[insertcolumns.index(column)] = mapped_col
            parammark[parammark.index(column)] = "?"
    except:
        #column not required
        columns.remove(column)
        insertcolumns.remove(column)
        parammark.remove(column)

##truncate temp table (in case not cleaned up after last run)
sqltruncate = "truncate table assetic2db_temp"
try:
    with open_db_connection(connstr,True) as cursor:
        cursor.execute(sqltruncate)
except Exception as ex:
    print(ex)
    exit()
    
##now prepare to into temp table 
sqlinsert = "insert into assetic2db_temp (" + ",".join(insertcolumns ) + ") values ("
sqlinsert = sqlinsert + ",".join(parammark ) + ")"

##prepare each row in a list so we can use the 'executemany' pypyodbc feature
allrows = []
for i_r in alldata:
    #map data to column list by key to avoid potential issues with column order
    allrows.append(tuple(map( lambda x: i_r.get( x, "" ), columns )))

allrows = list(set(allrows))
##write to db (assetic2db_temp table).
try:
    with open_db_connection(connstr,True) as cursor:
        cursor.executemany(sqlinsert,allrows)
except Exception as ex:
    print(ex)
    exit()

##now refresh assetic2db table from assetic2db_temp table
##Note that we use a case statememt to get boolean for status
sqldiff = """
WITH diff as (
    select  F1_Asset_Number
    ,F2_Asset_Description
    ,F3_Function_Code
    ,F4_Function_Description
    ,F5_Activity_Code
    ,F6_Activity_Description
    ,F7_Asset_Type
    ,F8_Asset_Type_Description
    , case when [F10_Active_Status]= 'Disposed' then '0' else '1'
    end as F10_Active_Status,
    [datemodified]
    FROM [dbo].[assetic2db_temp]
    except select 
    [F1_Asset_Number],[F2_Asset_Description],[F3_Function_Code],
    [F4_Function_Description],[F5_Activity_Code],[F6_Activity_Description],
    [F7_Asset_Type],[F8_Asset_Type_Description],[F10_Active_Status],
    [datemodified]
    FROM [dbo].assetic2db
    )
    MERGE [dbo].assetic2db as a using diff as c on
    (c.[F1_Asset_Number]=a.[F1_Asset_Number])
    when MATCHED then update set
    a.[F1_Asset_Number]=c.[F1_Asset_Number],
    a.[F2_Asset_Description]=c.[F2_Asset_Description],
    a.[F3_Function_Code]=c.[F3_Function_Code],
    a.[F4_Function_Description]=c.[F4_Function_Description],
    a.[F5_Activity_Code]=c.[F5_Activity_Code],
    a.[F6_Activity_Description]=c.[F6_Activity_Description],
    a.[F7_Asset_Type]=c.[F7_Asset_Type],
    a.[F8_Asset_Type_Description]=c.[F8_Asset_Type_Description],
    a.[F10_Active_Status]=c.[F10_Active_Status],
    a.[datemodified]=c.[datemodified]
    When not matched then insert
    ([F1_Asset_Number],[F2_Asset_Description],[F3_Function_Code],
    [F4_Function_Description],[F5_Activity_Code],[F6_Activity_Description],
    [F7_Asset_Type],[F8_Asset_Type_Description],[F10_Active_Status],
    [datemodified],[synctimestamp]) 
    values ([F1_Asset_Number],[F2_Asset_Description],[F3_Function_Code],
    [F4_Function_Description],[F5_Activity_Code],[F6_Activity_Description],
    [F7_Asset_Type],[F8_Asset_Type_Description],[F10_Active_Status],
    [datemodified],GETDATE())
    output $action,inserted.[F1_Asset_Number],getdate() into [dbo].[assetic2db_log];
"""
try:
    with open_db_connection(connstr,True) as cursor:
        cursor.execute(sqldiff)
except Exception as ex:
    print(ex)
    exit()
##now truncate temp table
#try:
#    with open_db_connection(connstr,True) as cursor:
#        cursor.execute(sqltruncate)
#except Exception as ex:
#    print(ex)
#    pass

