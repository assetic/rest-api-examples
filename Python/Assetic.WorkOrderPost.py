"""
    Example script to create a work order (Assetic.DocumentGet.py)
    Allows a work order to be created.  Can be at any stage in the
    work order process
"""
import assetic
import sys
import datetime
import json
try:
    import configparser as configparser
except:
    import ConfigParser as configparser
import logging

# Read ini
settings = configparser.ConfigParser()
ini_file = 'C:/Users/kwilton/asseticPreview.ini'
settings.read(ini_file)
# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
auth = conf.get_basic_auth_token()
#Debug settings
conf.logger_file = 'C:/temp/logger.txt'
conf.debug = False
if conf.debug == True:
    logging.basicConfig() 
    logging.getLogger().setLevel(logging.DEBUG)

##Assetic SDK instance 
client = assetic.ApiClient(conf.host,"Authorization",auth)
conf.api_client = client

##Setup values to use based on which environment connecting to
if ini_file == 'C:/Users/kwilton/asseticNightly.ini':
    ##Nightly
    resource_id = "58487b6f-1ed1-4fac-a2ec-b235acbe3628"   #Andrew Cox
    creator_resource_id = "58487b6f-1ed1-4fac-a2ec-b235acbe3628"   #Andrew Cox
    #bill_of_material_id = "6f4d6b7c-380f-e511-9448-06edd62954d7"   #Access Ramp
    bill_of_material_id = "7b993464-4c84-e511-a357-063b4877b1e3"   #Drainage
    group_craft_id = "4ee6f417-c531-e511-a357-063b4877b1e3"
    failure_sub_code = 45
    cause_sub_code_id = 23
    remedy_id = 14
    complex_asset_id = "172586"   #Fire trail Wellington park
else:
    ##Preview
    resource_id = "58487b6f-1ed1-4fac-a2ec-b235acbe3628"   #Andrew Cox
    creator_resource_id = "1ec40231-6687-e511-944f-06edd62954d7"  #Andy Boulton
    bill_of_material_id = "6204274c-201a-e511-9448-06edd62954d7"   #Acrylic basin
    group_craft_id = "eab88f71-63be-e511-9451-06edd62954d7"   #'Supervisor (Day)' in 'Civil' work group
    failure_sub_code = 46    #Pothole
    cause_sub_code_id = 22   #Normal wear & tear
    remedy_id = 16    #Repair
    complex_asset_id = "172586"    #Fire trail Wellington park
##End setup

    
#Set resource object values for work task
resource = assetic.Assetic3IntegrationApiRepresentationsRsResourceRepresentation()
resource.id = resource_id
#Set resource object values of work order creator
creator_resource = assetic.Assetic3IntegrationApiRepresentationsRsResourceRepresentation()
creator_resource.id = creator_resource_id

#Set bill of materials object values
material = assetic.Assetic3IntegrationApiRepresentationsWorkOrderMaterial()
material.bill_of_material_id = bill_of_material_id
material.planned_quantity = 1
#material.planned_cost = 666
#material.actual_quantity = 0

materiallist = [material]

#Set Craft object values
craft = assetic.Assetic3IntegrationApiRepresentationsWorkOrderLabour()
craft.id = 0
craft.craft = None
craft.actual_quantity = 1
craft.planned_quantity = 1.5
#craft.assigned_quantity = 1
#craft.planned_costs = 70
#craft.assigned_costs = 40
#craft.actual_costs = None
craft.assigned_group_craft_id = group_craft_id
craft.planned_group_craft_id = group_craft_id
#craft.supporting_information = "REST API - craft supporting information" #no impact

#Set work order task
wotask = assetic.Assetic3IntegrationApiRepresentationsWorkTask()
#wotask.work_task_status_id = 5
#wotask.work_task_status = "Unassigned"
#wotask.description = "REST API task description"
#wotask.materials = materiallist
wotask.work_task_craft_id = group_craft_id
wotask.craft = craft
#wotask.labour_costs = 840
#wotask.material_costs = 100
wotask.external_costs = 10
wotask.other_costs = 30
wotask.tool_and_material_available = True
wotask.loss_of_service = False

wotasklist = [wotask]

wo = assetic.Assetic3IntegrationApiRepresentationsWorkOrder()
wo.priority_id = 1  #corresponds to code in UI
wo.complex_asset_id = complex_asset_id
#wo.supporting_information = "REST API for status PREP"
#
#wo.work_order_status_id = 3
#wo.work_order_type_id = 2   #The number matches the order of the type in the UI (starting at 1)
wo.work_request_source_id = 3
wo.target_duration = 3.5
wo.target_start_date = "2016-04-12 00:00:00.000"
wo.target_finish_date = "2016-04-13 01:00:00.000"
wo.resource = creator_resource
wo.work_tasks = wotasklist
wo.external_id = 'External 1234'


woapi = assetic.WorkOrderApi(client)

#for statusnum in range(3,14):
for statusnum in range(3,4):
    wo.work_order_status_id = statusnum

    ##Set scheduled date
    if wo.work_order_status_id in [9,10,11,12]:
        ##Scheduled dates are required for these statuses
        if wo.work_order_status_id == 9:
            #for this status the scheduled date must be greater than current date
            wo.schedule_start_date = datetime.datetime.utcnow()
            + datetime.timedelta(days=1)
            wo.schedule_finish_date = datetime.datetime.utcnow()
            + datetime.timedelta(days=2)
        else:
            ##date can be in past
            wo.schedule_start_date = "2016-04-12 00:00:00.000"
            wo.schedule_finish_date = "2016-04-13 00:00:00.000"
    else:
            wo.schedule_start_date = None
            wo.schedule_finish_date = None

    #Assign resource, actuals, and task status as required
    if wo.work_order_status_id in [10]:
        ##set task status as assigned
        wo.work_tasks[0].work_task_status_id = 1
        wo.work_tasks[0].resource = resource
        wo.work_tasks[0].craft.actual_quantity = None
    elif wo.work_order_status_id in [11,12]:
        ##set task status as complete
        wo.work_tasks[0].work_task_status_id = 6
        wo.work_tasks[0].resource = resource
        wo.work_tasks[0].craft.actual_quantity = 0
        wo.work_tasks[0].craft.actual_costs = 789
        #set materials
        materiallist[0].actual_quantity = 1
        materiallist[0].actual_cost = 678
    else:
        ##set task status as unasssigned
        wo.work_tasks[0].work_task_status_id = 5
        wo.work_tasks[0].resource = None
        wo.work_tasks[0].craft.actual_quantity = 1
        #set materials
        materiallist[0].actual_quantity = 1
        materiallist[0].actual_cost = 1
        
    ##Add materials to the task
    if wo.work_order_status_id in [3,4]:
        wo.work_tasks[0].materials = None
        wo.materials = None
        wo.work_tasks[0].work_task_material_status_id = 1
    else:
        wo.work_tasks[0].materials = materiallist
        wo.work_tasks[0].work_task_material_status_id = 2
        wo.work_tasks[0].material_costs = 100  #planned
        wo.materials = materiallist

    ##Defect location
    if wo.work_order_status_id in [3]:
        wo.defect_location = "Collins St"
    else:
        wo.defect_location = None
        
    #for wotypenum in range(1,13):
    for wotypenum in range(5,6):   
        wo.work_order_type_id = wotypenum   #The number matches the order of the type in the UI (starting at 1)

        ##Set FCR
        if wo.work_order_status_id in [3,4,5,9,10,11,12]:
            #Remedy ID required for this status regardless of work order type
            wo.remedy_id = remedy_id

        if wo.work_order_type_id in [5,6,7,8]:
            #for this work order type we need to set these 2 codes
            wo.failure_sub_code_id = failure_sub_code
            wo.cause_sub_code_id = cause_sub_code_id
            wo.remedy_id = remedy_id
        else:
            wo.failure_sub_code_id = None
            wo.cause_sub_code_id = None
            if wo.work_order_status_id in [6]:
                wo.remedy_id = None

        #Set comments for work order and also task
        wo.supporting_information = "REST API. Initial Work order Status: {0}, Work Order Type: {1}, Initial Task Status: {2}".format(
                wo.work_order_status_id,wo.work_order_type_id,wo.work_tasks[0].work_task_status_id)
        wo.work_tasks[0].description = "REST API Task.  Initial Task Status: {0}".format(wo.work_tasks[0].work_task_status_id) 

        ##debug comment
        print('remedy: {0}, Failure: {1},Cause {2},taskstatus: {3}'
              .format(wo.remedy_id,wo.failure_sub_code_id,wo.cause_sub_code_id,
                      wo.work_tasks[0].work_task_status_id))

        ##Execute the POST
        try:            
            response = woapi.work_order_post(wo)    #returns guid of work request
        except assetic.rest.ApiException as e:
            print ('Status {0}, Reason: {1}, Work order Status: {2}, Work OrderType: {3}'
                   .format(e.status,e.reason,wo.work_order_status_id,
                    wo.work_order_type_id))
        else:
            datadict = response.get('Data')
            data = datadict[0]
            print('WKO: {3}, Guid: {0}, Work order Status: {1}, Work Order Type: {2}, External ID: {4}'.format(
                data.get('Id'),wo.work_order_status_id,wo.work_order_type_id,data.get('FriendlyIdStr'),data.get('ExternalID')))
        
