"""
    Script to automate data-exchange in bulk fashion (Assetic.BulkDataExchange.py)
    Requirements:
    1. Create a folder with following sub-folders: ToLoad, Processed, Error and a excel file named as DataToImport.xlsx
    2. Update assetic.ini file's [bulk_data_exchange] value with location of folder created in Step: 1
    3. Excel file named DataToImport.xlsx MUST have following columns in exact order
    ProfileId | DocumentToImport(Excel filename only)

    4. Load/move all document you wish to import in ToLoad directory of folder defined in Step: 1
    5. Run and Oberserve script output

    Output of this script explained:
    1. Successfully processed data-exchange requested document is moved to Processed directory of folder defined in Step: 1
    2. Failed to process data-exchange requested document is moved to Error directory of folder defined in Step: 1

    Please note that this script only requests data-exchange and returns wheather request accepted or failure. To check the operation progress use task id printed on output
"""

import assetic
import os
import shutil
import time
import sys
import base64
import openpyxl #to read excel files

try:    
    import configparser as configparser
except:
    try:
        #V2 python?
        import ConfigParser as configparser
    except:
        print ("Unable to load configparser module")
        sys.exit()

# Read ini
settingsRelativePath = os.path.join(os.path.dirname(__file__), 'assetic.ini')
settings = configparser.ConfigParser()
settings.read(settingsRelativePath)

# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
conf.bulk_document_upload_init_directory = settings.get('bulk_document_upload', 'init_directory')
conf.bulk_dataexchange_init_directory = settings.get('bulk_data_exchange', 'init_directory')
auth = conf.get_basic_auth_token()

#Debug settings
conf.logger_file = conf.bulk_dataexchange_init_directory + '//BulkDataExchangeLog.txt'
conf.debug = False

client = assetic.ApiClient(conf.host,"Authorization",auth)
docapi = assetic.DocumentApi(client)
dataexchangeapi = assetic.DataExchangeJobApi(client)

intdir = conf.bulk_dataexchange_init_directory + "//"
sourcedir = intdir + "ToLoad//"
errordir = intdir + "Error//"
processeddir = intdir + "Processed//"
excelFileToImport = intdir + 'DataToImport.xlsx'

wb = openpyxl.load_workbook(excelFileToImport)
sheet = wb.active
#sheet = wb.get_sheet_by_name('Population by Census Tract')
for row in range(2, sheet.max_row + 1):
    # Each row in the spreadsheet has data for one census tract.
    dataExchangeProfileId = sheet['A' + str(row)].value    
    fileName   = sheet['B' + str(row)].value    

    print ('Processing row entry: {2}(from excel file) for Profile Id: {0} and document to import: {1}\n'.format(dataExchangeProfileId, fileName, row))
 
    files = os.listdir(sourcedir)
    for filename in files:
        fetchCorrectFile = True if filename == fileName else False    
        if fetchCorrectFile:            
            fullfilename = sourcedir + filename    
            fileExtention = filename[filename.rindex('.') + 1:]
            with open(fullfilename, "rb") as f:
                data = f.read()

            if sys.version_info < (3,0):
                filecontents = data.encode("base64")
            else:
                filecontents = base64.b64encode(data)
                filecontents = filecontents.decode(encoding="utf-8", errors="strict")
                
            filesize = os.path.getsize(fullfilename)
            print ('Reading File name: {0}; File Size: {2}; Extracted file extention: {1} from {3} directory'.format(filename, fileExtention, filesize, sourcedir))

            file_properties = assetic.Assetic3WebApiModelsFilePropertiesVM()
            file_properties.name = filename
            file_properties.file_size = filesize
            file_properties.mimetype = fileExtention
            file_properties.filecontent = filecontents
            filearray = [file_properties]            
                    
            document = assetic.Assetic3IntegrationApiRepresentationsDocumentRepresentation()                               
            document.document_type = 'DataExchange'            
            document.label = 'Importing data-exchange document - ' + filename + ' on ' + time.strftime("%d/%m/%Y %H:%M:%S")
            
            document.file_property = filearray
            document.mime_type = fileExtention

            doc = docapi.document_post(document)
            docid = doc[0].get('Id') 
            print ('Document: {0} is uploaded to client'.format(docid))
            
            isDocumentUploadSuccessful = True if docid != '' else False    
            #move file to in-progress, and rename to task id to allow checks on success/failure    
            if isDocumentUploadSuccessful:
                print('Requesting data-exchange job for profile: {0}/Document: {1}'.format(dataExchangeProfileId, filename))
                job = assetic.Assetic3WebApiModelsDataExchangeDataExchangeJobVM()
                job.profile_id = dataExchangeProfileId
                job.document_id = docid
##                print job
                task = dataexchangeapi.data_exchange_job_post(job)
                print('Data-exchange task: {0} created'.format(task))

                print ('Moving file: {0} to processed document folder: {1}\n'.format(filename, processeddir))
##                taskDestination = processeddir + task + '.csv'                
##                print ('Moving file: {0} to processed/task folder: {1}\n'.format(filename, taskDestination))
                shutil.move(fullfilename,processeddir)
            else:
                print ('Moving file: {0} to error folder: {1}; No data-exchange job is requested for this document\n'.format(filename, errordir))
                shutil.move(fullfilename,errordir)                


    
    
    
