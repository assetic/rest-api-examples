import assetic
import os
import assetic_qgis
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsFeatureRequest,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingOutputNumber,
                       QgsProcessingParameterFileDestination)
from qgis import processing
import qgis
import requests
import json

from assetic_qgis import QgisConfig
from assetic_qgis.tools import QGISMessager


class AsseticIntegrationRunConfigurationCheck(QgsProcessingAlgorithm):
    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def name(self):
        return "Run XML Configuration Check"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Run XML Configuration Check')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('Assetic')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'asseticscripts'

    def createInstance(self):
        return AsseticIntegrationRunConfigurationCheck()

    def setup_assetic(self, feedback, conf_output=None):
        appdata = os.environ.get("APPDATA")
        inifile = os.path.abspath(appdata + r"\Assetic\assetic.ini")
        logfile = os.path.abspath(appdata + r"\Assetic\assetic_qgis.log")
        xmlfile = os.path.abspath(appdata + r"\Assetic\qgis_edit_config.xml")
        if not conf_output:
            conf_output = os.path.abspath(
                appdata + r"\Assetic\qgis_config_validation_report.txt")

        messager = QGISMessager(feedback)

        conf = QgisConfig()
        conf.messager = messager
        conf.xmlconfigfile = xmlfile
        conf.inifile = inifile
        conf.logfile = logfile
        conf.loglevelname = "info"

        assetic_qgis.Initialise(config=conf)

        self.conf_check = assetic_qgis.XMLConfigVerifier(
            xmlfile, inifile, conf_output)

        feedback.pushInfo("Initiated Assetic Libraries")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        appdata = os.environ.get("APPDATA")
        conf_output = os.path.abspath(
            appdata + r"\Assetic\qgis_config_validation_report.txt")
        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.INPUT,
                self.tr('Output File'),
                'Text Documents (*.txt)',
                conf_output
            )
        )

        self.addOutput(
            QgsProcessingOutputNumber(
                'SUCCESSCODE',
                self.tr('Success code for if configuration log was '
                        'successfully created.')
            )
        )
        pass

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        # get the output file name
        output_file = self.parameterAsFileOutput(
            parameters,
            self.INPUT,
            context
        )

        # initialise assetic package
        self.setup_assetic(feedback, output_file)

        self.conf_check.run()

        feedback.pushInfo(
            "Completed XML configuration check. Results can be viewed "
            "here: {0}".format(self.conf_check.conf_output))

        return {"SUCCESSCODE": 0}
