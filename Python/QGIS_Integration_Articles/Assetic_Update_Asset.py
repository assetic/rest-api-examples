import assetic
import os
import assetic_qgis
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsFeatureRequest,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingOutputNumber)
from qgis import processing
import qgis
import requests
import json

from assetic_qgis import QgisConfig
from assetic_qgis.tools import QGISMessager


class AsseticIntegrationUpdateAssets(QgsProcessingAlgorithm):
    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'

    def name(self):
        return "Update Assets"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Update Asset')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('Assetic')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'asseticscripts'

    def createInstance(self):
        return AsseticIntegrationUpdateAssets()

    def setup_assetic(self, feedback):
        appdata = os.environ.get("APPDATA")
        inifile = os.path.abspath(appdata + r"\Assetic\assetic.ini")
        logfile = os.path.abspath(appdata + r"\Assetic\assetic_qgis.log")
        xmlfile = os.path.abspath(appdata + r"\Assetic\qgis_edit_config.xml")

        messager = QGISMessager(feedback)

        conf = QgisConfig()
        conf.messager = messager
        conf.xmlconfigfile = xmlfile
        conf.inifile = inifile
        conf.logfile = logfile
        conf.loglevelname = "info"

        aq = assetic_qgis.Initialise(config=conf)
        self.tools = assetic_qgis.QGISLayerTools(conf)
        feedback.pushInfo("Initiated Assetic Libraries")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )

        self.addOutput(
            QgsProcessingOutputNumber(
                'NUMBERSUCCESS',
                self.tr('Number of features successfully processed')
            )
        )

        self.addOutput(
            QgsProcessingOutputNumber(
                'NUMBERFAIL',
                self.tr('Number of features NOT successfully processed')
            )
        )

        self.addOutput(
            QgsProcessingOutputNumber(
                'NUMBERSKIP',
                self.tr('Number of features skipped')
            )
        )

        self.addOutput(
            QgsProcessingOutputNumber(
                'NUMBERPARTIAL',
                self.tr('Number of features partially processed')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        # get the layer to be processed
        source = self.parameterAsVectorLayer(
            parameters,
            self.INPUT,
            context
        )
        if source is None:
            # couldn't get a vector layer from source
            raise QgsProcessingException(
                self.invalidSourceError(parameters, self.INPUT))

        if source.selectedFeatureCount() == 0:
            # expect there to be selected features, not intending to process
            # all features layer
            feedback.pushInfo(
                "No features selected to process for layer {0}".format(
                    source.name()))
            return {'NUMBERSUCCESS': 0, 'NUMBERFAIL': 0
                , 'NUMBERPARTIAL': 0, 'NUMBERSKIP': 0}
        # initialise assetic package
        self.setup_assetic(feedback)

        # execute integration
        results = self.tools.update_assets(source)

        if results is None:
            feedback.pushInfo("Bulk update of rows occurring. Check "
                              "output in data exchange.")
            numfail = 0
            numsuccess = 0
            numignore = 0
            numpartial = 0
        else:

            # report results of integration
            numfail = 0
            numsuccess = 0
            numignore = 0
            numpartial = 0
            if "pass_cnt" in results:
                numsuccess = results["pass_cnt"]
            if "fail_cnt" in results:
                numfail = results["fail_cnt"]
            if "ignore_cnt" in results:
                numignore = results["ignore_cnt"]
            if "partial_cnt" in results:
                numpartial = results["partial_cnt"]

        return {'NUMBERSUCCESS': numsuccess, 'NUMBERFAIL': numfail
                , 'NUMBERPARTIAL': numpartial, 'NUMBERSKIP': numignore}
