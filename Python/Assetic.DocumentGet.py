"""
    Example script to download a document (Assetic.DocumentGet.py)
    For a given document id download the document
"""
import assetic
from assetic.rest import ApiException
import re

# Assetic SDK instance
asseticsdk = assetic.AsseticSDK("c:/users/you/assetic.ini", None, "Error")

filedir = "C:/temp/"

docid = "c31b7d80-9a53-4d5c-a3b2-14a9f39077ec"  # document to get

# Assetic document API instance.
docapi = assetic.DocumentApi()

try:
    # get with http info because filename is in response header
    getfile = docapi.document_get_document_file_with_http_info(docid)
except ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1} {2}"
                            .format(e.status, e.reason, e.body))
    exit()

if getfile is not None:
    # initially set filename to unknown
    fullfilename = filedir + "unknownfilename"

    # get document name from response. The response is a tuple.
    if "Content-Disposition" in getfile[2]:
        # extract document name from response.
        if ("attachment" in getfile[2]["Content-Disposition"]
                and "filename=" in getfile[2]["Content-Disposition"]):
            # overwrite unknown filename if found in response
            filename = getfile[2]["Content-Disposition"].split("filename=", 1)[1]
            if '"' in filename or "'" in filename:
                filename = filename[1:-1]
            # check and replace any invalid characters in the filename
            invalid_char_match = r'[^\w\-_\.]+'
            replace_char = '_'
            filename = re.sub(invalid_char_match, replace_char, filename)
            fullfilename = filedir + filename

    # get document data from response
    data = getfile[0]

    if type(data) is bytes:
        # cater for different binary data
        with open(fullfilename, "wb") as out_file:
            out_file.write(getfile[0])
            print("Created file: {0}".format(fullfilename))
    elif type(data) is str:
        # string data
        with open(fullfilename, "w", newline="", encoding="utf-8") as out_file:
            try:
                out_file.write(getfile[0])
                print("Created file: {0}".format(fullfilename))
            except UnicodeEncodeError as ex:
                print("Encoding error: ".format(str(ex)))
    else:
        print("File not created, data type unhandled: {0}".format(
            str(type(data))))
