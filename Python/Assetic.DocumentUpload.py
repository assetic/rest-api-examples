"""
    Example script to upload a document(Assetic.DocumentUpload.py)
    This script uploads a file to assetic.  Can link the file to a record
    such as an asset or work order
"""
import assetic
import os
import datetime
import base64
import sys

##Assetic SDK instance 
asseticsdk = assetic.AsseticSDK(None,None,'Info')
docapi = assetic.DocumentApi()

fullfilename = "C:/temp/Assetic.txt"
filename = os.path.basename(fullfilename)
fullpathnoext,fileext = os.path.splitext(fullfilename)

with open(fullfilename, "rb") as f:   
    data = f.read()
if sys.version_info < (3,0):
    filecontents = data.encode("base64")
else:
    filecontents = base64.b64encode(data)
    filecontents = filecontents.decode(encoding="utf-8", errors="strict")

#Can get file size to check
filesize = os.path.getsize(fullfilename)

file_properties = assetic.Assetic3IntegrationRepresentationsFilePropertiesRepresentation()
file_properties.name = filename
file_properties.mimetype = fileext
file_properties.filecontent = filecontents
file_properties.file_size = filesize
filearray = [file_properties]

#Doc categorisation
document = assetic.Assetic3IntegrationRepresentationsDocumentRepresentation()
document.document_group = 1
document.document_category = 9
document.document_sub_category = 7
document.file_property = filearray
document.parent_type = 'ComplexAsset'  #conditional, use if assigning parentID
document.parent_identifier = "AP01"
document.is_key_photo = False
# Perform upload
try:
    doc = docapi.document_post(document)
except assetic.rest.ApiException as ex:
    asseticsdk.logger.error('Status {0}, Reason: {1} {2}'.format(
        e.status, e.reason, e.body))    
else:
    for docrow in doc:
        docid = docrow.get('Id') 
        print(docid)
