"""
    Example script to apply an assessment result to a task
    (Assetic.Assessments.py)

    1. create (POST) a form result.
    2. use the generated ID in form result response to link to the
    assessment task
"""
import assetic
from datetime import datetime
from datetime import timedelta
import csv
asseticsdk = assetic.AsseticSDK('C:/Users/kwilton/asseticAtkins.ini',None,'Info')

formlabel = "HVAC Physical Condition Assessment"
csvfile = "C:/Users/kwilton/Downloads/HVACPhysicalConditionAssessment API Testing.csv"
taskidcolumn = "taskid"
maxerrors = 5

##create an api instance
assessresultapi = assetic.AssessmentFormResultApi()
assesstaskapi = assetic.AssessmentTaskApi()
assessformapi = assetic.AssessmentFormApi()

def get_task_for_friendlytaskid(taskid):
    """
    Given the user friendly task ID get task
    :param taskid: user friendly taskid
    :return: task object or None
    """
    #get the details of the task and form from API using taskId
    kwargs = {"request_params_page":1,
              "request_params_page_size":1,
              "request_params_filters":"TaskId='{0}'".format(taskid)
    }

    try:
        response = assesstaskapi.assessment_task_get(**kwargs)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
            e.status,e.reason,e.body))
        return None
    if response["TotalResults"] == 0:
        asseticsdk.logger.error("Task [{0}] not found".format(taskid))
        return None    
    return response["ResourceList"][0]

def get_form_definition(formguid):
    """
    For the given form guid get the form definition
    :param: assessment form guid
    :return: definition of form as json, or None
    """
    try:
        response = assessformapi.assessment_form_get(formguid)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
            e.status,e.reason,e.body))
        return None
    return response

def prepare_form_result_payload(formguid,resultdata):
    """
    Match a dictionary of results with the form controls to create result payload
    :param formguid: The form to load the results against
    :param resultdata:dictionary, key = form field label, value = field result
    :return: dictionary , key = form control ID, value = field result from resultdata
    or None if error
    """
    ##get the form definition
    form = get_form_definition(formguid)
    if form == None:
        return None
    ##prepare the payload
    payload = {}
    ##loop through the form controls and see if there is a matching key in
    ##the result data
    for tab in form["FormTabs"]:
        for widget in tab["FormLayout"]["Widgets"]:
            for control in widget["FormControlGroup"]["FormControls"]:
                label = control["Label"].replace(" ","").lower()
                if label in resultdata:
                    if resultdata[label] != None and resultdata[label] != "":
                        payload[control["Name"]] = resultdata[label]
    return payload

def get_task_form_identifiers(taskid,formlabel):
    """
    Get the task and form guid, plus resource
    :param taskid: user friendly task id
    :param formlabel: the label of the form. Use to compare with forms in task
    :return: tuple of guid of the assessment task,guid of the assessment form
    and resource object assetic.Assetic3IntegrationRepresentationsRsResourceRepresentation
    if error return None tuple
    :return: 0 = success, >0 = error
    """
    ##get task details - guid of task resource, form
    task = get_task_for_friendlytaskid(taskid)
    if task == None:
        return None,None,None
    resource = task["RsResourceIdAssignedTo"]
    taskguid = task["Id"]
    formguid = None
    for form in task["AsmtTaskForms"]:
        if form["FormLabel"] == formlabel:
            formguid = form["FormId"]
            break
    if formguid == None:
        asseticsdk.logger.error("Task [{0}] not found".format(taskid))
        return None,None,None
    return taskguid,formguid,resource
    
def create_assessment_result(taskguid,formguid,resource,resultdata):
    """
    Create an assessment form result given the task & form guid and result data as
    a dictionary list
    :param taskguid: unique guid of the assessment task
    :param formguid: unique guid of the assessment form
    :param resource: assetic.Assetic3IntegrationRepresentationsRsResourceRepresentation
    :param resultdata: dictionary, key = form field label, value = field result
    :return: 0 = success, >0 = error
    """

    ##match the form controls with the data
    data = prepare_form_result_payload(formguid,resultdata)
    if data == None:
        return 1
    #build the object to create the result entry
    form_result = assetic.Assetic3IntegrationRepresentationsAsmtFormResultRepresentation()
    form_result.form_id = formguid
    if "formcomments" in resultdata:
        form_result.form_result_comment=resultdata["formcomments"]
    #start time is mandatory,so if not in data then use yesterday
    if "formstarttime" in resultdata and \
       resultdata["formstarttime"].strip(" ") != "":
        form_result.form_result_start_time = resultdata["formstarttime"]
    else:        
        form_result.form_result_start_time = datetime.isoformat(datetime.now() +
                                                            timedelta(days=-1))
    #end time is mandatory,so if not in data then use yesterday
    if "formendtime" in resultdata and \
       resultdata["formendtime"].strip(" ") != "":
        form_result.form_result_end_time = resultdata["formendtime"]
    else:
        form_result.form_result_end_time = datetime.isoformat(datetime.now())
        
    form_result.form_result_last_modified = datetime.isoformat(datetime.now())
    form_result.form_result_status = 1
    form_result.form_result_rs_resource_id_assessed_by=resource
    form_result.data = data
    ##execute the post
    try:
        response = assessresultapi.assessment_form_result_post(form_result)
    except assetic.rest.ApiException as e:
        ##Log exception and exit
        asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
            e.status,e.reason,e.body))
        return e.status    
    ##check for error messages
    if response.get('Errors') != None:
        ##Log exception and return
        asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
            e.status,e.reason,e.body))
        return 1

    ##get response ID
    resultid = response['Data'][0]['Id']
    asseticsdk.logger.info("Created result [{0}]".format(resultid))
    ##now link form result with task
    try:
        linkresponse = assesstaskapi.assessment_task_link_asmt_form_result(
            taskguid,resultid)
    except assetic.rest.ApiException as e:
        #Log exception and exit
        asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
            e.status,e.reason,e.body))
        return e.status
    asseticsdk.logger.debug(linkresponse)    #expect 'True'
    return 0

def main(taskidcolumn,formlabel,csvfile,maxerrors=1):
    errcnt = 0  #track number of errors

    bIsHeader = True
    allrows = []
    saved_row = []

    ##read data into a list.
    ##This takes care of potential carriage return issues
    with open(csvfile, 'rt', encoding='utf-8', newline = '') as csvfile:
        readCSV = csv.reader(csvfile,delimiter=',')
        for row in readCSV:
            if bIsHeader == True:
                bIsHeader = False
                ##set columns to lowercase for ease of testing later
                columns = [x.lower().replace(" ","") for x in row]
            else:
                #cater for carriage returns in a column
                if len(saved_row):
                    row_tail = saved_row.pop()  #gets last item in the list
                    row[0] = row_tail + '\r' +row[0]  # reconstitute field broken by newline
                    row = saved_row + row       # and reassemble the row (possibly only partially)
                if len(row) >= len(columns):
                    allrows.append(row)
                    saved_row = []
                else:
                    saved_row = row

    ##Loop through each row and create record                
    for result in allrows:
        resultdata = {}
        for x in range(0,len(row)):
            if columns[x] != "":
                #assign the value to the column name
                resultdata[columns[x]] = result[x]
                
        if len(resultdata) > 0 and taskidcolumn in resultdata:
            taskid =  resultdata[taskidcolumn]
            taskguid,formguid,resource = get_task_form_identifiers(
                                            taskid,formlabel)
            if taskguid != None:
                chk = create_assessment_result(
                    taskguid,formguid,resource,resultdata)
            else:
                chk = 1
            if chk > 0:
                errcnt = errcnt + 1
                if errcnt >= maxerrors:
                    msg = "Error limit of [{0}] reached.".format(maxerrors)
                    asseticsdk.logger.warning(msg)
                    return
        else:
            ##no result data
            msg = "No assessment result header names found to match form"
            asseticsdk.logger.warning(msg)
            return
    
if __name__ == "__main__":
    main(taskidcolumn,formlabel,csvfile,maxerrors)
