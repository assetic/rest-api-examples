"""
Get an asset including component and dimensions using the SDK asset tools
(Assetic.AssetsGetCompleteSDK.py)
"""
import assetic
#Assetic SDK instance
asseticsdk=assetic.AsseticSDK("c:/users/you/assetic.ini", None,"Info")
#Tools API
assettools = assetic.AssetTools()

assetid = "411142055"
attributes = ["Zone","Comment"]
asset = assettools.get_complete_asset(assetid,attributes)
print(asset)


