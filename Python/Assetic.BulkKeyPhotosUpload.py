"""
    Script to automate document/photo upload in bulk fashion (Based on Assetic.BulkDocumentUpload.py)
    Requirements:
    1. Create excel file named as KeyPhotosDataToImport.xlsx
    2. Change script start directory to location of folder created in Step: 1
    3. Excel file named KeyPhotosDataToImport.xlsx MUST have following columns in EXACT ORDER
    Request Status | ComplexAssetId | DocumentGroupId | DocumentCategoryId | DocumentSubCatgoryId | Image Data
    
    4. Run and Oberserve script output

    Output:
    Key photo upload request status is written back to KeyPhotosDataToImport.xlsx's [Request Status(First Column)] column

    Parameters - debug (optiona;): values True or False.  Default is False
"""

import assetic
import os
import shutil
import time
import sys
import base64
import openpyxl #to read excel files
import io
import array
import time
import cStringIO as StringIO

try:
    import configparser as configparser
except:
    #V2 python?
    import ConfigParser as configparser
        
settingsRelativePath = os.path.join(os.path.dirname(__file__), 'assetic.ini')
settings = configparser.ConfigParser()
settings.read(settingsRelativePath)

# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
auth = conf.get_basic_auth_token()

bulk_document_upload_init_directory = os.getcwd()
#Debug settings
conf.logger_file = bulk_document_upload_init_directory + '\\BulkKeyPhotoUploadLog.txt'
conf.debug = True

client = assetic.ApiClient(conf.host,"Authorization",auth)
docapi = assetic.DocumentApi(client)
dataexchangeapi = assetic.DataExchangeJobApi(client)

##Keyphoto file naming format: <SaaS AssetID>_keyphoto.jpg
keyPhotosStorageLocation = 'C:\\Projects\\rest-api-examples\\Testing_Python\\KeyPhotos\\'
excelSaveFilePath = keyPhotosStorageLocation + 'KeyPhotosMigrationStatus.xlsx'

validImageExtensions = {".jpg", ".png", ".gif"} 

keyPhotosFiles = os.listdir(keyPhotosStorageLocation)
noOfFiles  = len(os.walk(keyPhotosStorageLocation).next()[2])

wb = openpyxl.Workbook()
sheet = wb.active
sheet.title = "Key-photo Migration status"
sheet['A' + str(1)] = 'Operation Performed On'
sheet['B' + str(1)] = 'File Name'
sheet['C' + str(1)] = 'Migrated Document Id'
sheet['D' + str(1)] = 'Status'

wb.save(excelSaveFilePath)      

for keyPhotoFile in keyPhotosFiles:
    fullPath = keyPhotosStorageLocation + keyPhotoFile #os.path.abspath(keyPhotoFile)
    splitedFileName, fileExtention = os.path.splitext(keyPhotoFile) #[1][1:]    
    fileExtention = fileExtention[1:]    
    assetId = keyPhotoFile.split('_')[0]        
    if(any(keyPhotoFile.endswith(ext) for ext in validImageExtensions)):        
        if (os.path.exists(fullPath)):            
            with open(fullPath, "rb") as f:
                filename = f.name        
                data = f.read()

                if sys.version_info < (3,0):
                    filecontents = data.encode("base64")
                else:
                    filecontents = base64.b64encode(data)
                    filecontents = filecontents.decode(encoding="utf-8", errors="strict")            
                
                filesize = os.path.getsize(fullPath)

                file_properties = Assetic3WebApiModelsFilePropertiesVM()
                file_properties.name = filename
                file_properties.file_size = filesize
                file_properties.mimetype = fileExtention
                file_properties.filecontent = filecontents
                filearray = [file_properties]

                document = Assetic3IntegrationApiRepresentationsDocumentRepresentation()
                document.key_photo = True
                document.doc_group_id = 7 #Photos items Id from DOCCBGroup table
##                document.doc_sub_category_id = documentSubCategoryId
##                document.doc_category_id = documentCategoryId            
                document.parent_type = 'ComplexAsset'
                document.parent_id = assetId            
                document.label = 'Importing complex asset key-photo - ' + filename + ' on ' + time.strftime("%d/%m/%Y %H:%M:%S")

                document.file_property = filearray
                document.mime_type = fileExtention

                print ('Uploading file: [{0}/Ext: {1}]; associated to assetId: {2}'.format(keyPhotoFile, fileExtention, assetId))            
                doc = docapi.document_post(document)
                docid = doc[0].get('Id')
                print 'Processed document/photo upload request; Response received: [{0}]'.format(docid)                     

                isDocumentUploadSuccessful = True if docid != '' else False            
                for row in range(sheet.max_row + 1, sheet.max_row + 2):
                    sheet['A' + str(row)] = time.strftime("%c")
                    sheet['B' + str(row)] = keyPhotoFile
                    if isDocumentUploadSuccessful:                
                        sheet['C' + str(row)] = docid
                        sheet['D' + str(row)] = 'SUCCESS'
                    else:
                        sheet['C' + str(row)] = 'N/A'                    
                        sheet['D' + str(row)] = 'Error; Please check integration application logs for details'
                        
                    wb.save(excelSaveFilePath)                
        else:
            print 'File: [{0}] is non-existing\n'.format(keyPhotoFile)
    else:
        print 'File: [{0}] is not valid image for key-photos upload operation and will not be processed\n'.format(keyPhotoFile)               

    
    
    
