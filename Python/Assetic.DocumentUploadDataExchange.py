import assetic
import os
import shutil
try:
    import configparser as configparser
except:
    try:
        #V2 python?
        import ConfigParser as configparser
    except:
        print ("Unable to load configparser module")
        sys.exit()

# Read ini
settings = configparser.ConfigParser()
settings.read('C:\\Projects\\Assetic\\DocumentUpload_Kevin\\Python\\assetic.ini')
# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
auth = conf.get_basic_auth_token()
#Debug settings
conf.logger_file = 'C:/Testing_Python/logger.txt'
conf.debug = False

client = assetic.ApiClient(conf.host,"Authorization",auth)

docapi = assetic.DocumentApi(client)
dataexchangeapi = assetic.DataExchangeJobApi(client)

intdir = "C:/Testing_Python/"
sourcedir = intdir + "ToLoad/"
errordir = intdir + "Error/"
processeddir = intdir + "Processed/"

files = os.listdir(sourcedir)
for filename in files:
    fullfilename = sourcedir + filename
    fileExtention = filename[filename.rindex('.') + 1:]    

    with open(fullfilename, "rb") as f:
        data = f.read()
        filecontents = data.encode("base64")
    filesize = os.path.getsize(fullfilename)
    print 'Reading File name: {0}; File Size: {2}; Extracted file extention: {1} from {3} directory'.format(filename, fileExtention, filesize, sourcedir)

    file_properties = assetic.Assetic3WebApiModelsFilePropertiesVM()
    file_properties.name = filename
    file_properties.file_size = filesize
    file_properties.mimetype = fileExtention
    file_properties.filecontent = filecontents
    filearray = [file_properties]
        
    document = assetic.Assetic3IntegrationApiRepresentationsDocumentRepresentation()
    document.label = 'Importing data-exchange document - ' + filename + ' on ' + time.strftime("%d/%m/%Y %H:%M:%S")   
    
    document.document_type = 'DataExchange'
    document.mime_type = fileExtention    
	document.file_property = filearray

    doc = docapi.document_post(document)
    docid = doc[0].get('Id')
	print ('Document: {0} is uploaded to client'.format(docid))
	
	isDocumentUploadSuccessful = True if docid != '' else False    
    #move file to in-progress, and rename to task id to allow checks on success/failure    
    if isDocumentUploadSuccessful:
		print('Requesting data-exchange job for profile: {0}/Document: {1}'.format(dataExchangeProfileId, filename))
        profileid = profiles.get(category)
		job = assetic.Assetic3WebApiModelsDataExchangeDataExchangeJobVM()
		job.profile_id = profileid
		job.document_id = docid
		print (job)
		
		task = dataexchangeapi.data_exchange_job_post(job)
		print (task)
		
		destination = processeddir + task
        print 'Moving file: {0} to processed folder: {1}\n'.format(filename, destination)                
        shutil.move(fullfilename,destination)
    else:
		destination = errordir + task
		print 'Moving file: {0} to error folder: {1}; No data-exchange job is requested for this document\n'.format(filename, errordir)
        shutil.move(fullfilename,destination)