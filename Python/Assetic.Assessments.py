"""
    Example script to apply an assessment result to a task
    (Assetic.Assessments.py)

    1. create (POST) a form result.
    2. use the generated ID in form result response to link to the
    assessment task
"""
import assetic
from datetime import datetime
from datetime import timedelta
#Assetic SDK instance
asseticsdk = assetic.AsseticSDK('c:/users/you/assetic.ini',None,'Error')
#assessment form result API
assessresultapi = assetic.AssessmentFormResultApi()

##need some identifiers. These may come from GET /api/v2/assessmenttask
##which is in the AssessmentTaskApi, use method assessment_task_get which
##returns all tasks assigned to the logged in user
taskguid = '375f575c-2b6b-e611-9469-06edd62954d7'
formguid = 'e3244b32-286b-e611-9469-06edd62954d7'
resourceguid = '36bee8ab-4e39-4f1d-b713-4eefa521fcd8'

#define the assessor resource (need both guid & name)
resource = assetic.Assetic3IntegrationRepresentationsRsResourceRepresentation()
resource.id = resourceguid
resource.display_name = 'Kevin Wilton'

##build the data.  Structure of this changes for each form
##use the form API to get structure
data = {"CONTROL0766":"1",
        "CONTROL0770":True,
        "CONTROL0771":"Tea Tree",
        "CONTROL07712":"melaleuca alternifolia"}

#build the object to create the result entry
form_result = assetic.Assetic3IntegrationRepresentationsAsmtFormResultRepresentation()
form_result.form_id = formguid
form_result.form_result_comment='created via api'
form_result.form_result_start_time = datetime.isoformat(datetime.now() +
                                                        timedelta(days=-7))
form_result.form_result_end_time = datetime.isoformat(datetime.now()
                                                      + timedelta(days=-5))
form_result.form_result_last_modified = datetime.isoformat(datetime.now())
form_result.form_result_status = 1
form_result.form_result_rs_resource_id_assessed_by=resource
form_result.data = data
##execute the post
try:
    response = assessresultapi.assessment_form_result_post(form_result)
except assetic.rest.ApiException as e:
    ##Log exception and exit
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit();    
##check for error messages
if response.get('Errors') != None:
    print(response.get('Errors'))
    exit();

##get response ID
id = response['Data'][0]['Id']
print(id)

##now link form result with task.  First create api instance
taskapi = assetic.AssessmentTaskApi()
try:
    linkresponse = taskapi.assessment_task_link_asmt_form_result(taskguid,id)
except assetic.rest.ApiException as e:
    #Log exception and exit
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit();
print (linkresponse)    #expect 'True'
