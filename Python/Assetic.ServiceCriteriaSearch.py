"""
Get a list of service criteria scores (Assetic.ServiceCriteriaSearch.py)
Search returns a paginated list of scores
Filters and sorting can be applied
"""
import assetic
##Define the location of the assetic.ini,e.g inifile = "c:/users/me/assetic.ini" 
##or leave as None to use the default locations.  
inifile = None
##Define the location of the log file, or leave as None for logging to screen
logfile = None
##Define the loglevel - usually one of "Info","Debug","Error"
loglevel = "Info"

asseticsdk = assetic.AsseticSDK(inifile,logfile,loglevel)
svc_crit_api = assetic.ServiceCriteriaApi()

##define page size (no of records) and page number to get
pagesize = 5
pagenumber = 1

searchfilter = "AssetCategory~eq~'Roads'~and~IsMostRecentScore=true"
sortorder = "AssetId-desc"
kwargs = {"request_params_page":pagenumber,
          "request_params_page_size":pagesize,
          "request_params_sorts":sortorder,
          "request_params_filters":searchfilter}

##now execute the request
try:
    response = svc_crit_api.service_criteria_get(**kwargs)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
        e.status,e.reason,e.body))
    exit()

totalresults = response.get("TotalResults")
if totalresults < 1:
    asseticsdk.logger.info("Empty response")
    exit()

##display some fields from the returned data
results = response.get("ResourceList")
for result in results:
    assetid = result["AssetId"]
    typelabel = result["ServiceCriteriaTypeLabel"]
    score = result["Score"]
    mostrecent = result["IsMostRecentScore"]
    date = result["AssessmentDate"]
    msg = "asset: {0}, type {1}, score={2}, is most recent={3}, assessment date {4}".format(
        assetid,typelabel,score,mostrecent,date)
    asseticsdk.logger.info(msg)

