"""
Assetic.ValidateConfigurationTbxScript.py
Validate the integration configuration defined in the give XML file
Not a comprehensive validation as it doesn't validate dynamic fields
Outputs a text file with validation summary
"""
import arcpy
import sys
# check if this is first time the script has been run in this arcPro session
# stops unnecessary reinitialisation of package which can double up logging.
if 'assetic_esri' not in sys.modules:
    initiate = True
else:
    # The script has been run previously so the config is already loaded
    initiate = False
import assetic_esri
import os
from assetic.tools.shared.xml_config_verifier import XMLConfigVerifier
outfile_folder = arcpy.GetParameterAsText(0)
outfile_name = arcpy.GetParameterAsText(1)


def main(outfile):
    """
    For the given layer's selected features
    create a corresponding asset in Assetic and update the feature with
    the asset friendly asset id, or the assetic internal asset guid.
    Versioned editing is assumed.  A new edit session is opened for the layer
    edit
    Assumes the xml config file has the layer name (as appears in the TOC)
    :param layer: the layer to process
    """
    # Initialise the assetic_esri library.  It will read the config files etc
    files = get_file_locations()
    if initiate:
        # Script running for first time this session so get settings
        arcpy.AddMessage("Initiating Assetic ESRI configuration and logging")
        if not initasseticesri(
                inifile=files["inifile"]
                , logfile=files["logfile"]
                , xmlfile=files["xmlfile"]):
            return

    conf_check = XMLConfigVerifier(files["xmlfile"], files["inifile"], outfile)
    conf_check.run()
    arcpy.AddMessage("Completed validation process")


def get_file_locations():
    """
    Get the file locations of inifile, logfile, xmlfile
    :return: dict of file locations
    """
    appdata = os.environ.get("APPDATA")
    inifile = os.path.abspath(appdata + "\\Assetic\\assetic.ini")
    logfile = os.path.abspath(appdata + "\\Assetic\\addin.log")
    xmlfile = os.path.abspath(appdata + "\\Assetic\\arcmap_edit_config.xml")

    return {"inifile": inifile, "logfile": logfile, "xmlfile": xmlfile}


def initasseticesri(inifile, logfile, xmlfile):
    """
    initialise the helper module assetic_esri
    sets the paths to the xml config and ini files
    """
    try:
        ae = assetic_esri.Initialise(xmlfile,inifile,logfile,"Info")
    except Exception as ex:
        arcpy.AddError("Error initialising Addin: {0}".format(ex))
        return False
    return True


if __name__ == "__main__":
    arcpy.AddMessage("Preparing to validate")
    outfile = os.path.join(outfile_folder, outfile_name)
    arcpy.AddMessage("Validation Result file to be created is: {0}".format(outfile))
    main(outfile)
