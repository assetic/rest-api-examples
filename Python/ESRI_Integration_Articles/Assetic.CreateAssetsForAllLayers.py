"""
Assetic.CreateAssetsForAllLayers.py
Script to setup as a toolbox script
Iterate ArcGIS Pro session layers and create assets in Assetic
from the selected features in each of the ArcGIS Pro layers
This example assumes script is run in an edit session
Requires assetic_esri v1.0.1.4 or later
"""
import arcpy
import sys
# check if this is first time the script has been run in this arcPro session
# it's a bit crass, but effective.
if 'assetic_esri' not in sys.modules:
    initiate = True
else:
    # The script has been run previously so the config is already loaded
    initiate = False
import assetic_esri
import os

def main():
    """
    For the given layer's selected features
    create a corresponding asset in Assetic and update the feature with
    the asset friendly asset id, or the assetic internal asset guid.
    Assumes the xml config file has the layer name (as appears in the TOC)
    :param layer: the layer to process
    """

    #Initialise the assetic_esri library.  It will read the config files etc
    if initiate == True:
        # Script running for first time this session so get settings
        arcpy.AddMessage("Initiating Assetic ESRI configuration and logging")
        if not initasseticesri():
            return
    
    # initialise assetic esri tools for layers
    tools = assetic_esri.LayerTools()

    # get current map project and iterate layers in project if feature layer
    aprx = arcpy.mp.ArcGISProject("CURRENT")
    for layer in aprx.activeMap.listLayers():
        if(layer.isFeatureLayer):

            # assetic_esri.config.force_use_arcpy_addmessage = True

            #get workspace
            #featuredesc = None
            #desc = arcpy.Describe(layer)
            #featuredesc = desc.featureClass

            if layer.getSelectionSet():
                count = len(layer.getSelectionSet())
                ##execute asset creation
                arcpy.AddMessage("Processing layer: {0}, {1} Selected Features".format(
                    layer.name,count))
                tools.create_asset(layer)
            else:
                assetic_esri.logger.info("No selection")
                arcpy.AddMessage("Skipping layer: {0}. No Selected Features".format(
                    layer.name))
    return True
        
def initasseticesri():
    """
    initialise the helper module assetic_esri
    sets the paths to the xml config and ini files
    """
    appdata = os.environ.get("APPDATA")
    inifile = os.path.abspath(appdata + "\\Assetic\\assetic.ini")
    logfile = os.path.abspath(appdata + "\\Assetic\\addin.log")
    xmlfile = os.path.abspath(appdata + "\\Assetic\\arcmap_edit_config.xml")
    try:
        ae = assetic_esri.Initialise(xmlfile,inifile,logfile,"Debug")
    except Exception as ex:
        arcpy.AddError("Error initialising assetic_esri: {0}".format(ex))
        print("Error initialising assetic_esri: {0}".format(ex))

        return False
    return True


if __name__ == "__main__":
    main()
