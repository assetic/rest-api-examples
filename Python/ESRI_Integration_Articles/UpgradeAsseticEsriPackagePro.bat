echo off
rem activate the conda environment that support adding packages
rem accepts a single parameter of the conda environment name
IF "%1"=="" (
	echo Need to provide a conda environment.
	echo example usage:
	echo UpgradeAsseticEsriPackagePro.bat arcgispro-py3-clone1
	exit /B 1	
)
call "C:\Program Files\ArcGIS\Pro\bin\Python\Scripts\activate" %1

rem now pip shoud be available in the environment
rem install/upgrade the assetic_esri package,
rem being careful not to upgrade existing dependent packages
rem unless the asset packages specify a more recent package
echo on
pip install assetic_esri --upgrade --upgrade-strategy only-if-needed
