import arcpy
import csv

# lyr = arcpy.mapping.Layer(r"C:\Projects\TAS\tas_gdb\SealedRoads.lyr")
# lyr = arcpy.mapping.Layer(r'C:/Projects/TAS/tas_gdb/tasdata.gdb/list_transport_segments')
lyr = arcpy.mapping.Layer(r'C:/Projects/TAS/tas_gdb/tasdata.gdb/list_hydro_areas')
#lyr = arcpy.mapping.Layer(r'C:/Projects/TAS/tas_gdb/workorders.lyr')
lyr = arcpy.mapping.Layer(r"C:\Projects\CVH_Gis\Updated_Roads_Asset_Data_CVH.shp")

def main():
    # set some things
    desc = arcpy.Describe(lyr)    
    shapeType = desc.shapeType
    
    outsrid = 4326
    #asset_id_field = "TRANSEG_ID"
    asset_id_field = "HYDAREA_ID"
    #asset_id_field = "wkoguid"
    asset_id_field = "Asset_ID"
    filename = r"c:\temp\{0}.csv".format(lyr.name)

    # apply a filter to limit data for upload
    # set to null if processing all features in layer
    #where_clause = "HYDAREA_ID>{0}".format(2419117)
    where_clause = None

    # will need to project to EPSG4326 for output
    tosr = arcpy.SpatialReference(outsrid)

    # define the data to get from search cursor
    fields = ["SHAPE@", "SHAPE@WKT", asset_id_field]
        
    with open(filename, 'wb') as csvfile:
        wktwriter = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        # write header row
        wktwriter.writerow(["Complex Asset Id","Point","Polygon","Line"])
        
        polygonwkt=""
        linewkt=""
        pointwkt=""

        # loop through records
        for row in arcpy.da.SearchCursor(lyr, fields, where_clause):
            # get geomtry and process
            geometry = row[fields.index('SHAPE@')]
            new_geom = geometry.projectAs(tosr)
            if shapeType == "Polygon":
                polygonwkt = new_geom.WKT
            elif shapeType == "Polyline":
                linewkt = new_geom.WKT

            # define the point for all feature types
            pt_geometry = arcpy.PointGeometry(geometry.centroid
                                              , geometry.spatialReference)
            pointwkt = pt_geometry.projectAs(tosr).WKT
            # write line
            wktwriter.writerow([row[fields.index(asset_id_field)], pointwkt,
                                    polygonwkt, linewkt])
            
if __name__ == "__main__":
    main()
