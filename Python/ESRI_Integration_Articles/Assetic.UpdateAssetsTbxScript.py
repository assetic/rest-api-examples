"""
Assetic.UpdateAssetsTbxScript.py
Script to setup as a toolbox script for use in Model Builder
update assets in Assetic from the selected features in ArcGIS layer
Assume Model Builder model has applied a feature selection
This example assumes there is no geoadatabase versioning and will not initiate
an edit session to perform edits
Requires assetic_esri v1.0.1.4 or later
"""
import arcpy
import assetic_esri
import os

layer = arcpy.GetParameter(0)


def main(layer):
    """
    For the given layer's selected features
    update the corresponding asset in Assetic
    Versioned editing is assumed.
    A new edit session is opened for the layer
    edit
    Assumes the xml config file has the layer name (as appears in the TOC)
    :param layer: the layer to process
    """
    # Initialise the assetic_esri library.  It will read the config files etc
    if not initasseticesri():
        return

    # Uncomment to following if using arcMap and you don't want messages to
    # go to pythonaddins.MessageBox(), assetic_esri instead uses
    # arcpy.AdMessage() to send messages
    #assetic_esri.config.force_use_arcpy_addmessage = False

    # initialise assetic esri tools for layers
    tools = assetic_esri.LayerTools()


    # get record count
    count = len(layer.getSelectionSet())

    # execute asset update
    arcpy.AddMessage("Processing layer: {0}, {1} Selected Features".format(
        layer.name, count))
    tools.update_assets(layer)

    # uncomment if arcMap to reset message output in case addin is used next
    #assetic_esri.config.force_use_arcpy_addmessage = False


def initasseticesri():
    """
    initialise the helper module assetic_esri
    sets the paths to the xml config and ini files
    """
    appdata = os.environ.get("APPDATA")
    inifile = os.path.abspath(appdata + "\\Assetic\\assetic.ini")
    logfile = os.path.abspath(appdata + "\\Assetic\\addin.log")
    xmlfile = os.path.abspath(appdata + "\\Assetic\\arcmap_edit_config.xml")
    try:
        ae = assetic_esri.Initialise(xmlfile, inifile, logfile, "Info")
    except Exception as ex:
        arcpy.AddError("Error initialising assetic_esri: {0}".format(ex))
        return False
    return True


if __name__ == "__main__":
    main(layer)
