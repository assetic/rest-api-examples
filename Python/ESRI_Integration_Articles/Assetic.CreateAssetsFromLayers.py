"""
Assetic.CreateAssetsFromLayers.py
Script to create assets in Assetic from features in one or more ArcGIS layers
This example assumes a versioned enterprise geodatabase
Requires asset_esri v1.0.1.4 or later
"""
import arcpy
import assetic_esri
import os


def main():
    """
    Initialise the assetic_esri library.  It will read the config files etc
    """
    if not initasseticesri():
        return
    
    # define layers to process (one or more), and the search query that
    # identifies features to process as new assets
    layers = list()

    # define the query string for the layer we are adding
    query = "assetid is null"  # process fields where assetid is NULL
    # for testing, uncomment this line and specify a single record by id field
    #query = "UFI = 'rds002720978'"
    # add the layer to the list
    layers.append(("C:\Projects/TAS/SealedRoads_sde.lyr",query))
    
    # more layers may be added using layers.append((layername, query)) format
    
    # loop through layer list and process new assets
    for layer in layers:
        create_assets_for_layername(layer[0],layer[1])


def create_assets_for_layername(layername, query=None):
    """
    For the given layername and query filter process selected features
    to create a corresponding asset in Assetic and update the feature with
    the asset friendly asset id, or the assetic internal asset guid.
    Versioned editing is assumed.  A new edit session is opened for the layer
    edit
    Assumes the xml config file has the layer name (as appears in the TOC)
    :param layername: the file path and name of the layer file
    :param query: optional, but if omitted all assets are processed.
    example query strings:'assetid is null' or "externalid = ''"
    """

    # initialise assetic esri tools for layers
    tools = assetic_esri.LayerTools()

    # get workspace
    desc = arcpy.Describe(layername)
    lyr = None
    if desc.dataType == "Layer":
        # create an instance of the layer using the name
        lyr = arcpy.mapping.Layer(layername)
    else:
        # need a layer
        print("no layer defined")
        return

    # get the workspace
    arcpy.env.workspace = desc.Path

    # create edit session. If there is a workspace error try setting it to
    # the folder containing the connection.  Make sure you also import os at
    # the start.
    # arcpy.env.workspace = os.path.dirname(desc.Path)
    edit = arcpy.da.Editor(arcpy.env.workspace)
    # start edit session
    edit.startEditing(False,True)
    edit.startOperation()
    ##execute asset creation
    tools.create_asset(lyr, query)
    ##finalise
    edit.stopOperation()
    edit.stopEditing(True)


def initasseticesri():
    """
    initialise the helper module assetic_esri
    sets the paths to the xml config and ini files
    """
    appdata = os.environ.get("APPDATA")
    inifile = os.path.abspath(appdata + "\\Assetic\\assetic.ini")
    logfile = os.path.abspath(appdata + "\\Assetic\\addin.log")
    xmlfile = os.path.abspath(appdata + "\\Assetic\\arcmap_edit_config.xml")
    try:
        ae = assetic_esri.Initialise(xmlfile,inifile,logfile,"Info")
    except Exception as ex:
        print("Error initialising Addin: {0}".format(ex))
        return False
    return True


if __name__ == "__main__":
    main()
