import arcpy
import csv
import os


def process(asset_id_fld,output_folder):
    """
    Build Data Excahnge import file for spatial data
    Iterates over all layers and prociesses visible feature layers
    Outputs a CSV file per layer with Asset ID and WKT in 4326 projection
    CSV file name taken from layer name
    :param asset_id_fld: The attribute field name containing Asset Id
    :param output_folder: The output folder to write the files to
    """
    tosr = arcpy.SpatialReference(4326)
    arcpy.SetProgressorLabel("Export to WKT")
    aprx = arcpy.mp.ArcGISProject("CURRENT")
    for layer in aprx.activeMap.listLayers():
        if not layer.isFeatureLayer or not layer.visible:
            # not a feature layer or not visible so go to next layer
            continue
        
        #if layer.is3DLayer:
        #    pass
        
        # get some info about the layer
        desc = arcpy.Describe(layer)
        shape_type = desc.shapeType
        try:
            cnt_total = int(arcpy.GetCount_management(layer).getOutput(0))
        except Exception:
            arcpy.AddMessage(
                "Unable to get layer feature count for {0}".format(layer.name))
            # go to next layer
            continue
            
        # Use progressor to feedback progress
        arcpy.SetProgressor("step", layer.name,
                0, cnt_total, 1)
        arcpy.SetProgressorPosition()

        # prepare a dict to hold WKT for each layer prior to writing to CSV
        spatial_rows = list()

        # get features in a cursor
        arcpy.management.SelectLayerByAttribute(layer, "CLEAR_SELECTION")
        cursor = arcpy.da.SearchCursor(
            layer, [asset_id_fld, "SHAPE@"])
        arcpy.AddMessage(asset_id_fld)
        # iterate features
        for row in cursor:

            row_dict = dict()
            polygonwkt = ""
            linewkt = ""

            # get asset ID
            try:
                asset_id = str(row[0])
            except Exception:
                arcpy.AddError("Skipping record with non-string asset id")
                continue

            # get geometry and project to wgs84
            geometry = row[1]
            new_geom = geometry.projectAs(tosr)

            # different output based of feature type
            if shape_type == "Polygon":
                polygonwkt = new_geom.WKT.replace(" NAN","").replace(" ZM","")
            elif shape_type == "Polyline":
                linewkt = new_geom.WKT.replace(" NAN","").replace(" ZM","")

            # define the point for all feature types
            pt_geometry = arcpy.PointGeometry(geometry.centroid
                                              , geometry.spatialReference)
            pointwkt = pt_geometry.projectAs(tosr).WKT.replace(" NAN","")

            row_dict["Asset ID"] = asset_id
            row_dict["Point"] = pointwkt
            row_dict["Polygon"] = polygonwkt
            row_dict["Line"] = linewkt
            spatial_rows.append(row_dict)

        # write csv file if there were features processed
        if len(spatial_rows) > 0:
            keys = spatial_rows[0].keys()
            out_file_name = os.path.join(output_folder, layer.name + ".csv")
            arcpy.AddMessage(
                "Preparing to write file {0}".format(out_file_name))
            with open(out_file_name, 'w', newline='') as output:
                dict_writer = csv.DictWriter(output, keys)
                dict_writer.writeheader()
                dict_writer.writerows(spatial_rows)
            #Finalise messaging to progressor
            arcpy.AddMessage("Created file {0}".format(out_file_name))
            arcpy.SetProgressorLabel("Completed {0}".format(layer.name))


if __name__ == "__main__":
    process(asset_id_fld=arcpy.GetParameterAsText(0)
            ,output_folder=arcpy.GetParameterAsText(1))
