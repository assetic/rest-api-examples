"""
Assetic.CreateAssetsTbxScript.py
Script to setup as a toolbox script for use in Model Builder
create assets in Assetic from the selected features in ArcGIS layer
Assume Model Builder model has applied a feature selection
This example assumes a versioned geodatabase
Requires asset_esri v1.0.1.4 or later
"""
import arcpy
import sys
# check if this is first time the script has been run in this arcPro session
# stops unnecessary reinitialisation of package which can double up logging.
if 'assetic_esri' not in sys.modules:
    initiate = True
else:
    # The script has been run previously so the config is already loaded
    initiate = False
import assetic_esri
import os

layer = arcpy.GetParameter(0)


def main(layer):
    """
    For the given layer's selected features
    create a corresponding asset in Assetic and update the feature with
    the asset friendly asset id, or the assetic internal asset guid.
    Versioned editing is assumed.  A new edit session is opened for the layer
    edit
    Assumes the xml config file has the layer name (as appears in the TOC)
    :param layer: the layer to process
    """
    # Initialise the assetic_esri library.  It will read the config files etc
    if initiate == True:
        # Script running for first time this session so get settings
        arcpy.AddMessage("Initiating Assetic ESRI configuration and logging")
        if not initasseticesri():
            return  

    ##initialise assetic esri tools for layers
    tools = assetic_esri.LayerTools()

    #set workspace
    desc = arcpy.Describe(layer)
    count = len(layer.getSelectionSet())
    arcpy.env.workspace = desc.path
    # create edit session.   If there is a workspace error try setting it to
    # the folder containing the connection.  Make sure you also import os at
    # the start.
    # arcpy.env.workspace = os.path.dirname(desc.path)
    edit = arcpy.da.Editor(arcpy.env.workspace)
    # Initiate edit session
    edit.startEditing(False, True)
    edit.startOperation()
    # execute asset creation
    arcpy.AddMessage("Processing layer: {0}, {1} Selected Features".format(
        layer.name,count))
    tools.create_asset(layer)
    # finalise
    edit.stopOperation()
    edit.stopEditing(True)


def initasseticesri():
    """
    initialise the helper module assetic_esri
    sets the paths to the xml config and ini files
    """
    appdata = os.environ.get("APPDATA")
    inifile = os.path.abspath(appdata + "\\Assetic\\assetic.ini")
    logfile = os.path.abspath(appdata + "\\Assetic\\addin.log")
    xmlfile = os.path.abspath(appdata + "\\Assetic\\arcmap_edit_config.xml")
    try:
        ae = assetic_esri.Initialise(xmlfile,inifile,logfile,"Info")
    except Exception as ex:
        print("Error initialising Addin: {0}".format(ex))
        return False
    return True


if __name__ == "__main__":
    main(layer)
