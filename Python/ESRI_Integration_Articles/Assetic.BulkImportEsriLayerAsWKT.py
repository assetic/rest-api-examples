"""
    Get the spatial definition of features that match the search filter
    and upload to Assetic via Data Exchange
    (Assetic.BulkImportEsriLayerAsWKT)
"""
import arcpy
import csv
import six
import io
import base64
import assetic

# Define the layer to import (can be a layer file, shp file, or file gdb layer)
layer = arcpy.mapping.Layer(
    r"C:\Projects\TAS\tas_gdb\tasdata.gdb\list_transport_segments")

# apply a filter to limit data for upload
# set to null if processing all features in layer
# search_filter = "HYDAREA_ID>{0}".format(2419117)
# search_filter = None
search_filter = "assetname='{0}'".format("Trainer Street")
# define the field in the layer corresponding to the Assetic asset ID
asset_id_field_name = "assetid"
# Initialise Assetic SDK instance as a global variable
asseticsdk = assetic.AsseticSDK(r"C:\Users\kwilton\assetic.ini", None, 'Info')


def main(lyr, asset_id_field, where_clause=None):
    """
    Get the spatial definition of features that match the search filter
    and upload to Assetic via Data Exchange.
    Does not verify if Data Exchange Job is successful.
    :param lyr: an ESRI layer of type arcpy.mapping.Layer to process
    :param asset_id_field: the field in the layer that is the Assetic Asset ID
    :param where_clause: a search filter on the layer
    :return: Data Exchange Job ID if success, else None if error
    """
    # get some info about the layer
    desc = arcpy.Describe(lyr)    
    shape_type = desc.shapeType

    # will need to project to EPSG4326 for output
    tosr = arcpy.SpatialReference(4326)

    # define the data to get from search cursor
    fields = ["SHAPE@", "SHAPE@WKT", asset_id_field]

    # set csv writer output to string
    if six.PY2:
        output = io.BytesIO()
        wktwriter = csv.writer(output)
    else:
        output = io.StringIO()
        wktwriter = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)

    # write header row
    wktwriter.writerow(["Complex Asset Id", "Point", "Polygon", "Line"])

    polygonwkt = ""
    linewkt = ""
    pointwkt = ""

    record_count = 0
    # loop through records
    for row in arcpy.da.SearchCursor(lyr, fields, where_clause):
        # get geometry and process
        geometry = row[fields.index('SHAPE@')]
        new_geom = geometry.projectAs(tosr)
        if shape_type == "Polygon":
            polygonwkt = new_geom.WKT
        elif shape_type == "Polyline":
            linewkt = new_geom.WKT

        # define the point for all feature types
        pt_geometry = arcpy.PointGeometry(geometry.centroid
                                          , geometry.spatialReference)
        pointwkt = pt_geometry.projectAs(tosr).WKT
        # write line
        wktwriter.writerow([row[fields.index(asset_id_field)], pointwkt,
                                polygonwkt, linewkt])
        asseticsdk.logger.info("Processing asset {0}".format(
            row[fields.index(asset_id_field)]))
        record_count += 1
    if record_count == 0:
        return None
    # upload csv data to Assetic
    doc_id = upload_csv_string(output.getvalue(), lyr)
    if doc_id:
        # create data exchange job
        dx_id = run_data_exchange_for_doc(doc_id)
        return dx_id
    return None


def upload_csv_string(csv_data, lyr):
    """
    For the given string of csv data upload to Assetic as a document
    :param csv_data: The csv data as a string
    :param lyr: The layer being processed - use to derive file name
    :return: Assetic document ID or None if error
    """
    doc_api = assetic.DocumentApi()

    if six.PY2:
        filecontents = csv_data.encode("base64")
    else:
        filecontents = base64.b64encode(csv_data)
        filecontents = filecontents.decode(encoding="utf-8", errors="strict")
    file_properties = assetic.Assetic3IntegrationRepresentationsFilePropertiesRepresentation()
    file_properties.name = "{0}.csv".format(lyr.name)
    file_properties.mimetype = "csv"
    file_properties.filecontent = filecontents
    filearray = [file_properties]

    # Doc categorisation
    document = assetic.Assetic3IntegrationRepresentationsDocumentRepresentation()
    # Can set the following to make it easier to find the uploaded documents
    # if searching for them.
    document.document_group = 1
    document.document_category = 9
    document.document_sub_category = 7
    document.file_property = filearray
    document.is_key_photo = False

    # Perform upload
    try:
        doc = doc_api.document_post(document)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error('Status {0}, Reason: {1} {2}'.format(
            e.status, e.reason, e.body))
    doc_id = None
    if len(doc) > 0:
        doc_id = doc[0].get('Id')
    return doc_id


def run_data_exchange_for_doc(doc_id):
    """
    Import the spatial data defined in the document as a spatial data import
    via Data Exchange API
    :param doc_id: The Assetic document ID
    :return: Data Exchange Job representation or None if error
    """
    dx_job_api = assetic.DataExchangeJobApi()
    dx_no_profile_rep = \
        assetic.Assetic3IntegrationRepresentationsDataExchangeJobNoProfileRepresentation(
            doc_id, "Mapping", "ComplexAssetSpatialLocation")
    dx_job = None
    try:
        dx_job = dx_job_api.data_exchange_job_post_0(dx_no_profile_rep)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error('Status {0}, Reason: {1} {2}'.format(
            e.status, e.reason, e.body))
    return dx_job


if __name__ == "__main__":
    dx_job_rep = main(layer, asset_id_field_name, search_filter)
    asseticsdk.logger.info("Data Exchange Job = {0}".format(dx_job_rep))
