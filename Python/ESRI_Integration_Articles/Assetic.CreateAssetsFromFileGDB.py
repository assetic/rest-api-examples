"""
Assetic.CreateAssetsFromFileGDB
Sample script to create assets in Assetic corresponding with selected
features from a file geodatabase
"""
import assetic_esri
import arcpy

# Define the feature class in the file geodatabase.  Could alternatively assign
# a layer (.lyr)file
lyr = arcpy.mapping.Layer(r'C:\Projects\assets.gdb\roads')

# Define file location for log and ini file with Assetic credentials and
# integration configuration file
inifile = r"c:\Assetic\assetic.ini"
logfile = r"c:\Assetic\addin.log"
xmlfile = r"c:\Assetic\arcmap_edit_config.xml"
ae = assetic_esri.Initialise(xmlfile, inifile, None, "Info")
tools = assetic_esri.LayerTools()

# define the query string for the layer we are adding
where_clause = "assetid is null"  # process fields where assetid is NULL

# clear any current selections and apply the new query.  Only features selected
# by the query are processed
arcpy.SelectLayerByAttribute_management(lyr, "CLEAR_SELECTION")
arcpy.SelectLayerByAttribute_management(lyr, "NEW_SELECTION", where_clause)

# Now create assets in Assetic for the selected features
tools.create_asset(lyr)
