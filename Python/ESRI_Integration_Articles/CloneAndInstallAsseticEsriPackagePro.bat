rem clone the root (read-only) conda that comes with ArcGIS Pro
"C:\Program Files\ArcGIS\Pro\bin\Python\Scripts\conda.exe" create --clone arcgispro-py3 --name arcgispro-py3-clone1

rem activate the new conda clone environment that support adding packages
call "C:\Program Files\ArcGIS\Pro\bin\Python\Scripts\activate" arcgispro-py3-clone1

rem now install assetic package
pip install assetic_esri
