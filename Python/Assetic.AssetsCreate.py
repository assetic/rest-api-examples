"""
Create an asset (Assetic.AssetsCreate.py)
"""
import assetic
#Define location of configuration file, log file, log level
asseticsdk=assetic.AsseticSDK("C:/users/kwilton/assetic.ini",
                              None,"Debug")
assetapi = assetic.ComplexAssetApi()

#api has a predefined set of core fields
#can define additional asset attribute fields
#attribute field names are the internal field names, get using metadata api

#create an instance of the complex asset object
asset = assetic.Assetic3IntegrationRepresentationsComplexAssetRepresentation()
##mandatory fields
asset.asset_category_id='246da33d-6544-e411-82fb-f4b7e2cd8977'  #roads
#asset.asset_category = 'Roads'
asset.status = 'Active'
asset.asset_id = 'RINT07'
asset.asset_name = 'Kevin Road 7'
##optional core fields
asset.asset_class = 'Transport'
asset.asset_sub_class = 'Road'
asset.asset_type = 'Unsealed Road'
asset.asset_sub_type = 'Gravel'
asset.asset_external_identifier = "ext07"
asset.asset_criticality = "Arterial"
asset.asset_maintenance_type = "Road"
asset.asset_maintenance_sub_type = "Arterial Road"
asset.asset_work_group = "Roads"
##add attributes as a dictionary of field name & field value pairs
asset.attributes = {"Comment":"a comment","Link":"www.assetic.com"}

##now execute the request
try:
    response = assetapi.complex_asset_post(asset)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()

if response == None:
    asseticsdk.logger.info("Empty response")
    exit()

data = response.get("Data")
if data == None:
    asseticsdk.logger.info("Asset not created")
    exit()

##display the returned asset data
for newasset in data:
    assetid = newasset.get("AssetId")
    assetguid = newasset.get("Id")
    assetname = newasset.get("AssetName")
    msg = "guid: {0}, asset {1}, name {2}".format(
        assetguid,assetid,assetname)
##now get the asset by specifying asset guid
try:
    response = assetapi.complex_asset_get(assetguid,["Zone","Locality"])
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()
if response == None:
    asseticsdk.logger.info("Empty response")
    exit()

#display the returned info
assetid = response.get("AssetId")
assetguid = response.get("Id")
assetname = response.get("AssetName")
attributes = response.get("Attributes")
zone = attributes.get("Zone")
locality = attributes.get("Locality")
asseticsdk.logger.info("Now get the last asset in the list by asset guid")
msg = "guid: {0}, asset {1}, name {2}, zone {3}, locality {4}".format(
    assetguid,assetid,assetname,zone,locality)
asseticsdk.logger.info(msg)

##get the asset by specifying user friendly asset id
try:
    response = assetapi.complex_asset_get(assetid,["Zone","Locality"])
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()
if response == None:
    asseticsdk.logger.info("Empty response")
    exit()

#display the returned info
assetid = response.get("AssetId")
assetguid = response.get("Id")
assetname = response.get("AssetName")
attributes = response.get("Attributes")
zone = attributes.get("Zone")
locality = attributes.get("Locality")
msg = "Now get the last asset in the list by user friendly asset ID"
asseticsdk.logger.info(msg)
msg = "guid: {0}, asset {1}, name {2}, zone {3}, locality {4}".format(
    assetguid,assetid,assetname,zone,locality)
asseticsdk.logger.info(msg)

