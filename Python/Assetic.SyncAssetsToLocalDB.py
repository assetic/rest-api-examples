"""
    Assetic.SyncAssetsToLocalDB.py
    Uses an advanced search profile to get all date reurned by the search
    Data is written to a temp db table and then the 'merge' SQL is used
    to update/insert records in the permanant table
    Will create the tables as required (TODO check columns exist)
    Writes to a log table the changed record (TODO expand the output)
"""
import pypyodbc
import sys
import assetic   #This is an SDK to wrap the REST endpoints
import datetime
import dateutil.parser
import configparser as configparser
import logging
import math
import functools
from contextlib import contextmanager
# Script Variables
# Read ini
settings = configparser.ConfigParser()
settings.read('c:/users/kwilton/assetic.ini')

# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
#Debug settings
conf.logger_file = 'c:/temp/create_request.txt'

conf.debug = False
if conf.debug == True:
    logging.basicConfig() 
    logging.getLogger().setLevel(logging.DEBUG)

##Database details
dbserver = "db"
dbname = "scratch"
logtable = "synclog"
keyfield = "[Asset Id]"
##search profile ID

searchguid = "5c6f3ac0-7179-e611-946c-06edd62954d7"
## set the table each profile relates to
profile_to_table_dict = {"20ca50db-c45d-e511-944d-06edd62954d7":"alldata",
                         "f08b7f20-f473-e611-946b-06edd62954d7":"syncaddress",
                         "5c6f3ac0-7179-e611-946c-06edd62954d7":"corefields"}

##Assetic SDK instance
auth = conf.get_basic_auth_token()
client = assetic.ApiClient(conf.host,"Authorization",auth)
conf.api_client = client
    

##Define functions.  End of this script initiates the process that uses these functions 

@contextmanager
def open_db_connection(connection_string, commit=False):
    """
    Use contextmanager to manage connections so we cleanup connection
    as we go to avoid database locking issues on exception
    """
    connection = pypyodbc.connect(connection_string)
    cursor = connection.cursor()
    try:
        yield cursor
    except pypyodbc.DatabaseError as err:
        cursor.execute("ROLLBACK")
        raise err
    except Exception as ex:
        print(ex)
    else:
        if commit:
            cursor.execute("COMMIT")
        else:
            cursor.execute("ROLLBACK")
    finally:
        connection.close()

def runsearchforpages(searchguid, startpage, pagelimit, client):
    ##create instance of search api
    sapi = assetic.SearchApi(client)

    ##set parameters for search
    numpagesize = 500  #max number of pagess
    kw = {'request_params_id':searchguid,
        'request_params_page':startpage,
        'request_params_page_size':numpagesize}

    ##Get first page of results
    sg = sapi.search_get(**kw)

    morepages = True
    totalresults=int(sg.get('TotalResults'))
    if totalresults == 0:
        morepages = False
        return None,None,morepages
    numpages = int(sg.get('TotalPages')) - (startpage-1)
    print('Total Pages: {0}'.format(numpages))
    if pagelimit > int(numpages):
        ##page limit defines the number of pages we'll process before adding
        ##to DB
        pagelimit = numpages
    if numpages <= pagelimit:
        morepages = False
    endpage = (startpage-1) + pagelimit
    print("Start page: {0}, End page: {1}, pages left: {2}".format(startpage, endpage,numpages))
    
    ##get actual data from nested output
    resourcelist = sg.get('ResourceList')
    resource = resourcelist[0]
    data = resource.get('Data')
    ##Get a list of columns
    mappings = resource.get('Mappings')
    columns = list(mappings.keys())
    print("first page, columns are: {0}".format(columns))
    ##Copy as alldata becuase we will be appending to this
    alldata = data

    ##Get a list of columns
    #columns = map( lambda x: x.keys(), data )
    #columns = map( lambda x: x.keys(), mappings )
    #if sys.version_info < (3,0):
    #    columns = reduce( lambda x,y: x+y, columns )
    #else:
    #    columns = functools.reduce( lambda x,y: x|y, columns )

    ##Write list of columns to the 'all' list as this may grow
    allcolumns = columns

    ##Now loop through remaining pages
    if numpages > 1:
        for pagenum in range(startpage + 1,endpage +1):
            ##set page number to get
            kw['request_params_page']=pagenum
            print('Page: {0}'.format(kw['request_params_page']))

            ##Now get results for this page
            sg = sapi.search_get(**kw)
            totalresults=int(sg.get('TotalResults'))
            if totalresults == 0:
                morepages = False
                print("no more data")
                break
            ##get actual data from nested output
            resourcelist = sg.get('ResourceList')
            resource = resourcelist[0]
            data = resource.get('Data')
            mappings = resource.get('Mappings')
            columns = list(mappings.keys())
            #alldata = alldata + data
            #Get column list for this page - there may be new columns
            #columns = map( lambda x: x.keys(), data )
            #columns = map( lambda x: x.keys(), mappings )
            print("in page loops, columns are: {0}".format(columns))
            if sys.version_info < (3,0):
                #columns = reduce( lambda x,y: x+y, columns )
                ##Add new column list to 'allcolumns', will get unique list later
                allcolumns = allcolumns + columns
                #append new data to 'alldata'
                alldata = alldata + data
            else:
                #columns = functools.reduce( lambda x,y: x|y, columns )
                ##merge column list sets
                #allcolumns = allcolumns | columns
                allcolumns.extend(columns)
                #append new data to 'alldata'
                alldata.extend(data)

            if startpage + pagenum > 1000:
                ##catchall escape
                morepages = False
                break
    return allcolumns,alldata,morepages

def data_to_db(allcolumns,alldata,dbtable,keyfield,logtable):
    #write data to database
    #get unique list of columns
    columns = list( set( allcolumns ) )
    #print("Start data to db, columns are : {0}".format(columns))

    #name for tmp table we'll write data dump to
    tmpdbtable = "#{0}_tmp".format(dbtable)

    ##First get the database table column names and build the various tables
    ##TODO add a check for existing tables to make sure all columns present
    insertcolumns = []
    updatecolumns = []
    parammark = []  #add a "?" for each column to use in sql parameter settings
    for column in columns:
        insertcolumns.append("[{0}]".format(column))
        updatecolumns.append("a.[{0}]=c.[{0}]".format(column))
        parammark.append("?")

    sqltblchktmp =  "select object_id('{0}')".format(tmpdbtable)
    sqltruncatetmp = "truncate table {0}".format(tmpdbtable)
    sqlcreatetmp = """create table {0} ({1} nvarchar(500) COLLATE
        database_default)""".format(tmpdbtable,
        " nvarchar(500) COLLATE database_default,".join(insertcolumns ))
    sqltblchk =  "select object_id('{0}')".format(dbtable)
    sqlcreate = """create table {0} ({1} nvarchar(500))
        """.format(dbtable,"nvarchar(500),".join(insertcolumns ))
    sqllogchk = "select object_id('{0}')".format(logtable)
    sqllogcreate = """CREATE TABLE {0}(
	[action] [nvarchar](500),
	[Asset Id] [varchar](200),
	[syncdate] [datetime])""".format(logtable)
    ##Now create insert string
    sqlinsert = "insert into {0} (".format(tmpdbtable)
    sqlinsert = sqlinsert + ",".join(insertcolumns ) + ") values ("
    sqlinsert = sqlinsert + ",".join(parammark ) + ")"

    ##Now add data to sql insert string and do insert
    ##(using 'insertmany' effectively row by row)
    allrows = []
    ##print(columns)
    for i_r in alldata:
        #map data to column list by key to avoid issues with column order
        allrows.append(tuple(map( lambda x: i_r.get( x, "" ), columns )))
    try:
        allrows = list(set(allrows))
    except Exception as ex:
        print(str(ex))
        exit()
    ##now refresh base table from temp table where there are changes
    ##write the change to the archive file
    ##TODO output all columns for updated record (original record)
    sqldiff = """
    WITH diff as ( select {2}
        FROM {0}
        except select {2}
        FROM {1}
        )
        MERGE {1} as a using diff as c on
        (c.{5}=a.{5})
        when MATCHED then update set {3}
        When not matched then insert
        ({2}) 
        values ({2})
        output $action,inserted.[Asset Id],getdate() into {4};
    """.format(tmpdbtable,dbtable,",".join(insertcolumns),
               ",".join(updatecolumns),logtable,keyfield)

    ##Now execute all the sql statements in the one transaction block
    ##Bit slow opening & closing db connection, but transaction block works well
    try:
        with open_db_connection(connstr,True) as cursor:
            #check if temportaty table exists
            cursor.execute(sqltblchktmp)
            row = cursor.fetchone()
            if row[0] == None:
                cursor.execute(sqlcreatetmp)
            else:
                cursor.execute(sqltruncatetmp)

            #check if permanent table exists
            cursor.execute(sqltblchk)
            row = cursor.fetchone()
            if row[0] == None:
                #table not found - create permament table for current data
                cursor.execute(sqlcreate)
            #check if log table exists
            cursor.execute(sqllogchk)
            row = cursor.fetchone()
            if row[0] == None:
                #table not found - create permament table for logging changes
                cursor.execute(sqllogcreate)
            ##write all the data to the temp table in db.
            cursor.executemany(sqlinsert,allrows)
            ##execute diff statement to populate permanent tables from temp table
            cursor.execute(sqldiff)
    except Exception as ex:
        print(ex)
        return False
    return True
    
####End of functions################################

##set pagesize, max is 500
numpagesize = 500

##get tablename based on profile
try:
    dbtable = profile_to_table_dict.get(searchguid)
except:
    exit()
    
#can use UID=user;PWD=password instead of trusted connection.
#Provide APP to all dba to see what this connection is
connstr = "Driver={SQL Server};" + """Server={0};Database={1};
    Trusted_Connection=True;
    APP=Assetic Asset Sync""".format(dbserver,dbname)

pagelimit = 22
startpage = 1
morepages = True
while morepages == True:
    print(startpage)
    allcolumns,alldata,morepages = runsearchforpages(searchguid, startpage, pagelimit, client)
    startpage = startpage + pagelimit
    ##catchall escape
    if alldata == None or startpage/pagelimit > 30000:
        morepages = False

    success = data_to_db(allcolumns,alldata,dbtable,keyfield,logtable)
    if success == False:
        print("existing prematurely due to exception in database process")
        morepages = False

