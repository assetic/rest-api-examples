import requests
import configparser

# Read Config File
settings = configparser.ConfigParser()
settings.read('/Users/joel/assetic.ini')

# Script Variables
env_url = settings.get('environment', 'url')
headers = {
    'X-Auth-API-Key': settings.get('auth', 'api_key'),
    'X-Auth-Subdomain': settings.get('environment', 'subdomain')
}
basic_auth = settings.get('auth', 'username'), settings.get('auth', 'password') # REMOVE ME

# Open Assetic API Session
session = requests.Session()
session.headers.update(headers)
session.auth = basic_auth # REMOVE ME

# Output a Roles Permission Matrix
response = session.get(env_url + '/api/RoleApi')

if response.status_code != 200:
    print('Response failed with error {}'.format(response.status_code))
    exit()

# Decode the JSON response into a dictionary and use the data
role_data = response.json()

# Sort the listing of Roles Alphabetically
def extract_role(json):
    try:
        return json['Name'].lower()
    except KeyError:
        return 0

role_data.sort(key=extract_role, reverse=False)

# Generate Table

output = "<h2>Permissions</h2>\n<p>The following permissions are applied at a sub-module level for all User Roles.</p>\n<ul>\n"
output += "<li><b>None (-):</b> Cannot Access</li>\n"
output += "<li><b>Read:</b> Can Search and View Only</li>\n"
output += "<li><b>Create:</b> Can Search, View and Create New Only</li>\n"
output += "<li><b>Update:</b> Can Search, View, Create New and Edit Existing [Officer Level]</li>\n"
output += "<li><b>Delete:</b> Can Search, View, Create New, Edit Existing & Delete [Manager / Admin Level]</li>\n</ul>\n"

# Generate Roles Listing HTML
output += '<h2>User Roles</h2>\n<ul>\n<p>The following Roles are pre-packaged within Assetic. Each Role has permissions restricted at a sub-module level.</p>'

for role in role_data:
    if role['Name'] != 'admin':
        output += '<li><a class=\"xref\" href=\"#' + role['Name'] + '\">' + role['Name'] + '</a></li>\n'  # add each name to the output variable

output += '</ul>\n'

output += '<div class=\"note\"><p><b>Note:</b> When assigning Roles to Users they can be stacked together. Stacking roles together will utilise the highest possible access across all applied Roles.</p><p>Please also refer to <a href=\"https://assetic.zendesk.com/hc/en-us/articles/213109768-Roles-and-Work-Groups-Overview\">Roles and Work Groups</a> for information on how Work Groups can be used to further limit available records.</p><p>There is a maximum of 10 client admin accounts allowed.</p></div>\n'

# Generate Claims Table HTML

for role in role_data:
    if role['Name'] != "admin":
        response = session.get(env_url + '/api/RoleApi/' + role['Id'] + '/ClaimType')
        if response.status_code != 200:
            print('Response failed with error {}'.format(response.status_code))
            exit()
        output += '<div id=\"' + role['Name'] + '\">\n<h3>' + role['Name'] + '</h3>\n'
        output += '<table>\n<tr><td><b>Module</b></td><td><b>Sub Module</b></td><td><b>Permissions</b></td></tr>\n'

        claims_data = response.json()
        claims_data = claims_data["Data"]

        # Sort the listing of Roles Alphabetically
        def extract_claim(json):
            try:
                return str(json['Group']).lower() + str(json['Description']).lower()
            except KeyError:
                return 0

        claims_data.sort(key=extract_claim, reverse=False)

        for claim in claims_data:
            description = str(claim["Description"])
            if description != "Assetic Admin Support":
                claim_value = str(claim["ClaimValue"])
                if claim_value == "None":
                    claim_value = "-"
                if claim_value != "-":
                    output += '<tr><td>' + str(claim["Group"]) + '</td><td>' + description + '</td><td>' + claim_value + '</td></tr>\n'

        output += '</table></div>\n'

with open('claims_tables.html', mode='w', encoding='utf-8') as f:
    f.write(output)
