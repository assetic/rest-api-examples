"""
Script to support moving of documents from one asset to another
Can run from a csv file listing 'old asset id' -> 'old asset id'
or be prompted for the from and to assets

When file is run, user is prompted to enter their choice of using a csv file
or to be prompted for the asset IDs
"""

import pandas as pd
import assetic
import base64
import sys
import six

# set the location of the ini file or leave as 'None' if you have the file
# %APPDATA%/Assetic/assetic.ini
ini = None
# set the log file to have progress messages written to log
logfile = r"c:\temp\move.log"
#logfile = None
asseticsdk = assetic.AsseticSDK(
    inifile=ini, loglevelname="Info", logfile=logfile)


# Function to download a document given an old asset ID
def download_document(old_asset_id):

    # will return a list of file objects
    files = list()

    # Get Asset
    asset = assetic.AssetApi()
    try:
        asset_response = asset.asset_get(old_asset_id, list())
    except assetic.rest.ApiException as ex:
        msg = "Error attempting to get original asset {0}.  " \
              "Error status {1}, Reason: {2} {3}".format(
            old_asset_id, ex.status, ex.body, ex.reason)
        asseticsdk.logger.error(msg)
        if logfile:
            # Messages going to logfile, alert user about error
            print(msg)
        return files
    asset_response_guid = asset_response["Id"]

    # Get Documents
    document = assetic.DocumentApi()
    pagesize = 500
    sortorder = "LastModified-desc"
    request_filter = "ParentType~eq~'ComplexAsset'~and~ParentId~eq~'{0}'" \
                     "~and~Status~eq~300".format(asset_response_guid)

    kwargs = {"request_params_page": 1,
              "request_params_page_size": pagesize,
              "request_params_sorts": sortorder,
              "request_params_filters": request_filter
              }

    try:
        doc = document.document_get(**kwargs)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error('Status {0}, Reason: {1} {2}'.format(
            e.status, e.reason, e.body))
    else:
        if doc['TotalResults'] > 0:
            doc_list = doc['ResourceList']

            for file in doc_list:
                file_guid = file["Id"]
                file_data = None
                if not file["DocumentLink"]:
                    file_data = document.document_get_document_file(file_guid)
                    if sys.version_info < (3, 0):
                        filecontents = file_data.encode("base64")
                    else:
                        if isinstance(file_data, str):
                            filecontents = base64.b64encode(
                                bytes(file_data, 'utf-8'))
                        else:
                            filecontents = base64.b64encode(file_data)
                        filecontents = filecontents.decode(
                            encoding="utf-8", errors="strict")
                    file_data = filecontents

                file_meta = {
                    "asset_id": old_asset_id,
                    "document_id": file["Id"],
                    "label": file["Label"],
                    "mime_type": file["MimeType"],
                    "status": file["Status"],
                    "data": file_data,
                    "is_key_photo": file["IsKeyPhoto"],
                    "size": file["DocumentSize"],
                    "doc_group": file["DocumentGroup"],
                    "doc_group_label": file["DocGroupLabel"],
                    "doc_sub_cat": file["DocumentSubCategory"],
                    "doc_sub_cat_label": file["DocSubCategoryLabel"],
                    "doc_cat": file["DocumentCategory"],
                    "doc_cat_label": file["DocCategoryLabel"],
                    "expiry_date": file["ExpiryDate"],
                    "description": file["Description"],
                    "external_id": file["ExternalId"],
                    "doc_link": file["DocumentLink"]
                }

                files.append(file_meta)

    return files


# Function to upload a document to a new asset ID
def upload_document(new_asset_id, document_content):
    # Get Asset
    asset = assetic.AssetApi()
    try:
        asset_response = asset.asset_get(new_asset_id, list())
    except assetic.rest.ApiException as ex:
        msg = "Error attempting to get new destination asset {0}.  " \
              "Error status {1}, Reason: {2} {3}".format(
            new_asset_id, ex.status, ex.body, ex.reason)
        asseticsdk.logger.error(msg)
        if logfile:
            # Messages going to logfile, alert user about error
            print(msg)
        return False

    asset_response_guid = asset_response["Id"]

    # Get Documents
    document_api = assetic.DocumentApi()
    # create file properties object and set values
    file_properties = assetic.FilePropertiesRepresentation()
    if document_content["data"]:
        file_properties.name = document_content["label"]
        file_properties.mimetype = document_content["mime_type"]
        file_properties.filecontent = document_content["data"]
        file_properties.file_size = document_content["size"]
        filearray = [file_properties]
    else:
        filearray = None

    # create document object and assign values, including file properties
    document = assetic.DocumentRepresentation()
    document.file_extension = document_content["mime_type"]

    document.doc_group = document_content["doc_group"]
    document.document_category = document_content["doc_cat"]
    document.document_sub_category = document_content["doc_sub_cat"]
    document.expiry_date=document_content["expiry_date"]
    document.description = document_content["description"]
    document.external_id = document_content["external_id"]

    if document_content["doc_link"]:
        document.document_link = document_content["doc_link"]
        document.label = document_content["label"]

    # apply parent link
    document.parent_type = "ComplexAsset"
    document.parent_id = asset_response_guid

    if filearray:
        document.file_property = filearray

    try:
        response = document_api.document_post(document)
    except assetic.rest.ApiException as ex:
        msg = "Error attempting to upload document to {0}.  " \
              "Error status {1}, Reason: {2} {3}".format(
            new_asset_id, ex.status, ex.body, ex.reason)
        asseticsdk.logger.error(msg)
        if logfile:
            # Messages going to logfile, alert user about error
            print(msg)
        return False

    asseticsdk.logger.info(
        "Document {0} uploaded successfully to new asset ID {1}".format(
            document_content['label'], new_asset_id))

    return True


# Function to delete the original document using document id
def delete_document(id):
    #  URL to delete the document
    document_api = assetic.DocumentApi()
    try:
        document_api.document_delete_document_file_by_id(id)
    except assetic.rest.ApiException as e:
        msg = "Error attempting to delete document.  " \
              "Error status {0}, Reason: {1} {2}".format(
            e.status, e.body, e.reason)
        asseticsdk.logger.error(msg)
        if logfile:
            # Messages going to logfile, alert user about error
            print(msg)
        return
    asseticsdk.logger.info("Original Document deleted successfully")


# Function to process bulk option
def bulk_option(move_document):
    file_path = six.moves.input("Enter the file location (CSV): ")
    file_path = file_path.replace('"', '')
    df = pd.read_csv(file_path)

    for index, row in df.iterrows():
        old_asset_id = row['old asset id']
        new_asset_id = row['new asset id']

        document_files = download_document(old_asset_id)

        for document_content in document_files:
            msg = "Processing documents for old asset {0} " \
                  "to new asset {1}".format(old_asset_id, new_asset_id)
            asseticsdk.logger.error(msg)
            if logfile:
                print(msg)
            upload_document(new_asset_id, document_content)

            if move_document:
                # delete the document from the original environment
                delete_document(document_content["document_id"])


# Function to process single option
def single_option(move_document):
    old_asset_id = input("Enter the old asset ID: ")
    new_asset_id = input("Enter the new asset ID: ")

    document_files = download_document(old_asset_id)

    for document_content in document_files:
        upload_document(new_asset_id, document_content)

        if move_document:
            # Assuming successful upload, you can delete the original document here if needed
            delete_document(document_content["document_id"])


# Main menu
while True:
    print("Options:")
    print("1. Bulk Option")
    print("2. Single Option")
    print("3. Quit")

    choice = input("Enter your choice: ")

    if choice == '3':
        break


    chk_move = six.moves.input("Enter 'M' to move document, or 'C' to copy document: ")
    if chk_move.lower() == "c":
        bool_move_document = False
        print("Documents will be copied from old asset to new asset")
    elif chk_move.lower() == "m":
        bool_move_document = True
        print("Documents will be moved from old asset to new asset")
    else:
        print("Invalid choice. Exiting process.")
        break

    if choice == '1':
        bulk_option(bool_move_document)
    elif choice == '2':
        single_option(bool_move_document)
    else:
        print("Invalid choice. Please enter 1, 2, or 3.")
