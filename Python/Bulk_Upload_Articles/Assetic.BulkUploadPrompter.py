import assetic
##Define the location of the assetic.ini file, or leave as None to use the
##default locations
inifile = None
##Define the location of the log file, or leave as None for logging to screen
logfile = None
##Define the loglevel - usually one of "Info","Debug","Error"
loglevel = "Info"

asseticsdk = assetic.AsseticSDK(inifile,logfile,loglevel)
uploadapi = assetic.BulkProcesses()
uploadapi.bulk_upload_prompter()
