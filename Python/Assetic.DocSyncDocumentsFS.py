import assetic
#############Settings - make changes here###############################
#docsearchfilter = "ExternalId=''"
search_profile = "c723fbaf-4a56-e711-8187-025500ccdddb"
wkocloseprofile = "fca838ec-e756-e711-8187-025500ccdddb" #Assetic Test
inifile = "c:/users/kwilton/assetic.ini"   #Has your login details
loglevel = "Info"   #can also set as "Debug" for tracing errors
logfile = None  #Log file
#############End of Settings################################

asseticsdk = assetic.AsseticSDK(inifile,logfile,loglevel)
docapi = assetic.DocumentApi(asseticsdk.client_for_docs)
doctools = assetic.DocumentTools("File_System",asseticsdk.client_for_docs)
##create an instance of the search api
sapi = assetic.SearchApi()

#def main(searchfilter):
def main(searchprofile):
    """
    The given search filter provides a list of
    documents to move to Trim
    """
    ##get list of assets
    page = 1
    pagesize = 20
    sort = None

    cnt_ok = 0
    cnt_warn = 0
    while page < 3:
        data = getunsyncedbysearch(searchprofile,page,pagesize)

        for row in data:
            ecms_doc_rep = assetic.ECMSDocumentRepresentation()
            doc_rep = assetic.Assetic3IntegrationRepresentationsDocumentRepresentation()
            doc_rep.id = row["Id"]
            ecms_doc_rep.retain_in_assetic = True
            ecms_doc_rep.assetic_doc_representation = doc_rep
            asseticsdk.logger.info("Processing = {0}".format(doc_rep.id)) 
            chk = doctools.move_document_to_ecms(ecms_doc_rep,True,False)
            if chk == 1:
                ##error
                return
            elif chk == -1:
                cnt_warn = cnt_warn + 1
            else:
                asseticsdk.logger.info("Processed document {0}".format(
                    doc_rep.id))
                cnt_ok = cnt_ok + 1
        page = page + 1
    asseticsdk.logger.info("Total successfully processed = {0}".format(cnt_ok))
    asseticsdk.logger.info("Total skipped = {0}".format(cnt_warn))        

def getunsyncedbysearch(searchprofile,page,pagesize):
    """
    Use advanced search profile to get a list of documents
    Return:List of results or empty list of error
    """
    kw = {"request_params_id": searchprofile,
        "request_params_page":page,
        "request_params_page_size":pagesize}
#        "request_params_sorts":sort,
#        "request_params_filters":searchfilter}
    try:
        sg = sapi.search_get(**kw)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
            e.status,e.reason,e.body))
        return []

    if page == 1:
        totalresults = sg.get("TotalResults")
        totalpages = sg.get("TotalPages")
        asseticsdk.logger.info("Found {0} documents over {1} page(s)".format(
            totalresults,totalpages))
    else:
        asseticsdk.logger.info("Processing page {0} of results".format(page))
    resourcelist = sg.get("ResourceList")
    resource = resourcelist[0]
    data = resource.get("Data")
    return data        


    
def getunsynceddocuments(searchfilter,page,pagesize,sort):
    """
    Use document API search to get list of documents
    Return:List of results or empty list of error
    """
    kwargs = {"request_params_page":page,
              "request_params_page_size":pagesize,
#              "request_params_sorts":sort,
              "request_params_filters":searchfilter}
    try:
        doc = docapi.document_get(**kwargs)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error('Status {0}, Reason: {1} {2}'.format(
            e.status,e.reason,e.body))
        return []

    if len(doc) == 0:
        return []
    if page == 1:
        totalresults = doc[0].get("TotalResults")
        totalpages = doc[0].get("TotalPages")
        asseticsdk.logger.info("Found {0} documents over {1} page(s)".format(
            totalresults,totalpages))
    else:
        asseticsdk.logger.info("Processing page {0} of results".format(page))
    resourcelist = doc[0].get("ResourceList")

    return resourcelist        

def delwkodocs(searchprofile):
    """
    The given search filter provides a list of
    documents to move to Trim
    """
    ##get list of assets
    page = 1
    pagesize = 7
    sort = None

    cnt_ok = 0
    cnt_warn = 0
    while page < 4:
        data = getwkodocuments(searchprofile,page,pagesize)

        for row in data:
            docid = row["Id"]
            asseticsdk.logger.info("Processing = {0}".format(docid)) 
            chk = doctools.delete_doc(docid)
            if chk > 0:
                ##error
                asseticsdk.logger.error("Process aborted due to error")
                return
            else:
                asseticsdk.logger.info("Processed document {0}".format(
                    doc_rep.id))
                cnt_ok = cnt_ok + 1
        page = page + 1
    asseticsdk.logger.info("Total successfully processed = {0}".format(cnt_ok))

def getwkodocuments(searchfilter,page,pagesize,sort):
    """
    Use document API search to get list of documents
    Return:List of results or empty list of error
    """
    kwargs = {"request_params_page":page,
              "request_params_page_size":pagesize,
#              "request_params_sorts":sort,
              "request_params_filters":searchfilter}
    try:
        doc = docapi.document_get(**kwargs)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error('Status {0}, Reason: {1} {2}'.format(
            e.status,e.reason,e.body))
        return []

    if len(doc) == 0:
        return []
    if page == 1:
        totalresults = doc[0].get("TotalResults")
        totalpages = doc[0].get("TotalPages")
        asseticsdk.logger.info("Found {0} documents over {1} page(s)".format(
            totalresults,totalpages))
    else:
        asseticsdk.logger.info("Processing page {0} of results".format(page))
    resourcelist = doc[0].get("ResourceList")

    return resourcelist        


if __name__ == "__main__":
    #main(docsearchfilter)
    main(search_profile)
    #delwkodocs(wkocloseprofile)
