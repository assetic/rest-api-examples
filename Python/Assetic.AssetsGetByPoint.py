"""
Get assets by sptaial search (Assetic.AssetsGetByPoint.py)
"""
import assetic
#Assetic SDK instance
asseticsdk=assetic.AsseticSDK("c:/users/you/assetic.ini",None,"Debug")
#asset API
assetapi = assetic.AssetApi()

def main():
    """
    Initiate the search.  Search parameters hardcoded here  
    """
    ##get some nearby assets
    latitude = -37.7189464838113
    longitude = 144.73930864699
    nearby = get_assets_by_spatial(latitude,longitude,50,"meter")
    for row in nearby:
        msg = "Nearby asset {0}, Category {1}".format(
            row["properties"]["Asset Name"],row["properties"]["Asset Category"])
        asseticsdk.logger.info(msg)
    
def get_assets_by_spatial(latitude,longitude,radius,uom):
    """
    Get nearby assets based on a goegraphic point and radius
    :param latitude: latitude of point
    :param longitude: longitude of point
    :param radius: search radius
    :param uom: radius unit of measure - meter or kilometer
    :return: asset array
    """
    asseticsdk.logger.info("Get the assets near point {0},{1}".format(
        latitude, longitude))

    kw = {"request_params_longitude":longitude,
        "request_params_latitude":latitude,
        "request_params_condition":"near_point",
        "request_params_range":radius,
        "request_params_unit":uom,
        "request_params_page":1,
        "request_params_page_size": 500}
    try:
        assets = assetapi.asset_search_asset_spatial_locations(**kw)
    except assetic.rest.ApiException as e:
        msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
        asseticsdk.logger.error(msg)
        return []
    if assets["TotalResults"] > 0:
        return assets["ResourceList"][0]["Data"]["features"]
    else:
        return []
    return assets

##This will run the method main() to kickstart it all
if __name__ == "__main__":
    main()
