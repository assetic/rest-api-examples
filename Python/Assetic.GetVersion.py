"""
    Example script to get version (Assetic.GetVersion.py)
"""
import assetic

# Assetic SDK instance
asseticsdk = assetic.AsseticSDK(None, None, 'Info')
# Version API
versionapi = assetic.VersionApi()
try:
    response = versionapi.version_get()
except assetic.rest.ApiException as e:
    asseticsdk.logger.error('Status {0}, Reason: {1} {2}'.format(
        e.status, e.reason, e.body))
else:
    print("{0}.{1}.{2}.{3}".format(response["Major"], response["Minor"]
                                   , response["Build"], response["Revision"]))

