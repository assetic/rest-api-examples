import assetic
import csv
#############Settings - make changes here###############################
searchprofileid = "b8b6594a-7a55-e711-8187-025500ccdddb"  #Assetic Test
inifile = "c:/users/kwilton/assetic.ini"   #Has your login details
loglevel = "Info"   #can also set as "Debug" for tracing errors
logfile = None  #Log file
#############End of Settings################################


asseticsdk = assetic.AsseticSDK(inifile,logfile,loglevel)
doc = assetic.DocumentTools("File_System",asseticsdk.client)
##create an instance of the search api
sapi = assetic.SearchApi()

def main(profile,searchfilter = None):
    """
    The given search profile and optional filter provides a list of
    assets to create folders for
    """
    ##get list of assets
    page = 1
    pagesize = 2
    sort = None
    
    while page < 2:
        data = getassetsbysearch(profile,searchfilter,page,pagesize,sort)

        for row in data:
            assetid = row["Asset Id"]
            assetcategory = row["Asset Category"]
            assetname = row["Asset Name"]

            chk = create_folder_for_asset(assetid,assetname,assetcategory)
            print(chk)
            if chk == 1:
                ##error
                return
        page = page + 1
        
def create_folder_for_asset(assetid,assetname,assetcategory):
    """
    Create a folder in File System for the given asset and classification
    """
    
    folder_rep = assetic.ECMSFolderRepresentation()

    ##get settings
    folder_rep.ecmsparent = "c:/temp/documents/{0}".format(assetcategory)

    #set folder values
    #folder_rep.title = "{0} -{1}".format(assetid,assetname)
    folder_rep.title = "{0}".format(assetid)
    folder_rep.asseticid = assetid

    return doc.new_folder_for_asset(assetid,folder_rep)
    

def getassetsbysearch(searchprofile,searchfilter,page,pagesize,sort):
    """
    Use advanced search profile to get a list of assets
    """
    kw = {"request_params_id": searchprofile,
        "request_params_page":page,
        "request_params_page_size":pagesize,
        "request_params_sorts":sort,
        "request_params_filters":searchfilter}
    try:
        sg = sapi.search_get(**kw)
    except assetic.rest.ApiException as e:
        asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
            e.status,e.reason,e.body))
        return []

    if page == 1:
        totalresults = sg.get("TotalResults")
        totalpages = sg.get("TotalPages")
        asseticsdk.logger.info("Found {0} assets over {1} page(s)".format(
            totalresults,totalpages))
    else:
        asseticsdk.logger.info("Processing page {0} of results".format(page))
    resourcelist = sg.get("ResourceList")
    resource = resourcelist[0]
    data = resource.get("Data")
    return data

if __name__ == "__main__":
    main(searchprofileid,None)
