"""
    Example script to automate data exchange (Assetic.DocumentUploadDataExchange.py)
    This script uploads a file to Assetic and creates a data exchange job
"""
import assetic
import os
import shutil
import sys
import base64
try:
    import configparser as configparser
except:
    import ConfigParser as configparser
import logging

# Read ini
settings = configparser.ConfigParser()
settings.read('C:/Users/kwilton/asseticPreview.ini')
# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
auth = conf.get_basic_auth_token()
#Debug settings
conf.logger_file = 'C:/temp/logger.txt'
conf.debug = False
if conf.debug == True:
    logging.basicConfig() 
    logging.getLogger().setLevel(logging.DEBUG)

##Assetic SDK instance     
client = assetic.ApiClient(conf.host,"Authorization",auth)
docapi = assetic.DocumentApi(client)
dataexchangeapi = assetic.DataExchangeJobApi(client)

fullfilename = "c:/temp/dxerror.csv"
#profileid = "876a0a4e-c732-e611-945f-06edd62954d7"
profileid = "b0587b2d-5b21-e611-9458-06edd62954d7" #(Preview - Roads)
##read file contents and prepare for upload
with open(fullfilename, "rb") as f:
    data = f.read()
if sys.version_info < (3,0):
    filecontents = data.encode("base64")
else:
    filecontents = base64.b64encode(data)
    filecontents = filecontents.decode(encoding="utf-8", errors="strict")

filename = os.path.basename(fullfilename)

file_properties = assetic.Assetic3WebApiModelsFilePropertiesVM()
file_properties.name = filename
#file_properties.file_size = filesize
file_properties.mimetype = 'csv'
file_properties.filecontent = filecontents
filearray = [file_properties]

##document properties
document = assetic.Assetic3IntegrationApiRepresentationsDocumentRepresentation()
document.label = 'Data Exchange - ' + filename
document.file_extension = 'csv'
#document.expiry_date = 'datetime'
document.document_type = 'DataExchange'
document.mime_type = 'csv'
document.doc_group_id = 1
#document.doc_sub_category_id = int
#document.doc_category_id = int
document.file_property = filearray

#Upload document
doc = docapi.document_post(document)
docid = doc[0].get('Id') 
print(docid)

#Prepare data exchange parameters
job = assetic.Assetic3WebApiModelsDataExchangeDataExchangeJobVM()
job.profile_id = profileid
job.document_id = docid

#Create data exchange task
task = dataexchangeapi.data_exchange_job_post(job)
print(task)
