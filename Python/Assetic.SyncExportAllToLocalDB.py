"""
    Assetic.SyncExportToLocalDB.py
    Checks if there are files to download from from Export All task
    Downloads the file and saves locally to then process
    Uses file system to record pending & processed export jobs
    Data is written to a temp db table and then the 'merge' SQL is used
    to update/insert records in the permanant table
    Will create the tables as required (TODO check columns exist)
    Writes to a log table the changed record (TODO expand the output)
"""
import pypyodbc
import sys
import assetic   #This is an SDK to wrap the REST endpoints
import datetime
import dateutil.parser
import configparser as configparser
import logging
import math
import functools
import csv
from contextlib import contextmanager
# Script Variables
# Read ini
settings = configparser.ConfigParser()
settings.read('c:/users/kwilton/assetic.ini')

# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
#Debug settings
conf.logger_file = 'c:/temp/create_request.txt'

conf.debug = False
if conf.debug == True:
    logging.basicConfig() 
    logging.getLogger().setLevel(logging.DEBUG)

##Database details
dbserver = "Quicksilver"
dbname = "scratch"
logtable = "synclog"
keyfield = "[Asset Id]"
##search profile ID

#searchguid = "20ca50db-c45d-e511-944d-06edd62954d7"
searchguid = "xxx"
## set the table each profile relates to
profile_to_table_dict = {"20ca50db-c45d-e511-944d-06edd62954d7":"alldata",
                         "f08b7f20-f473-e611-946b-06edd62954d7":"syncaddress",
                         "5c6f3ac0-7179-e611-946c-06edd62954d7":"corefields",
                         "xxx":"exportall"}

##Assetic SDK instance
auth = conf.get_basic_auth_token()
client = assetic.ApiClient(conf.host,"Authorization",auth)
conf.api_client = client
    

##Define functions.  End of this script initiates the process that uses these functions 

@contextmanager
def open_db_connection(connection_string, commit=False):
    """
    Use contextmanager to manage connections so we cleanup connection
    as we go to avoid database locking issues on exception
    """
    connection = pypyodbc.connect(connection_string)
    cursor = connection.cursor()
    try:
        yield cursor
    except pypyodbc.DatabaseError as err:
        cursor.execute("ROLLBACK")
        raise err
    except Exception as ex:
        print(ex)
    else:
        if commit:
            cursor.execute("COMMIT")
        else:
            cursor.execute("ROLLBACK")
    finally:
        connection.close()

def getexport(exporttaskguid,savefolder):
    ##returns the csv file (full path) for a given bulk export id 

    ##create instance of export api
    #sapi = assetic.xxxApi(client)

    ##Apply search on task id
    #docid = sapi.xxxx
    docid = None
    
    ##Download file not available yet
    if docid == None:
        return None
    
    ##download the file
    csvfile = downloadfile(docid,savefolder)
    ##return the file name
    return csvfile

def downloadfile(docid,savefolder):
    docclient = assetic.ApiClient(conf.host,"Authorization",auth,None,'Content-Disposition')
    docapi = assetic.DocumentApi(docclient)
    getfile = docapi.document_get_document_file(docid)
    fullfilename = None
    if getfile != None:
        if 'attachment' in getfile[1] and 'filename=' in getfile[1]:
            filename = getfile[1].split('filename=',1)[1]
            if '"' in filename or "'" in filename:
                filename = filename[1:-1]
            fullfilename = savefolder + filename
        else:
            fullfilename = savefolder + "unknownfilename" 

        data = getfile[0]
        if sys.version_info >= (3,0):
            try:
                data = data.decode('utf-8')
            except:
                pass
        if type(data) == bytes:
            with open( fullfilename, 'wb' ) as out_file:
                out_file.write(getfile[0])
                print ("Created file: {0}".format(fullfilename))
        elif type(data) == str:
            with open( fullfilename, 'w', newline='' ) as out_file:
                out_file.write(getfile[0])
                print ("Created file: {0}".format(fullfilename))
        else:
            print("File not created, data type unhandled: {0}".format(str(type(data))))

    return fullfilename

def check_for_csv
def csv_to_db(csvfile,dbtable,keyfield,logtable):
    #write data in file to database
    #get list of columns
    bIsHeader = True
    allrows = []
    saved_row = []

    with open(csvfile, 'rt', encoding='utf-8', newline = '') as csvfile:
        readCSV = csv.reader(csvfile,delimiter=',')
        for row in readCSV:
            if bIsHeader == True:
                bIsHeader = False
                columns = row
            else:
                #cater for carriage returns in a column
                if len(saved_row):
                    row_tail = saved_row.pop()  #gets last item in the list
                    row[0] = row_tail + '\r' +row[0]  # reconstitute field broken by newline
                    row = saved_row + row       # and reassemble the row (possibly only partially)
                if len(row) >= len(columns):
                    allrows.append(row)
                    saved_row = []
                else:
                    saved_row = row
                    
    #name for tmp table we'll write data dump to
    tmpdbtable = "#{0}_tmp".format(dbtable)

    ##First get the database table column names and build the various tables
    ##TODO add a check for existing tables to make sure all columns present
    insertcolumns = []
    updatecolumns = []
    parammark = []  #add a "?" for each column to use in sql parameter settings
    for column in columns:
        insertcolumns.append("[{0}]".format(column))
        updatecolumns.append("a.[{0}]=c.[{0}]".format(column))
        parammark.append("?")

    sqltblchktmp =  "select object_id('{0}')".format(tmpdbtable)
    sqltruncatetmp = "truncate table {0}".format(tmpdbtable)
    sqlcreatetmp = """create table {0} ({1} nvarchar(500) COLLATE
        database_default)""".format(tmpdbtable,
        " nvarchar(500) COLLATE database_default,".join(insertcolumns ))
    sqltblchk =  "select object_id('{0}')".format(dbtable)
    sqlcreate = """create table {0} ({1} nvarchar(500))
        """.format(dbtable,"nvarchar(500),".join(insertcolumns ))
    sqllogchk = "select object_id('{0}')".format(logtable)
    sqllogcreate = """CREATE TABLE {0}(
	[action] [nvarchar](500),
	[Asset Id] [varchar](200),
	[syncdate] [datetime])""".format(logtable)
    ##Now create insert string
    sqlinsert = "insert into {0} (".format(tmpdbtable)
    sqlinsert = sqlinsert + ",".join(insertcolumns ) + ") values ("
    sqlinsert = sqlinsert + ",".join(parammark ) + ")"

    ##Now add data to sql insert string and do insert
    ##(using 'insertmany' effectively row by row)

    ##now refresh base table from temp table where there are changes
    ##write the change to the archive file
    ##TODO output all columns for updated record (original record)
    sqldiff = """
    WITH diff as ( select {2}
        FROM {0}
        except select {2}
        FROM {1}
        )
        MERGE {1} as a using diff as c on
        (c.{5}=a.{5})
        when MATCHED then update set {3}
        When not matched then insert
        ({2}) 
        values ({2})
        output $action,inserted.[Asset Id],getdate() into {4};
    """.format(tmpdbtable,dbtable,",".join(insertcolumns),
               ",".join(updatecolumns),logtable,keyfield)

    ##Now execute all the sql statements in the one transaction block
    ##Bit slow opening & closing db connection, but transaction block works well
    try:
        with open_db_connection(connstr,True) as cursor:
            #check if temportaty table exists
            print(sqltblchktmp)
            cursor.execute(sqltblchktmp)
            row = cursor.fetchone()
            if row[0] == None:
                print(sqlcreatetmp)
                cursor.execute(sqlcreatetmp)
            else:
                print(sqltruncatetmp)
                cursor.execute(sqltruncatetmp)

            #check if permanent table exists
            cursor.execute(sqltblchk)
            print(sqltblchk)
            row = cursor.fetchone()
            if row[0] == None:
                #table not found - create permament table for current data
                print(sqlcreate)
                cursor.execute(sqlcreate)
            #check if log table exists
            print(sqllogchk)
            cursor.execute(sqllogchk)
            row = cursor.fetchone()
            if row[0] == None:
                #table not found - create permament table for logging changes
                print(sqllogcreate)
                cursor.execute(sqllogcreate)
            ##write all the data to the temp table in db.
            print(sqlinsert)
            cursor.executemany(sqlinsert,allrows)
            ##execute diff statement to populate permanent tables from temp table
            print(sqldiff)
            cursor.execute(sqldiff)
    except Exception as ex:
        print(ex)
        return False
    return True
    
####End of functions################################

##get tablename based on profile
try:
    dbtable = profile_to_table_dict.get(searchguid)
except:
    exit()
    
#can use UID=user;PWD=password instead of trusted connection.
#Provide APP to all dba to see what this connection is
connstr = "Driver={SQL Server};" + """Server={0};Database={1};
    Trusted_Connection=True;
    APP=Assetic Asset Sync""".format(dbserver,dbname)
csvfile = 'C:/Users/kwilton/Downloads/Key attributes.csv'
if csv != None:
    success = csv_to_db(csvfile,dbtable,keyfield,logtable)
    if success == False:
        print("existing prematurely due to exception in database process")
        morepages = False

