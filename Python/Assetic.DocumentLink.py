"""
    Example scipt for linking a document to an asset
    Could also link to other types such as work order
"""
import assetic
import os
import shutil
try:
    import configparser as configparser
except:
    import ConfigParser as configparser
import logging
        
# Read ini
settings = configparser.ConfigParser()
settings.read('C:/Users/kwilton/asseticPreview.ini')
# Apply config settings
conf = assetic.configuration
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
auth = conf.get_basic_auth_token()
#Debug settings
conf.logger_file = 'C:/temp/logger.txt'
conf.debug = True
if conf.debug == True:
    logging.basicConfig() 
    logging.getLogger().setLevel(logging.DEBUG)

docid = "c5c98092-4329-e611-9459-06edd62954d7"
parent = assetic.Assetic3WebApiModelsDocumentsDocumentParentVM()
parent.parent_id = "2c1f3a3c-4ad1-e511-9451-06edd62954d7"
parent.parent_type = "ComplexAsset"

client = assetic.ApiClient(conf.host,"Authorization",auth)

docapi = assetic.DocumentApi(client)
putlink = docapi.document_put_link_to_parent(docid,parent)
print(putlink)


