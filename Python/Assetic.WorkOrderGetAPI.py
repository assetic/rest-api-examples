"""
    Example script to get work order using guid (Assetic.WorkOrderGetAPI.py)

"""
import assetic
##Assetic SDK instance
asseticsdk = assetic.AsseticSDK("C:/Users/sjsam/Documents/rest-api-examples/Python/assetic.ini",None,"Info")
#instance of work order API
wkoapi = assetic.WorkOrderApi()
##unique ID of work order
woguid = "115ee90b-7ede-4e00-8ad2-0001f6646bab"

try:
    wko = wkoapi.work_order_integration_api_get_0(woguid)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
else:
    ##output the response to the logger
    asseticsdk.logger.info(wko.get("WorkOrderStatus"))
    asseticsdk.logger.info(wko.get("FriendlyId"))
