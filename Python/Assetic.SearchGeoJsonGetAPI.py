"""
    Example script to run a search profile (Assetic.SearchGeoJsonGetAPI.py)
    Search results includes spatial definition of each record
"""
import assetic

##Assetic SDK instance      
asseticsdk = assetic.AsseticSDK("C:/Users/kwilton/assetic.ini",None,"Info")
#instance of search API
sapi = assetic.SearchApi()

#profileid = 2a6bf35b-021c-e611-9812-0632cf3be881  #nightly roads REST test 1
profileid = "ec71f6c3-0d10-e611-9458-06edd62954d7" #preview
profileid = "7c277340-7d3e-e611-945f-06edd62954d7"#demo
#set pagesize (number of results returned per request
pagesize = 500
##Run requests, looping through pages until all records returned
flag = True
cnt = 1
while (flag == True):

    kw = {'request_params_id': profileid,
        'request_params_page':cnt,
        'request_params_page_size':pagesize}
        #,'request_params_filters':"ComplexAssetLastModified>=2016-07-03T1:16:07,ComplexAssetType=Sealed Road"}

    sg = sapi.search_get_geo_json_result(**kw)

    resourcelist = sg.get('ResourceList')
    resource = resourcelist[0]
    data = resource.get('Data')
    features = data.get('features')
    for row in features:
        geometry = row.get('geometry')
        gtype = geometry.get('type')
        if gtype != 'Point':
            print(row)
        else:
            pass
            #print("no spatial",row)

    totalresults=sg.get('TotalResults')
    numpages = sg.get('TotalPages')
    if cnt > numpages:
        flag = False
    else:
        cnt = cnt + 1
        print('Total Results: {0}, Pages {1}, Page: {2}'.format(totalresults,numpages,cnt))

