"""
    Example script to apply a work order cost (Assetic.UpdateWorkOrderTask.py)
    Applies an actual cost to a work order task
"""
import assetic

##Assetic SDK instance      
asseticsdk = assetic.AsseticSDK("c:/users/kwilton/assetic.ini",None,"Info")

##define work order and task GUID to update against
woguid = "f6a6a21e-0a23-e611-9458-06edd62954d7"   #Preview - COMP stage
taskid = "f7a6a21e-0a23-e611-9458-06edd62954d7"

woguid = "46365754-20c3-e611-946c-06edd62954d7"   #Demo
taskid = "5a309e65-20c3-e611-946c-06edd62954d7"

##create work order API instance
wkoapi = assetic.WorkOrderApi()

#get current values for the task. Use this response object as the base
#for the update to ensure fields we are not updating are preserved
try:
    task = wkoapi.work_order_get_work_task_by_work_task_id(woguid,taskid)
except assetic.rest.ApiException as e:
    asseticsdk.logger.info("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()
#update task object to set external_costs to $500, other costs to $200
task["ParentId"] = woguid
task["ExternalCosts"] = 500
task["OtherCosts"] = 200

##apply updated task
try:
    wkoapi.work_order_put_0(woguid,taskid,task)
except assetic.rest.ApiException as e:
    asseticsdk.logger.info("Status {0}, Reason: {1}".format(e.status,e.reason))
else:
    asseticsdk.logger.info("Success")
