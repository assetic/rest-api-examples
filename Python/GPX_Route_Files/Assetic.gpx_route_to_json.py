import json
import xml.etree.ElementTree as ET
from dateutil import parser, tz
import urllib3
import logging
import base64
import configparser
import os


def lambda_handler(event, context):
    """
    Get recently created GPX files in Work Orders.  For each work order with a new GPX file(s)
     get all GPX files for the Work Order and translate to geoJson.  Upload the geoJson file
     to the Work Order, replacing a previous geoJson file if one exists.
    @param event: parameters as a dict
    @param context:
    @return:
    """

    # set some defaults from the event parameter
    last_execution_file = event["last_execution_file"]
    log_file = event["log_file"]

    # initiate the classes we will use
    gpx_tool = GpxToJSON(log_file=log_file)
    assetic_tool = Assetic(auth_location=event["auth_location"], log_file=log_file)

    # set logging
    logger = logging.getLogger("Main")
    if log_file:
        c_handler = logging.FileHandler(filename=log_file, mode='a')
    else:
        c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.INFO)
    c_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    c_handler.setFormatter(c_format)
    logger.addHandler(c_handler)
    logger.level = logging.INFO

    logger.info("Initiating GPX Process")

    # set some Assetic defaults from the event parameter
    assetic_tool.doc_output_mime_type = event["output_mime_type"]
    assetic_tool.doc_group = event["doc_group"]
    assetic_tool.doc_category = event["doc_category"]
    assetic_tool.doc_subcategory = event["doc_subcategory"]

    # get datetime to include in search filter for candidate GPX files
    max_last_modified = None
    if os.path.isfile(last_execution_file):
        with open(last_execution_file) as f:
            max_last_modified = f.readline()
    if not max_last_modified:
        # first execution - set an initial datetime to start processing from
        max_last_modified = "2018-09-19T09:27:20"

    # get candidate files - ordered by LastModified Ascending
    file_list = assetic_tool.get_unprocessed_gpx_file_list(max_last_modified, parent_type_id=16)

    # variable to record the work order the file belongs to as 'file_list' may contain multiple gpx
    # files from the same work order
    processed_parent = list()

    # iterate candidate files
    for file_metadata in file_list:
        if file_metadata["ParentId"] in processed_parent:
            # we've already processed all gpx files for this parent (they are merged into a single geojson)
            # increment the last modified date then move to next gpx file
            with open(last_execution_file, 'w') as f:
                f.write(file_metadata["LastModified"])
            continue

        # record that we are processing this work order parent so we don't reprocess
        processed_parent.append(file_metadata["ParentId"])
        # get all gpx files for the work order
        wo_files = assetic_tool.get_file_list_for_parent(file_metadata["ParentId"], parent_type_id=16, mime_type='gpx')
        for wo_file in wo_files:
            # get the gpx file
            file_content = assetic_tool.get_document(wo_file["Id"])
            # add file content  to metadata
            wo_file["file_content"] = file_content

        # prepare the geojson by reading all gpx and merging as single output geojson
        geojsonstr = gpx_tool.process_files(wo_files)
        # prepare a file name
        if file_metadata["DocumentWorkOrder"] and "FriendlyId" in file_metadata["DocumentWorkOrder"] and \
                file_metadata["DocumentWorkOrder"]["FriendlyId"]:
            file_name = f"{file_metadata["DocumentWorkOrder"]["FriendlyId"]} Inspection Route"
        else:
            file_name = "Inspection Route from gpx"

        # does a geojson already exist?
        existing_file = assetic_tool.get_file_list_for_parent(
            file_metadata["ParentId"], parent_type_id=16, mime_type="geojson", label=file_name)
        if len(existing_file) > 0:
            # delete existing file
            chk = assetic_tool.delete_document(existing_file[0]["Id"])
            if chk != 0:
                logger.error("Unable to delete existing geojson, skipping record")
                continue
            else:
                logger.info(f"Removed existing geoJson file {file_name}, Id {existing_file[0]["Id"]}")

        # upload new geojson to Assetic work order
        uploaded_doc = assetic_tool.upload_document(
            parent_type=16, parent_id=file_metadata["ParentId"], file_name=file_name, file_content=geojsonstr)
        if uploaded_doc and len(uploaded_doc) > 0:
            logger.info(f"Uploaded file {file_name}, Id {uploaded_doc[0]["Id"]}")

        # record new max last modified for next run to start from
        # write it each time we process a wo in case there is an error we start at the errored Work Order next time
        with open(last_execution_file, 'w') as f:
            f.write(file_metadata["LastModified"])

    logger.info("Finished GPX Process")


class GpxToJSON(object):
    """
    Class to parse to gpx xml and format as geoJSON
    """

    def __init__(self, log_file=None):
        self.from_zone = tz.gettz('UTC')
        self.logger = logging.getLogger("GpxToJSON")
        if log_file:
            c_handler = logging.FileHandler(filename=log_file, mode='a')
        else:
            c_handler = logging.StreamHandler()
        c_handler.setLevel(logging.INFO)
        c_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        c_handler.setFormatter(c_format)
        self.logger.addHandler(c_handler)
        self.logger.level = logging.INFO

    def process_files(self, files: list):
        """
        Iterate over the list of gpx files and return json
        @param files: a list of file dict objects.  Has file metadata ands file content
        @return: json
        """

        output = self.output_dict_stub()
        segment_id = 0
        for file in files:
            features = self.process_file(file["file_content"], file["Id"], segment_id)
            if len(features) > 0:
                output["features"] = output["features"] + features
                segment_id += 1

        if len(output["features"]) > 0:
            return json.dumps(output)

    @staticmethod
    def output_dict_stub():
        """
        The basic dict structure of the outer part of the geojson output
        @return: dict
        """
        return {
            "type": "FeatureCollection"
            , "features": []
        }

    @staticmethod
    def line_feature_dict_stub(track_id):
        """
        The basic structure for the segment (linestring) feature of the geojson output
        @param track_id:
        @return: dict
        """
        return {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": []
            },
            "properties":
            {
                "track_id": track_id,
                "name": "",
                "description": "",
                "resource": ""
            }
        }

    @staticmethod
    def point_feature_dict_stub(
            track_id: str, track_segment: int, point_index: int, segment_time_local_str: str, gps_point: list):
        """
        The segment (point) feature of the geojson output
        @param gps_point: gps point as list [longitude, latitude]
        @param segment_time_local_str: formatted time, local time
        @param point_index: integer id for point, incrementing number
        @param track_segment: integer id for segment
        @param track_id: string id for segment
        @return: dict
        """
        return {
            "type": "Feature",
            "geometry":
            {
                "type": "Point",
                "coordinates": gps_point
            },
            "properties":
            {
                "track_id": track_id,
                "track_segment": track_segment,
                "track_segment_point_index": point_index,
                "creation_time": segment_time_local_str,
                "name": "",
                "description": "",
                "resource": ""
            }
        }

    def process_file(self, filecontent: str, track_id: str, track_segment: int, ):
        """
        Process a gpx file, parsing it as xml and adding data to a feature dict
        @param filecontent: the gpx file to process
        @param track_id: an identifier for the gpx session
        @param track_segment: integer id for the gpx session
        @return: dict representing the features defined in the gpx file
        """
        features = list()

        line_feature = self.line_feature_dict_stub(track_id)

        root = ET.fromstring(filecontent)
        #tree = ET.parse(filename)  # use this if a file on disk
        if root.tag[1:6] == 'https':
            namespaces = {"d": "https://www.topografix.com/GPX/1/1"}
        else:
            namespaces = {"d": "http://www.topografix.com/GPX/1/1"}

        metadata_name = "Assetic GPX"   # dummy value
        metadata_desc = "Assetic GPX"
        metadata_user = "Assetic User"
        for metadata in root.findall('d:metadata', namespaces):
            try:
                metadata_name = metadata.attrib["name"]
                metadata_desc = metadata.attrib["desc"]
            except KeyError as ex:
                # could be alternate GPX structure
                try:
                    metadata_name = root.find('d:metadata' '/d:name', namespaces).text
                except AttributeError as ex:
                    self.logger.warning(f"Unable to get name: {ex}")
                try:
                    metadata_desc = root.find('d:metadata' '/d:desc', namespaces).text
                except AttributeError as ex:
                    self.logger.warning(f"Unable to get description: {ex}")
        # get the user (author)
        try:
            # this is the 'alternate structure'
            metadata_user = root.find('d:metadata' '/d:author' '/d:name', namespaces).text
        except AttributeError as ex:
            try:
                metadata_user = root.find('d:metadata' '/d:author', namespaces).attrib["name"]
            except Exception as ex:
                self.logger.warning(f"Unable to get author: {ex}")

        if len(root.findall('d:trk' '/d:trkseg' '/d:trkpt', namespaces)) == 0:
            # No data
            return features

        point_index = 0
        for child in root.findall('d:trk' '/d:trkseg' '/d:trkpt', namespaces):
            gps_point = [float(child.attrib["lon"]), float(child.attrib["lat"])]
            line_feature["geometry"]["coordinates"].append(gps_point)
            for child1 in child:
                #convert to local time
                segment_time = parser.parse(child1.text)
                segment_time_utc = segment_time.replace(tzinfo=self.from_zone)
                segment_time_local = segment_time_utc.astimezone()
                segment_time_local_str = segment_time_local.isoformat(timespec='seconds')
                point_feature = self.point_feature_dict_stub(
                    track_id, track_segment, point_index, segment_time_local_str, gps_point
                )
                point_feature["properties"]["name"] = metadata_name
                point_feature["properties"]["description"] = metadata_desc
                point_feature["properties"]["resource"] = metadata_user
                features.append(point_feature)
                point_index += 1
        if len(features) > 1:
            line_feature["properties"]["name"] = metadata_name
            line_feature["properties"]["description"] = metadata_desc
            line_feature["properties"]["resource"] = metadata_user
            features.append(line_feature)

        return features


class Assetic(object):
    """
    Class to manage the API calls to Assetic
    """

    def __init__(self, auth_location: str, log_file=None):
        """
        Setup class
        @param auth_location: location of auth settings
        @param log_file: file name to write log to, optional.  If none then logging to stream
        """
        # get secrets for Assetic auth
        auth_secrets = self.get_secrets(auth_location)
        username = auth_secrets["username"]
        password = auth_secrets["password"]
        assetic_url = auth_secrets["site"]

        # set urllib
        headers = urllib3.make_headers(basic_auth=f"{username}:{password}", user_agent="Lambda Unit Rates")
        self.http = urllib3.PoolManager(headers=headers)

        self.assetic_url = assetic_url

        # set logging
        self.logger = logging.getLogger("Assetic")
        if log_file:
            c_handler = logging.FileHandler(filename=log_file, mode='a')
        else:
            c_handler = logging.StreamHandler()
        c_handler.setLevel(logging.INFO)
        c_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        c_handler.setFormatter(c_format)
        self.logger.addHandler(c_handler)
        self.logger.level = logging.INFO

        self._doc_output_mime_type = None
        self._doc_group = None
        self._doc_category = None
        self._doc_subcategory = None

    @property
    def doc_output_mime_type(self):
        return self._doc_output_mime_type

    @doc_output_mime_type.setter
    def doc_output_mime_type(self, value):
        self._doc_output_mime_type = value

    @property
    def doc_group(self):
        return self._doc_group

    @doc_group.setter
    def doc_group(self, value):
        self._doc_group = value

    @property
    def doc_category(self):
        return self._doc_category

    @doc_category.setter
    def doc_category(self, value):
        self._doc_category = value

    @property
    def doc_subcategory(self):
        return self._doc_subcategory

    @doc_subcategory.setter
    def doc_subcategory(self, value):
        self._doc_subcategory = value

    def get_unprocessed_gpx_file_list(self, from_date: str, parent_type_id: int) -> list:
        """
        Get list of document metadata filtering by mime type of gpx, last processed date, parent type (e.g work order)
        @param from_date: string containing last processed date as starting point for this iteration
        @param parent_type_id: the integer id of the parent type
        return: list of document metadata
        """
        url = f"{self.assetic_url}/api/v2/document"
        page = 1
        documents = list()
        # make api call, incrementing by page number, limit to 10
        while page < 10:
            parameters = {
                "requestParams.filters": f"MimeType~eq~'gpx'~and~LastModified~gte~'{from_date}'"
                                         f"~and~ParentType~eq~{parent_type_id}"
                , "requestParams.page": 1
                , "requestParams.pageSize": 500
                , "requestParams.sorts": "LastModified-asc"

            }
            try:
                r = self.http.request('GET', url, fields=parameters)
            except Exception as ex:
                self.logger.error(f"Error getting list of unprocessed GPX files from Assetic: {ex}")
                break
            if r.status != 200:
                self.logger.error(f"Unable to get list of unprocessed GPX files from Assetic.  "
                                  f"Status Code {r.status}, Reason:{r.reason}")
                break
            data = r.json()
            documents += data["ResourceList"]
            if data["TotalPages"] == page:
                break
            page += 1

        return documents

    def get_file_list_for_parent(self, parent_id: str, parent_type_id: int, mime_type: str, label=None) -> list:
        """
        Get list of document metadata filtering by parent id and parent type id
        Typically parentid=work order GUID, parent type id represents work order
        @param parent_id: the guid of the work order/asset
        @param parent_type_id: the integer id of the parent type
        @param mime_type: file mime type (extension)
        @param label: specify a document via it's label - optional parameter
        return: list of document metadata
        """
        url = f"{self.assetic_url}/api/v2/document"
        page = 1
        documents = list()
        # make api call, incrementing by page number, limit to 10, status 300 means active
        while page < 10:
            parameters = {
                "requestParams.filters": f"MimeType~eq~'{mime_type}'~and~ParentId~eq~'{parent_id}'"
                                         f"~and~ParentType~eq~{parent_type_id}~and~Status~eq~300"
                , "requestParams.page": 1
                , "requestParams.pageSize": 500
            }
            if label:
                parameters["requestParams.filters"] += f"~and~Label~eq~'{label}'"
            # execute API request
            try:
                r = self.http.request('GET', url, fields=parameters)
            except Exception as ex:
                self.logger.error(f"Unable to get list of {mime_type} files for parent {parent_id} from Assetic: {ex}")
                break
            if r.status != 200:
                self.logger.error(f"Unable to get list of {mime_type} files for parent {parent_id} from Assetic.  "
                                  f"Status Code {r.status}, Reason:{r.reason}")
                break
            data = r.json()
            documents += data["ResourceList"]
            if data["TotalPages"] == page:
                break
            page += 1

        return documents

    def get_document(self, document_id):
        """
        get document for given Id
        @param document_id: assetic Id for document
        @return: document
        """
        url = f"{self.assetic_url}/api/v2/document/{document_id}/file"
        try:
            r = self.http.request('GET', url)
        except Exception as ex:
            self.logger.error(f"Error getting document {document_id} from Assetic: {ex}")
            return None
        if r.status != 200:
            self.logger.error(f"Error getting document {document_id} from Assetic.  "
                              f"Status Code {r.status}, Reason:{r.reason}")
            return None
        return r.data.decode("utf-8")

    def delete_document(self, document_id):
        """
        delete document for given Id
        @param document_id: assetic Id for document
        @return: 0=success, else error
        """
        url = f"{self.assetic_url}/api/v2/document/{document_id}"
        try:
            r = self.http.request('DELETE', url)
        except Exception as ex:
            self.logger.error(f"Unable to delete geojson file {document_id} from Assetic: {ex}")
            return -1
        if r.status != 200:
            self.logger.error(f"Unable to delete geojson file {document_id} from Assetic.  "
                              f"Status Code {r.status}, Reason:{r.reason}")
            return -1
        return 0

    def upload_document(self, parent_type: int, parent_id: str, file_name: str, file_content: str):
        """
        get document for given Id
        @return: document id
        """
        url = f"{self.assetic_url}/api/v2/document"

        # need to upload as base64 string
        bfile_content = base64.b64encode(bytes(file_content, 'utf-8'))
        b64file_content = bfile_content.decode(encoding="utf-8", errors="strict")

        payload = {
          "ParentType": parent_type,
          "DocumentGroup": self._doc_group,
          "DocumentSubCategory": self._doc_subcategory,
          "DocumentCategory": self._doc_category,
          "FileProperty": [
            {
              "Name": file_name,
              "mimetype": self._doc_output_mime_type,
              "filecontent": b64file_content
            }
          ],
          "ParentId": parent_id
        }
        json_body = json.dumps(payload)

        try:
            r = self.http.request('POST', url, body=json_body)
        except Exception as ex:
            self.logger.error(f"Error uploading file to Assetic: {ex}")
            return None
        if r.status != 201:
            self.logger.error(f"Error uploading file to Assetic.  Status Code {r.status}, Reason:{r.reason}")
            return None
        return r.json()

    def get_secrets(self, auth_location):
        """
        function to get assetic credentials
        @param auth_location: the configuration file wih
        @return: location of assetic credentials
        """
        config = configparser.ConfigParser()
        config.read(auth_location)
        asseticusername = config["auth"]["username"]
        asseticpassword = config["auth"]["api_key"]
        site = config["environment"]["url"]
        secrets = {"username": asseticusername, "password": asseticpassword, "site": site}

        return secrets


if __name__ == "__main__":
    """
    This will run the script by calling the method lambda_handler with the parameters
    defined in the 'event' dictionary
    """
    event = {
        "output_mime_type": "geojson",
        "doc_group": 10,
        "doc_category": 10,
        "doc_subcategory": 9,
        "auth_location": r"C:\Users\z004nhcv\AppData\Roaming\Assetic\assetic.ini",
        "last_execution_file": r"c:\temp\gpx_last_execution.txt",
        "log_file": r"c:\temp\gpx_to_geojson.log"
    }
    lambda_handler(event, None)
