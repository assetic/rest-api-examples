# coding: utf-8

"""
    Assetic.AssessmentFormCreateByPrompt.py
    Sript that prompts for the required information to build an
    Assessment Form in Assetic.
    Requires a csv file with field
    definitions.
    Requires the Assetic Python SDK to be installed
    Requires an "ini" file with the Assetic credentials
    Refer to https://assetic.zendesk.com/hc/en-us/articles/115013657007-Create-Assessment-Form-from-csv-Assetic-Python-SDK-
"""
inifile = None
logfile = None

#Define location of ini file, or leave as None for default locations
#The example below shows how to set the inifile.  Remove the # 
#inifile = "c:/users/me/assetic_sandbox.ini"

#Define location of log file, or leave as None for default which is to
#prints log messages to the window
#The example below shows how to set the logfile.  Remove the # 
#logfile = "c:/temp/form_create.log"

import assetic
asseticsdk=assetic.AsseticSDK(inifile,logfile,"Info")
assessmenthelper = assetic.AssessmentHelper()
assessmenthelper.assessment_form_create_prompter()

