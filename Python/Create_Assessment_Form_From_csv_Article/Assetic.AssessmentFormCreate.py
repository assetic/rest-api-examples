"""
    Example script to create an assessment form
    (Assetic.AssessmentFormCreate.py)
"""
import assetic
asseticsdk = assetic.AsseticSDK(None,None,'info')

def main():
    assesshelp = assetic.AssessmentHelper(asseticsdk.client)

    form_repr = assetic.AssessmentFormCreateRepresentation()
    form_repr.form_name = "HVACAssessment"
    form_repr.form_label = "HVAC Assessment"
    #The form is for an asset
    form_repr.form_level = "Asset"
    form_repr.version = 1
    form_repr.can_add_attachment = True
    form_repr.tab_label = "HVAC L2 Assessment"
    form_repr.layout_pattern = "column: 2 equal columns"
    widget_labels = list()
    ##Define the labels of the 2 control groups
    widget_labels.append("Facility Detail")
    widget_labels.append("System Detail")   
    form_repr.widget_labels = widget_labels
    form_repr.label_formatter = assesshelp.insert_space_in_camelcase

    #IF source file uses "Multi line Text" instead of "MultiLineText"
    #and IF it uses "Numeric" instead of "NumericStepper"
    assesshelp.control_types["MultiLineText"] = "Multi line Text"
    assesshelp.control_types["NumericStepper"] = "Numeric"

    #The column order in the csv file is as follows
    csv_structure = ("Label", "Type", "Attributes", "Widget", "Required", "Hint")
    #define where the csv file is
    csvfile = "C:/temp/FormFields.csv"
    ##create the form
    result = assesshelp.create_form_from_csv(form_repr,csvfile,csv_structure)
            
if __name__ == "__main__":
    main()
