"""
    Example script to retrieve a request (Assetic.WorkRequestGet.py)
    Get Request for given ID
"""
import assetic
import logging
##Assetic SDK instance.
asseticsdk = assetic.AsseticSDK("c:/users/kwilton/assetic.ini",None,"Info")
##work request API
wrapi = assetic.WorkRequestApi()

wrguid = '54e43a5a-7da4-e611-946c-06edd62954d7'  #demo
try:
    wrget = wrapi.work_request_get(wrguid)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1}".format(e.status,e.reason))
    exit()

##set logger format to make it easier to read
formatter = logging.Formatter('%(levelname)s - %(message)s')
asseticsdk.logger.handlers[0].setFormatter(formatter)

##dispay some of the key fields using logger
asseticsdk.logger.info("Request ID: {0}".format(wrget.get('FriendlyIdStr')))
asseticsdk.logger.info("Status: {0}".format(wrget.get('WorkRequestStatus')))
asseticsdk.logger.info("Work Order ID: {0}".format(wrget.get('WorkOrderId')))
##each comment entered in supporting information is a separate record
for h in wrget.get('SupportingInformationHistory'):
    asseticsdk.logger.info("History Date: {0}, Comment: {1}".format(
        h.get('CreatedDateTime'),h.get('Description')))
