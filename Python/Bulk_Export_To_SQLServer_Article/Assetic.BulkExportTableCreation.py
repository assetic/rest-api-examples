# Assetic.BulkExportTableCreation
# Create the target database table for a given search profile
import assetic

# initialise Assetic Python SDK.  Set logging mode to 'info'
asseticsdk = assetic.AsseticSDK('C:/Users/myself/assetic.ini', None, 'info')

sync = assetic.SyncToLocalProcesses()
# database connection required.
# Database will be tested for existence of the target table and script will not
# be created if table exists
dbtools = assetic.DB_Tools('dbserver', 'assetic_sync')

# define the profile that the table SQL create syntax is to be built for
profilemapping = assetic.SearchProfileRepresentation()
profilemapping.profileguid = "52e11423-dc76-e511-944d-06ed862954d5"
profilemapping.profilename = "Component Sync"
profilemapping.tablename = "expcomp"
profilemapping.keyfield = "[Component ID]"
profilemapping.logtable = "expcomp_log"
profilemapping.useinternalnames = False
profilemapping.replacespaces = True
profilemapping.spacedelimiter = "_"
profilemapping.set_empty_as_null = True
profilemapping.allow_delete = True
# note that changing profilemapping.useinternalnames = TRUE changes
# the SQL created to use Assetic internal field names for database column names

# get column mappings and data types for search profile and build table
sync.build_table_from_search(dbtools, None, profilemapping, True)