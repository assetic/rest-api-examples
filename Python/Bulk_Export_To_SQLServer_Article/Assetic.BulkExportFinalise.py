"""
Assetic.BulkExportFinalise.py

Finalises the bulk export process.

When this py script is run it checks to see if there are any outstanding bulk
exports awaiting download.  This is indicated by the database table
assetic_sync_manager where status of an export is 'In Progress'
The Assetic API's are used to check if the export is ready for download and if
so, the data is downloaded and sync with the current data.
The 'initialise' process defines the database table to sync to
Full documentation: https://assetic.zendesk.com/hc/en-us/articles/236156708-Bulk-Export-Process-Assetic-Python-SDK-
Author: Kevin Wilton (Assetic)
"""
import assetic
# Set logging to "Info" level
asseticsdk = assetic.AsseticSDK("C:/Users/myself/assetic.ini", None, "Info")
sync = assetic.SyncToLocalProcesses()
# Using trusted connection
response = sync.dbsync_finalise("dbserver", "asseticsync")
