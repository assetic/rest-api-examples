"""
Assetic.BulkExportInitialise.py

Initialises the bulk export process.
Define a set of search profiles to be exported, and the corresponding db tables
to write the data to, and log changes to
When this py script is run it uses the Assetic 'export all' api to initiate the
export, which uses the Assetic background worker to queue and execute the export
A second 'finalise' step run via a separate py script to check to see if the
export is complete, and if so downloads the exported data
Assumes the user running the script can establish a connection with the
database
Full documentation: https://assetic.zendesk.com/hc/en-us/articles/236156708-Bulk-Export-Process-Assetic-Python-SDK-
Author: Kevin Wilton (Assetic)
"""
import assetic
# Define location of configuration file, log file, log level
asseticsdk = assetic.AsseticSDK("C:/users/myself/assetic.ini", None, "Error")

sync = assetic.SyncToLocalProcesses()

# Define an empty list of profiles
profiles = []

# Profile for 'Assets Sync'
profilemapping = assetic.SearchProfileRepresentation()
profilemapping.profileguid = "d9a350d7-8078-e611-946c-06edd62954d7"
profilemapping.profilename = "Water Pressure Pipes"
profilemapping.tablename = "waterpressurepipes"
profilemapping.keyfield = "[Asset Id]"
profilemapping.logtable = "waterpressurepipes_log"
profilemapping.useinternalnames = False
# Use field labels which will have whitespaces, so remove the whitespaces
profilemapping.replacespaces = True
profilemapping.spacedelimiter = "_"  # Replace whitespace with "_"
# If the data is an empty string set it as NULL in the database
profilemapping.set_empty_as_null = True
# If a record is no longer in the Assetic data then remove it from the DB
profilemapping.allow_delete = True
# The column name may have characters such as "/".  Replace with a "_"
profilemapping.replace_special_characters = {
            "(": "_",
            ")": "_",
            "-": "_",
            "/": "_"
        }
# Rather than ensuring the target columns are long enough, truncate the data
profilemapping.truncation_indicator = "..."
# If a profile has less than 10,000 records, force it to export via background worker
profilemapping.background_export = True
# add profile definition to profiles list
profiles.append(profilemapping)

# Profile for 'Component Sync'
profilemapping = assetic.SearchProfileRepresentation()
profilemapping.profileguid = "65ffd995-4e76-e611-946c-06edd62954d7"
profilemapping.profilename = "All Components"
profilemapping.tablename = "allcomponents"
profilemapping.keyfield = "[Component Id]"
profilemapping.logtable = "allcomponents_log"
profilemapping.useinternalnames = True
profilemapping.background_export = False
profiles.append(profilemapping)

# Initialise the export.
sync.dbsync_initiate('dbserver', 'asseticsync', profiles,
                     force_export=False, always_background_export=False)
