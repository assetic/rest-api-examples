"""
    Example script to search for work order(Assetic.WorkOrderGetByFriendlyIdAPI.py)
    Gets work orders that satify filter - in this case by friendly id
"""
import assetic
try:
    import configparser as configparser
except:
    import ConfigParser as configparser
import logging

# Read ini
settings = configparser.ConfigParser()
settings.read('C:/Users/sjsam/Documents/rest-api-examples/Python/assetic.ini')
##configuration of host, authentication, debugging 
conf = assetic.configuration.Configuration()
conf.host = settings.get('environment', 'url')
conf.username = settings.get('auth', 'username')
conf.password = settings.get('auth', 'api_key')
auth = conf.get_basic_auth_token()
conf.logger_file = 'C:/temp/logger.txt'
conf.debug = True
if conf.debug == True:
    logging.basicConfig() 
    logging.getLogger().setLevel(logging.DEBUG)

kwargs = {'request_params_page':1,
         'request_params_page_size':2,
          'request_params_filters':'FriendlyId=86908'
    }

##Assetic SDK instance.
cli = assetic.ApiClient(conf,"Authorization",auth)
wkoapi = assetic.WorkOrderApi(cli)
try:
    wko = wkoapi.work_order_integration_api_get(**kwargs)
except assetic.rest.ApiException as e:
    print('Status {0}, Reason: {1}'.format(e.status,e.reason))
else:
    if wko.get('TotalResults') == 1:
        wkodataarr = wko.get('ResourceList')
        wkodata = wkodataarr[0]
        wkoguid = wkodata.get('Id')
        print(wkoguid)
