"""
Update an asset, component and dimensions
(Assetic.AssetsPutComplete.py)
"""
import assetic

# Define location of configuration file, log file, log level
asseticsdk=assetic.AsseticSDK("c:/users/you/assetic.ini", None, "Info")
# Asset Tools API
assettools = assetic.AssetTools()
    
def main(assetguid,componentguid,dimensionguid):
    """
    Update the asset, components, and component dimensions
    """

    ##instantiate the complete asset representation
    complete_asset_obj = assetic.AssetToolsCompleteAssetRepresentation()
    
    #create an instance of the complex asset object
    asset = assetic.Assetic3IntegrationRepresentationsComplexAssetRepresentation()
    ##set asset guid to update
    asset.id = assetguid
    ##set fields to update
    asset.asset_external_identifier = "upd"
    attributes = {"Comment":"a revised comment","Link":"www.assetic.com"}
    asset.attributes = attributes
    
    ##Set the asset_representation to the defined asset values
    complete_asset_obj.asset_representation = asset

    ##array list to put component plus dimensions
    components_and_dims = []
    
    #instantiate complete component representation
    component_obj = assetic.AssetToolsComponentRepresentation()
    
    ##create an instance of the component representation
    component = assetic.Assetic3IntegrationRepresentationsComponentRepresentation()
    component.id = componentguid
    ##set fields to update
    component.design_life = 50
    component_obj.component_representation = component

    ##create an array for the dimensions to be added to the component
    dimlist = []
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    #set the id fields
    dim.id = dimensionguid
    dim.component_id = componentguid
    #set the field to update
    dim.network_measure = 5
    ##append to dimension array
    dimlist.append(dim)

    ##Add the dimension array to the component
    component_obj.dimensions = dimlist
    
    ##Add component to the list
    components_and_dims.append(component_obj)

    ##add the component & dims array to the complete asset object
    complete_asset_obj.components = components_and_dims

    ##update the complete asset
    errcode = assettools.update_complete_asset(complete_asset_obj,False)
    if errcode > 0:
        return
    print("success")
    
##This will run the method main() to kickstart it all
if __name__ == "__main__":
    #set the IDs
    assetguid = "88439a40-6e1e-e711-946c-06edd62954d7"
    componentguid = "8e439a40-6e1e-e711-946c-06edd62954d7"
    dimensionguid = "90439a40-6e1e-e711-946c-06edd62954d7"
    #execute the main function
    main(assetguid,componentguid,dimensionguid)
