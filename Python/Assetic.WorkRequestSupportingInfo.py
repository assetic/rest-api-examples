"""
    Example script to POST a comment to supporting info of a request
    and then get back all comments (to illustrate GET)
    (Assetic.WorkRequestSupportingInfo.py)
"""
import assetic
import logging
from datetime import datetime, timedelta  #use to get current date

##Assetic SDK instance.
asseticsdk = assetic.AsseticSDK("c:/users/kwilton/assetic.ini",None,"Info")
##work request API
wrapi = assetic.WorkRequestApi()

#get current datetime
dt = datetime.now()

wrguid = '54e43a5a-7da4-e611-946c-06edd62954d7'  #set wr guid
#object for POST payload
info = assetic.Assetic3IntegrationRepresentationsSupportingInformation()
info.description = "Comment {0}".format(str(dt))
#created date can be null current date, or be set as a past date
#in this example set created date to yesterday
info.created_date_time = dt - timedelta(days=1)

##post the comment
try:
    wrpost = wrapi.work_request_add_supporting_information_for_work_request(
        wrguid,info)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
        e.status,e.reason,e.body))
    exit()

##get all comments for the request
try:
    wrget = wrapi.work_request_get_supporting_information_for_work_request(
        wrguid)
except assetic.rest.ApiException as e:
    asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
        e.status,e.reason,e.body))
    exit()

##set logger format to make it easier to read
formatter = logging.Formatter('%(levelname)s - %(message)s')
asseticsdk.logger.handlers[0].setFormatter(formatter)

##display all comments using logger
##each comment entered in supporting information is a separate record
for h in wrget["ResourceList"]:
    asseticsdk.logger.info(
        "Date: {0}, Comment: {1}, Created By: {2}".format(
        h.get("CreatedDateTime"),h.get("Description"),h.get("CreatedByDisplayName")))
