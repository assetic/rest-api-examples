from qgis.PyQt.QtCore import QCoreApplication, QVariant, QDate
from qgis.core import (QgsProcessing, QgsProject, QgsRendererCategory, QgsSymbol, QgsCategorizedSymbolRenderer,
                       QgsFeatureSink, QgsField, edit, QgsProcessingParameterNumber, Qgis,
                       QgsProcessingException, QgsProcessingParameterField, QgsProcessingParameterFile,
                       QgsProcessingAlgorithm, QgsVectorLayer, QgsProcessingParameterEnum,
                       QgsProcessingParameterFeatureSource, QgsProcessingParameterString,
                       QgsProcessingParameterFeatureSink, QgsProcessingParameterVectorDestination)
from qgis import processing
from qgis.PyQt.QtGui import QColor
import pandas as pd # pandas is required for the Kerb & Channel extension
import os
from datetime import datetime


class PredictorMergeAlgorithm(QgsProcessingAlgorithm):
    """
    Processing Algorithm for merging GIS base layer and Predictor results into a new layer
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'
    PREDICTOR_URL = 'PREDICTOR_URL'
    GIS_JOIN_FIELD = 'GIS_JOIN_FIELD'
    PREDICTOR_JOIN_FIELD = 'PREDICTOR_JOIN_FIELD'
    OUTPUT_LAYER_NAME = 'OUTPUT_LAYER_NAME'
    CALENDAR_YEAR_START = 'CALENDAR_YEAR_START'
    PREDICTOR_JOIN_FILE = 'PREDICTOR_JOIN_FILE'
    EXTENSIONS = 'EXTENSIONS'

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return PredictorMergeAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'predictormerge'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Predictor Merge')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('Brightly')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'brightlyscripts'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        msg = """
            This tool that merges a vector layer and with a Brightly Predictor Results file.

            Predictor Simulation results contain a record (row) per Asset per Year of modelling.
            The corresponding GIS layer is expected to have a single ‘Feature’ per Asset.
            The Asset identifier is held against the Feature as an ‘Attribute’.
        
            Merging the Predictor results with the GIS layer will yield a new layer where the feature
            is duplicated for each Year in the Predictor results.
        
            The fields from the Predictor results are appended to the existing attribute fields of the GIS layer.
            This enables the geographic analysis and visualization of the Predictor results.
        
            The resultant layer will have layer symbology, metadata and temporal settings configured.
    """
        return self.tr(msg)

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # initiate 'Extensions' custom class for non-standard processing options
        extensions = Extensions()

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Input layer'),
                [QgsProcessing.SourceType.TypeVectorAnyGeometry]
            )
        )

        # We define the GIS Join Field
        self.addParameter(
            QgsProcessingParameterField(
                self.GIS_JOIN_FIELD,
                self.tr('GIS Join Field'),
                defaultValue="CompKey",
                parentLayerParameterName=self.INPUT
            )
        )

        # We add a destination
        self.addParameter(
            QgsProcessingParameterVectorDestination(
                self.OUTPUT,
                self.tr('Output layer')
            )
        )

        # We add a Predictor URL
        self.addParameter(
            QgsProcessingParameterString(
                self.PREDICTOR_URL,
                self.tr('Predictor URL link')
            )
        )

        # We add a parameter for Predictor Join Field
        self.addParameter(
            QgsProcessingParameterString(
                self.PREDICTOR_JOIN_FIELD,
                self.tr('Predictor Join Field'),
                defaultValue="Unique Asset ID"
            )
        )

        # We set calendar start year
        self.addParameter(
            QgsProcessingParameterNumber(
                self.CALENDAR_YEAR_START,
                self.tr('Calendar Year for Predictor Year 0'),
                defaultValue=datetime.now().year
            )
        )

        # parameter that defines extensions to run custom modification, such as kerb data
        self.addParameter(
            QgsProcessingParameterEnum(
                self.EXTENSIONS,
                self.tr('Custom extensions'),
                extensions.action_options,
                defaultValue="Not Applicable"
            )
        )

        # We set calendar start year
        self.addParameter(
            QgsProcessingParameterFile(
                self.PREDICTOR_JOIN_FILE,
                self.tr('Custom extension file.'),
                defaultValue=None,
                optional = True
            )
        )

    def symbology_category_setting(self, field_val, col_r, col_g, col_b, label, layer):
        """

        """

        # color
        range_color = QColor.fromRgb(col_r, col_g, col_b)
        # hardcode opacity
        opacity = 100

        # create symbol and set properties
        symbol = QgsSymbol.defaultSymbol(layer.geometryType())
        symbol.setColor(range_color)
        symbol.setOpacity(opacity)
        if layer.geometryType().name == "Polygon":
            # set the polygon outline to the same colour
            symbol.symbolLayer(0).setStrokeColor(range_color)
        # create range and append to rangeList
        cat_setting = QgsRendererCategory(field_val, symbol, label)
        return cat_setting

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        It is initiated by the geoprocessing UI
        @param parameters: QGIS geoprocessing parameters
        @param context: QGIS geoprocessing context
        @param feedback: QGIS geoprocessing feedback
        """

        # initiate 'Extensions' custom class for non-standard processing options
        extensions = Extensions()
        extensions.parameters = parameters
        extensions.context = context
        extensions.feedback = feedback

        # Retrieve the feature source
        source = self.parameterAsSource(
            parameters,
            self.INPUT,
            context
        )

        # If source was not found, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSourceError method to return a standard
        # helper text for when a source cannot be evaluated
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))

        # Get the Predictor Simulation Id from the URL
        url_parts = parameters["PREDICTOR_URL"].split("/")
        if len(url_parts) > 4 and url_parts[3] == "result":
            # we've got the right splitting
            simulation_id = url_parts[4]
        else:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.PREDICTOR_URL))

        # download the predictor file
        pf = processing.run(
            "native:filedownloader", {
                'URL': parameters["PREDICTOR_URL"], 'METHOD': 0, 'DATA': '', 'OUTPUT': 'TEMPORARY_OUTPUT'}
            , is_child_algorithm=True, context=context, feedback=feedback)
        local_predictor_file = pf['OUTPUT']

        if feedback.isCanceled():
            return {}

        # Kerb and Channel have a different relationship for Unique Asset ID, need to apply different comp key
        # and osi field
        if parameters["EXTENSIONS"] and parameters["EXTENSIONS"] > 0:
            if parameters["PREDICTOR_JOIN_FILE"] and os.path.isfile(parameters["PREDICTOR_JOIN_FILE"]):
                kwargs = {"local_predictor_file": local_predictor_file}
                feedback.pushInfo(f'Running extension: {parameters["EXTENSIONS"]}')
                local_predictor_file = extensions.process(parameters["EXTENSIONS"], **kwargs)
                # need filename with forward slashes to be able to load ias vector layer!
                local_predictor_file = local_predictor_file.replace("\\", "/")
            else:
                msg = f"Extension file {parameters['PREDICTOR_JOIN_FILE']} for {parameters['EXTENSIONS']}"
                raise QgsProcessingException(msg)

        # load the predictor file
        feedback.pushInfo(f'Loading Predictor Results file as a layer: {local_predictor_file}')
        pvl = QgsVectorLayer(f"file://:{local_predictor_file}?delimiter=,", "predictor_data", "delimitedtext")
        feedback.pushInfo(f'Loaded Predictor file as a layer: {local_predictor_file}')

        if feedback.isCanceled():
            return {}

        # ensure Unique Asset ID field exists, check it's type
        p_join_field_type = None
        for field in pvl.fields():
            if parameters["PREDICTOR_JOIN_FIELD"] == field.name():
                p_join_field_type = field.typeName()
                break

        # ensure GIS field with Unique Asset ID exists, and ensure same type
        g_join_field_type = None
        for field in source.fields():
            if parameters["GIS_JOIN_FIELD"] == field.name():
                g_join_field_type = field.typeName()
                break

        if not p_join_field_type or not g_join_field_type or \
                not p_join_field_type.lower() == g_join_field_type.lower():
            msg = "Join fields type mismatch. \r\n"\
                  f"Predictor field type: {p_join_field_type}, gis field type: {g_join_field_type}"
            raise QgsProcessingException(msg)

        # clone predictor csv layer into an in-memory layer
        feedback.pushInfo(f'Cloning Predictor Layer')
        pvl.selectAll()
        clone_layer = processing.run("native:saveselectedfeatures", {'INPUT': pvl, 'OUTPUT': 'memory:'})['OUTPUT']
        pvl.removeSelection()
        feedback.pushInfo(f'Predictor Layer Cloned')

        if feedback.isCanceled():
            return {}

        start_year = parameters["CALENDAR_YEAR_START"]
        cal_year_field_name = "Calendar_Year"

        # add Calendar Year field to in-memory Predictor data (couldn't do it against the csv based layer)
        pv = clone_layer.dataProvider()
        pv.addAttributes([QgsField(cal_year_field_name, QVariant.Int)])
        clone_layer.updateFields()

        cal_year_field_idx = clone_layer.fields().indexOf(cal_year_field_name)
        if cal_year_field_idx < 0:
            # field not added
            raise QgsProcessingException(f"Calendar field '{cal_year_field_name}' could not be added")

        # grab total records for use with progress bar
        total = 100.0 / clone_layer.featureCount() if source.featureCount() else 0
        cnt = 0
        feedback.pushInfo(f'Setting Calendar Year for Predictor data')
        with edit(clone_layer):
            for f in clone_layer.getFeatures():
                # Stop the algorithm if cancel button has been clicked
                if feedback.isCanceled():
                    break
                # apply the value
                cal_year = f["Year"] + start_year
                clone_layer.changeAttributeValue(f.id(), cal_year_field_idx, cal_year)

                # Update the progress bar
                cnt += 1
                feedback.setProgress(int(cnt * total))

        # Prepare payload for merge with GIS data
        payload = {
            "INPUT": parameters["INPUT"],
            "FIELD": parameters["GIS_JOIN_FIELD"],
            "INPUT_2": clone_layer,
            "FIELD_2": parameters["PREDICTOR_JOIN_FIELD"],
            "FIELDS_TO_COPY": [
            ],
            "METHOD": 0,
            "DISCARD_NONMATCHING": False,
            "PREFIX": "p_",
            "OUTPUT": parameters["OUTPUT"]
        }

        feedback.pushInfo(f'output param {parameters["OUTPUT"]}')

        feedback.pushInfo(f'Merge Predictor with {parameters["INPUT"]}')
        result = processing.runAndLoadResults(
            "native:joinattributestable", payload, context=context, feedback=feedback)

        if feedback.isCanceled():
            return {}

        feedback.pushInfo(f'Prepare resultant merged layer properties')
        new_layer = None
        for layer in QgsProject.instance().mapLayers().values():
            if layer.dataProvider().name() == "memory":
                if layer.id() == result["OUTPUT"]:
                    new_layer = layer
                    break
            elif layer.source() == result["OUTPUT"]:
                new_layer = layer
                break
        if not new_layer:
            raise QgsProcessingException(
                f"Merged layer not found, Id: {result['OUTPUT']}")

        # is the result a valid layer?
        if not new_layer.isValid():
            raise QgsProcessingException("Merge unsuccessful")

        # add metatadata to resultant layer
        result_meta = new_layer.metadata()

        # 1. Do the metadata updating as needed
        result_meta.setTitle("Predictor Simulation Results")
        result_meta.setParentIdentifier(
            f"Predictor Simulation Data source: {parameters['PREDICTOR_URL']}")
        result_meta.setIdentifier(f"Predictor Simulation: '{simulation_id}'")
        result_meta.setType("attribute")
        result_meta.setAbstract("Merged Brightly Predictor results with base GIS layer.  "
                                "For each year and asset in the Predictor data the corresponding GIS feature"
                                " is joined to the base GIS layer using an asset ID key in the GIS layer."
                                "The join is 1:many, resulting in the GIS feature being duplicated "
                                "for each year.")

        # 2. "commit" updated metadata
        new_layer.setMetadata(result_meta)

        # apply symbology
        osi_field = "p_|SI|OSI"
        categorized_renderer = QgsCategorizedSymbolRenderer()
        col_settings = [
            {'RGB': [97, 163, 54, 100], "value": 0, "label": "0"}
            , {'RGB': [122, 204, 68, 100], "value": 1, "label": "1"}
            , {'RGB': [173, 219, 89, 100], "value": 2, "label": "2"}
            , {'RGB': [213, 217, 92, 100], "value": 3, "label": "3"}
            , {'RGB': [247, 174, 107, 100], "value": 4, "label": "4"}
            , {'RGB': [226, 123, 105, 100], "value": 5, "label": "5"}
            , {'RGB': [196, 83, 75, 100], "value": 6, "label": "EoL"}
        ]
        for col in col_settings:
            cat = self.symbology_category_setting(
                col["value"], col["RGB"][0], col["RGB"][1], col["RGB"][2], col["label"], new_layer)
            categorized_renderer.addCategory(cat)
        categorized_renderer.setClassAttribute(osi_field)
        new_layer.setRenderer(categorized_renderer)

        # turn on temporal capabilities
        new_layer.temporalProperties().setIsActive(True)
        new_layer.temporalProperties().setAccumulateFeatures(False)
        new_layer.temporalProperties().setMode(
            Qgis.VectorTemporalMode.FeatureDateTimeStartAndEndFromExpressions
        )
        new_layer.temporalProperties().setLimitMode(Qgis.VectorTemporalLimitMode.IncludeBeginIncludeEnd)
        new_layer.temporalProperties().setStartExpression('make_datetime("p_Calendar_Year",7,1,0,0,0)')
        new_layer.temporalProperties().setEndExpression('make_datetime("p_Calendar_Year" + 1,6,30,23,59,59)')

        # Return the results of the algorithm.  These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        return {self.OUTPUT: new_layer}


class Extensions(object):
    """
    Extensions module to allow non-standard customisations to the process
    Currently supports the non-standard 'Kerb and Channel' layer because the assets
    are modelled with the road assets
    """
    def __init__(self):
        self._parameters = None
        self._context = None
        self._feedback = None

        self._action_options = ["Not Applicable", "Kerb Merge"]

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, value):
        self._parameters = value

    @property
    def context(self):
        return self._context

    @context.setter
    def context(self, value):
        self._context = value

    @property
    def feedback(self):
        return self._feedback

    @feedback.setter
    def feedback(self, value):
        self._feedback = value

    @property
    def action_options(self):
        return self._action_options

    def process(self, action, **kwargs):
        self._feedback.pushInfo(f'action: {action}')
        if action == 1:
            return self.kerb_merge(**kwargs)

    def kerb_merge(self, **kwargs):
        """
        The Predictor result has:
        * the kerb condition in the field |SC|Kerb Condition
        * the compkey (unique asset id) field is for Roads - need to lookup keb GIS id from external file
        Need to return a new Predictor file that has the kerb comp keys and kerb conditiom
        in the osi field
        @param predictor_file: full filename and path of Predictor result file
        @param join_file: full filename and path to file used to get new compkey and condition
        @return: updated predictor file
        """
        predictor_file = kwargs["local_predictor_file"]
        join_file = self.parameters["PREDICTOR_JOIN_FILE"]
        self._feedback.pushInfo(f'predictor file: {predictor_file}')
        self._feedback.pushInfo(f'join file: {join_file}')

        predictor_df = pd.read_csv(predictor_file)
        join_df = pd.read_csv(join_file)

        # merge
        merged_pd = pd.merge(
            left=predictor_df, right=join_df, how="inner", left_on="Unique Asset ID", right_on="Road Comp Key")

        merged_pd["Unique Asset ID"] = merged_pd["Kerb Comp Key"]
        merged_pd["|SI|OSI"] = merged_pd["|SC|Kerb Condition"]

        # write file back
        fname = os.path.basename(predictor_file)
        dname = os.path.dirname(predictor_file)
        newfname = f"joined_{fname}"
        new_predictor_file = os.path.join(dname, newfname)
        merged_pd.to_csv(new_predictor_file, index=False)
        return new_predictor_file
