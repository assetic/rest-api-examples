"""
    Example script to create an assessment form
    (Assetic.AssessmentFormCreate.py)
    Creates an assessment form in Assetic
"""
import assetic
from assetic import Configuration
from assetic import ApiClient
from assetic import AssessmentTools

import re   #to replace camelcase with spaced label
import csv  #to read csv file with form definition
import six
# Assetic SDK instance.
asseticsdk = assetic.AsseticSDK("c:/users/you/assetic.ini",None,"Info")

def main():
    fid=67
    assesshelp = AssessmentHelper(asseticsdk.client)

    form_repr = AssessmentFormCreateRepresentation()
    form_repr.form_name = "ALPACA{0}".format(fid)
    form_repr.form_label = "Alpaca Test Form {0}".format(fid)
    form_repr.form_level = "Asset"
    form_repr.version = 1
    form_repr.can_add_attachment = True
    form_repr.tab_label = "Data Tab"
    form_repr.layout_pattern = "Single"
    form_repr.layout_pattern = "column: 2 equal columns"
    widget_labels = list()
    widget_labels.append("Section 1")
    widget_labels.append("Section 2")
    form_repr.widget_labels = widget_labels
    form_repr.label_formatter = None

    assesshelp.control_types["MultiLineText"] = "Multi line Text"
    assesshelp.control_types["NumericStepper"] = "Numeric"

    csv_structure = None
    csvfile = "C:/temp/HVAC fields.csv"
    assesshelp.create_form_from_csv(form_repr,csvfile,csv_structure)

class AssessmentHelper(object):
    """
    Class to manage simplified processes for creating assessment forms
    such 
    Uses the AssessmentTools class
    """

    def __init__(self, api_client=None):
        config = Configuration()
        if api_client:
            self.api_client = api_client
        else:
            if not config.api_client:
                config.api_client = ApiClient()
            self.api_client = config.api_client
            
        self.logger = config.packagelogger

        self.assesstool = assetic.AssessmentTools()
        self._control_types = self.initiate_control_types()

    @property
    def control_types(self):
        """
        dictionary of control type.  The dictionary values can be changed
        to match the source data.  The corresponding keys are tested for when
        determine which control to create
        """
        return self._control_types
    @control_types.setter
    def control_types(self,control_types):
        self._control_types = control_types    
   
    def create_form_from_csv(self,form_repr,csvfile,structure=None):
        """
        create an assessment form from a csv file definition
        :param form_repr: A representation containing the core form
        creation details. AssessmentFormCreateRepresentation
        :param csvfile: The file containing the form definition
        :param structure: an optional mapping of row number in csv file to
        form creation purpose.  e.g row 1 = control header, row 2 = control type
        :returns: >0=error, 0 = Success
        """

        form_name = form_repr.form_name
        if form_repr.form_label == None:
            form_label = form_repr.form_name
        else:
            form_label = form_repr.form_label
        form_level = form_repr.form_level
        version = form_repr.version
        can_add_attachment = form_repr.can_add_attachment
        tab_label = form_repr.tab_label
        layout_pattern = form_repr.layout_pattern
        widget_labels = form_repr.widget_labels
        
        ##first create the form definition
        form = self.assesstool.new_form(form_name,form_level,form_label)
        ##create tab and widgets
        tab = self.assesstool.add_tab_with_widgets_to_form(form,tab_label,
                    widget_labels,layout_pattern)
        if tab == None:
            ##error encountered
            return 1

        ##get the structure from csv
        rows = self.read_csv_file_into_dict(csvfile,structure)
        if len(rows) == 1:
            return 1

        ##Add the controls
        for row in rows:
            self.process_csv_dict_row(row,tab,widget_labels)
            
        ##now create form
        self.assesstool.create_form(form)

    def read_csv_file_into_dict(self,filename,structure=None):
        """
        Read the the contents of a csv file into a representation that can be
        used to build the controls on a form
        :param filename: the file to read
        :param structure: the layout mapping of the rows in the file
        :returns: a list of the representation
        Assetic3IntegrationRepresentationsAssessmentFormFormControlRepresentation()
        """
        if structure == None:
            structure =  ("Label","Type","Attributes","Widget","Required")

        rows = list()
        if six.PY2:
            with open(filename, 'rt') as csvfile:
                readCSV = csv.DictReader(
                            csvfile,fieldnames=structure,delimiter=',')            
                for row in readCSV:
                    rows.append(row)
        else:
            with open(filename, 'rt', encoding='utf-8', newline = '') as csvfile:
                readCSV = csv.DictReader(csvfile,fieldnames=structure,
                                         delimiter=',')            
                for row in readCSV:
                    rows.append(row)
        return rows
                

    def process_csv_dict_row(self,field_def,tab,widget_labels):
        """
        The field definition is from csv.DictReader.
        Look for specific headers to determine
        what kind of control to create and it's attributes.
        :param field_def: the dictionary for a field
        :param tab: the tab to add the control to
        :param widget_labels: the widget to add the control to
        must be in the tab
        :returns: a list of the representation
        Assetic3IntegrationRepresentationsAssessmentFormFormControlRepresentation()
        """
        label = None
        if "Label" in field_def:
            label = field_def["Label"]
        else:
            #assume first element is the label
            label = field_def[0]

        #check if hint for control but otherwise leave null
        hint = None
        if "Hint" in field_def:
            hint = field_def["Hint"]
        #check if name for control but otherwise leave null
        name = None
        if "Name" in field_def:
            name = field_def["Name"]

        #check if 'required' is set
        required = None
        if "Required" in field_def:
            required = field_def["Required"]
            
        #check if widget is defined.  If not set as first label
        widget_label = widget_labels[0]
        if "Widget" in field_def and field_def["Widget"] in widget_labels:
            widget_label = field_def["Widget"]

        ##now test for type and create control
        if "Type" not in field_def:
            #assume text box
            field_def["Type"] = "textbox"
        if "Type" in field_def:
            if field_def["Type"].lower() == \
                   self._control_types["TextBox"].lower():
                #Standard text box
                #get length
                length = 200   #Default field length(will be used if undefined)
                if "Length" in field_def:
                    length = self.get_numeric_or_default(
                                field_def["Length"],200)
                elif "Attributes" in field_def:
                    length = self.get_numeric_or_default(
                                field_def["Attributes"],200)
                control = self.assesstool.new_text_control(label,name,length,
                                hint=hint,required=required)
            elif field_def["Type"].lower() == \
                     self._control_types["MultiLineText"].lower():
                ##this is a multiline text box
                length = 1000   #Default field length(will be used if undefined)
                if "Length" in field_def:
                    length = self.get_numeric_or_default(
                                field_def["Length"],1000)
                elif "Attributes" in field_def:
                    length = self.get_numeric_or_default(
                                field_def["Attributes"],1000)
                control = self.assesstool.new_multiline_text_control(label,
                                    name,length,hint=hint,required=required)
            elif field_def["Type"].lower() == \
                     self._control_types["Dropdown"].lower():
                ##this is a combobox (drop down list)
                #get items
                itemstring = None
                if "DropdownItems" in field_def:
                    itemstring = field_def["DropdownItems"]
                elif "Attributes" in field_def:
                    itemstring = field_def["Attributes"]
                item_delim=";"  #hardcode delimiter for the moment
                key_value_delim="|"  #hardcode delimiter for the moment
                items = self.get_combobox_items(
                                itemstring,item_delim,key_value_delim)
                control = self.assesstool.new_combobox_control(label,name,
                                items=items,hint=hint,required=required)
            elif field_def["Type"].lower() == \
                    self._control_types["Date"].lower():
                ##this is a date picker
                control = self.assesstool.new_date_control(label,name,
                                    hint=hint,required=required)
            elif field_def["Type"].lower() == \
                     self._control_types["DateTime"].lower():
                ##this is a date picker
                control = self.assesstool.new_datetime_control(label,name,
                                    hint=hint,required=required)
            elif field_def["Type"].lower() == \
                     self._control_types["NumericStepper"].\
                                         lower():
                ##this is a numeric stepper
                #get items
                sdef = self.get_stepper_defs(field_def)
                control = self.assesstool.new_stepper_control(label,name,
                        sdef["minval"],sdef["maxval"],sdef["increment"],
                        mask=sdef["valueformat"],hint=hint,required=required)
            elif field_def["Type"].lower() == \
                     self._control_types["CheckBox"].lower():
                ##this is a checkbox
                control = self.assesstool.new_checkbox_control(label,name,
                                    hint=hint,required=required)
            elif field_def["Type"].lower() == \
                     self._control_types["Map"].lower():
                ##this is a map control
                control = self.assesstool.new_map_control(label,name,
                                    hint=hint,required=required)
            else:
                ##Assume text
                #get length
                length = 200   #Default field length(will be used if undefined)
                if "Length" in field_def:
                    length = self.get_numeric_or_default(
                                field_def["Length"],200)
                elif "Attributes" in field_def:
                    length = self.get_numeric_or_default(
                                field_def["Attributes"],200)
                control = self.assesstool.new_text_control(label,name,length,
                                    hint=hint,required=required)
        #Add the control definition to the tab
        if control != None:
            self.assesstool.add_control_to_tab_widget(tab,widget_label,
                        control)

    def get_combobox_items(self,itemstring,item_delim=";",key_value_delim="|"):
        """
        given a string decode into a list of key/value pairs and create
        combobox items.
        By default key/value pairs are separated by ";"
        By default "|" for splitting key from value 
        If only value supplied then key = value
        :param itemstring: the string containing key/value pairs
        :param item_delim: delimiter between each set of items (key/value pairs)
        Default = ";"
        :param key_value_delim:delimiter between key and value. default = "|"
        :returns: a list of combobox item representations
        Assetic3IntegrationRepresentationsAssessmentFormFormControlRepresentation
        """
        items = list()
        if itemstring == None or itemstring.strip() == "":
            #no lookups items defined
            return items

        #split into key/value pairs
        pairs = itemstring.split(item_delim)
        for kvp in pairs:
            #split key and value
            itemkvp = kvp.split(key_value_delim)
            if len(itemkvp) == 1:
                #only label defined.  Set key to label
                itemkvp = itemkvp + itemvkp
            #add item representation to items list
            items.append(self.assesstool.new_combobox_item(
                    itemkvp[1],itemkvp[0]))
        return items

    def get_stepper_defs(self,field_def):
        """
        given a form field configuration definition, test if there are settings
        for a numeric stepper 
        :param field_def: the dictionary of settings to test
        :returns: a dictionary of the parameters and their value
        """
        attributes_delim="|"  #hardcode attributes delimiter for the moment
        default_min = 0
        default_max = 0
        default_inc = 0
        default_format = "n0"
        stepperdict = dict()
        ##apply defaults and then override if there is a setting
        stepperdict["minval"] = default_min
        stepperdict["maxval"] = default_max
        stepperdict["increment"] = default_inc
        stepperdict["valueformat"] = default_format
        
        #First look for separately defined parameters
        useatts = True  #flag to indicate use of generic attributes field
        if "MinValue" in field_def:
            stepperdict["minval"] = field_def["MinValue"]
            useatts = False
        if "MaxValue" in field_def:
            stepperdict["maxval"] = field_def["MaxValue"]
            useatts = False
        if "ValueIncrement" in field_def:
            stepperdict["increment"] = field_def["ValueIncrement"]
            useatts = False
        if "ValueFormat" in field_def:
            stepperdict["valueformat"] = field_def["ValueFormat"]
        #If useatts = True set then look for delimited 'Attributes' and split
        if useatts == True and "Attributes" in field_def and \
        field_def["Attributes"] != None:
            stepperdefs = field_def["Attributes"].split(attributes_delim)
            stepperdict["minval"] = self. get_numeric_or_default(
                                            stepperdefs[0],default_min)
            if len(stepperdefs) > 1:
                stepperdict["maxval"] = self.get_numeric_or_default(
                                            stepperdefs[1],1000)
            if len(stepperdefs) > 2:
                stepperdict["increment"] = self.get_numeric_or_default(
                                            stepperdefs[2],1)
            if len(stepperdefs) > 3:
                stepperdict["valueformat"] = stepperdefs[3]  

        return stepperdict
        
    
    def get_numeric_or_default(self,value,default):
        """
        given a string, test if numeric and if not return default
        :param value: the string to test
        :param default: the default vaue.Must be a numeric (is not validated)
        :returns: the reformatted value as a numeric
        """
        try:
            numeric = value/1
        except:
            numeric = default
        return numeric
        
    def insert_space_in_camelcase(self,value):
        """
        given a string, insert a space in from of upper case characters
        or numbers.
        Use to make a user presentable label
        :param value: the string to operate on
        :returns: the reformatted string
        """
        return re.sub('([A-Z]{1})', r' \1',value).strip().strip("_")

    def initiate_control_types(self):
        """
        initiate a dictionary of control type names with default values.
        :returns: dictionary of supported control types
        """
        control_types = dict()
        control_types["TextBox"] = "TextBox"
        control_types["MultiLineText"] = "MultiLineText"
        control_types["Dropdown"] = "Dropdown"
        control_types["Date"] = "Date"
        control_types["DateTime"] = "DateTime"
        control_types["NumericStepper"] = "NumericStepper"
        control_types["CheckBox"] = "CheckBox"
        control_types["Map"] = "Map"
        return control_types
        
class AssessmentFormCreateRepresentation(object):
    """"
    A structure for defining table metadata and relationships between
    search profile names, id's and tables
    """
    def __init__(self,form_name=None,form_label=None,form_level=None,version=1,can_add_attachment=True,tab_label="Tab",layout_pattern="Single",widget_labels="Data",label_formatter=None):
        """
        AssessmentFormCreateRepresentation - a model defining the core form
        details - form, tab, layoutpattern, control group labels
        """
        self.fieldtypes = {
            "form_name": "str",
            "form_label": "str",
            "form_level": "str",
            "version": "int",
            "can_add_attachment": "bool",
            "tab_label":"string",
            "layout_pattern":"string",
            "widget_labels":"list[str]",
            "label_formatter": "string"
        }
        self._form_name = form_name
        self._form_label = form_label
        self._form_level = form_level
        self._version = version
        self._can_add_attachment = can_add_attachment
        self._tab_label = tab_label
        self._layout_pattern = layout_pattern
        if widget_labels == None:
            widget_labels = []
        self._widget_labels = widget_labels
        self._label_formatter = label_formatter
        
    @property
    def form_name(self):
        return self._form_name
    @form_name.setter
    def form_name(self,form_name):
        self._form_name = form_name
    
    @property
    def form_label(self):
        return self._form_label
    @form_label.setter
    def form_label(self,form_label):
        self._form_label = form_label

    @property
    def form_level(self):
        return self._form_level
    @form_level.setter
    def form_level(self,form_level):
        self._form_level = form_level

    @property
    def version(self):
        return self._version
    @version.setter
    def version(self,version):
        self._version = version

    @property
    def can_add_attachment(self):
        return self._can_add_attachment
    @can_add_attachment.setter
    def can_add_attachment(self,can_add_attachment):
        self._can_add_attachment = can_add_attachment

    @property
    def tab_label(self):
        return self._tab_label
    @tab_label.setter
    def tab_label(self,tab_label):
        self._tab_label = tab_label

    @property
    def layout_pattern(self):
        return self._layout_pattern
    @layout_pattern.setter
    def layout_pattern(self,layout_pattern):
        self._layout_pattern = layout_pattern

    @property
    def widget_labels(self):
        return self._widget_labels
    @widget_labels.setter
    def widget_labels(self,widget_labels):
        self._widget_labels = widget_labels

    @property
    def label_formatter(self):
        return self._label_formatter
    @label_formatter.setter
    def label_formatter(self,label_formatter):
        self._label_formatter = label_formatter
                              
    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in six.iteritems(self.fieldtypes):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
    
        
if __name__ == "__main__":
    main()


