"""
    Example script to bulk upload a list of documents using SDK process
"""
import assetic

##bulk upload process
filedir = 'c:/temp/integration/'
inifile = 'C:/Users/kwilton/asseticPreview.ini'
uploadapi = assetic.BulkProcesses(filedir,inifile)
#uploadapi.document_upload()

#download a document (simple test of tools)
filedir = 'C:/temp/'
#docid = "cbd905d1-7d33-e611-945f-06edd62954d7"  #demo
docid = "9d7d6848-2084-e511-944f-06edd62954d7"   #preview
uploadapi.document_get(filedir,docid)

