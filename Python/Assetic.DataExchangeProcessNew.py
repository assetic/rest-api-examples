"""
    Example script to automate data exchange (Assetic.DataExchangeProcessNew.py)
    First of two scripts
    This script uploads to Assetic the files in the 'ToLoad' folder.  The
    filename is parsed to get the asset category which allows the data exchange
    profile to be determined.  A data exchange task is created for the document
    The name of the file is changed to the task id and moved to the 'InProgress'
    folder.  This allows the subsequent script to identify the task and check
    progress
"""
import assetic
import os
import shutil
import sys
import base64

# Assetic SDK instance
asseticsdk = assetic.AsseticSDK("C:/Users/kwilton/assetic.ini",None,"Info")
##API instances
# Document API
docapi = assetic.DocumentApi()
# Data Exchange API
dataexchangeapi = assetic.DataExchangeJobApi()

# File Processing Directory structure
intdir = "C:/temp/Integration/"
sourcedir = intdir + "ToLoad/"
inprogress = intdir + "InProgress/"
errordir = intdir + "Error/"
processeddir = intdir + "Processed/"

##preview
profiles = {'Sewer Nodes': 'c7e6d3ed-3917-e611-9458-06edd62954d7',
            'Roads': 'd889d99b-3317-e611-9458-06edd62954d7'}
##demo
profiles = {'Sewer Nodes': 'c7e6d3ed-3917-e611-9458-06edd62954d7',
            'Roads': '876a0a4e-c732-e611-945f-06edd62954d7',
            'Water Pressure Pipes': '3abd431c-2017-e611-9812-0632cf3be881'}

##Loop through files in folder
files = os.listdir(sourcedir)
for filename in files:
    fullfilename = sourcedir + filename
    #get category, assumes file name has a whitespace between date and category
    filedate, categorywithext = filename.split(None,1)
    category, fileext = os.path.splitext(categorywithext)
    asseticsdk.logger.info("File Date: {0}, Category: {1}".format(
        filedate, category))

    ##read file and encode for upload
    with open(fullfilename, "rb") as f:
        data = f.read()
    if sys.version_info < (3,0):
        filecontents = data.encode("base64")
    else:
        filecontents = base64.b64encode(data)
        filecontents = filecontents.decode(encoding="utf-8", errors="strict")

    ##create file properties object and set values 
    file_properties = \
        assetic.Assetic3IntegrationRepresentationsFilePropertiesRepresentation()
    file_properties.name = filename
    file_properties.mimetype = 'csv'
    file_properties.filecontent = filecontents
    filearray = [file_properties]
    ##create document object and assign values, including file properties
    document = \
        assetic.Assetic3IntegrationRepresentationsDocumentRepresentation()
    document.label = 'Data Exchange - ' + filename
    document.file_extension = 'csv'
    document.document_type = 'DataExchange'
    document.mime_type = 'csv'
    document.doc_group_id = 1
    document.file_property = filearray

    #Upload document and get ID from response
    doc = docapi.document_post(document)
    docid = doc[0].get('Id')

    #get dataexchange profile for category
    profileid = profiles.get(category)

    ##prepare data exchange parameters
    job = assetic.Assetic3IntegrationRepresentationsDataExchangeJobRepresentation()
    job.profile_id = profileid
    job.document_id = docid
    asseticsdk.logger.info("Profile ID:{0}".format(job.profile_id))
    asseticsdk.logger.info("Job Document ID:{0}".format(job.document_id))
    try:
        task = dataexchangeapi.data_exchange_job_post(job)
        #move file to in-progress, and rename to task id to allow checks
        #on success/failure
        destination = inprogress + task + '.csv'
        shutil.move(fullfilename,destination)
    except assetic.rest.ApiException as e:
        #gross error - move to error folder??
        asseticsdk.logger.error('Status {0}, Reason: {1}'.format(
            e.status,e.reason))
        destination = errordir + task + '.csv'
        shutil.move(fullfilename,destination)

