"""
Get a list of assets in radius of a point (Assetic.AssetsSpatialSearch.py)
"""
import assetic
import sys
import json
import six

#Define location of configuration file, log file, log level
asseticsdk=assetic.AsseticSDK("C:/users/kwilton/asseticPreview.ini",
                              None,"Debug")

assetapi = assetic.AssetApi()
componentapi = assetic.ComponentApi()

def main(latitude, longitude, radius, uom):
    """
    Get nearby assets based on a geographic point and radius
    Save as a geojson file
    :param latitude: latitude of point
    :param longitude: longitude of point
    :param radius: search radius
    :param uom: radius unit of measure - meter or kilometer   
    """

    nearby = get_assets_by_spatial(latitude,longitude,radius,uom)
    if len(nearby) == 0:
        return
    if six.PY2 == True:
        with open( "c:/temp/assets.geojson", "wb") as out_file:
            json.dump(nearby, out_file)
    else:
        with open( "c:/temp/assets.geojson", "w", newline="" ) as out_file:
            json.dump(nearby, out_file)
        
    for row in nearby["features"]:
        for geom in row["geometry"]["geometries"]:
            if geom["type"] == "Polygon" or geom["type"] == "MultiPolygon":
                    row["geometry"] = geom
            elif geom["type"] == "Point":
                if geom["coordinates"][0] < 130:
                    msg = "Bad point {0} {1}, Asset ID {2}".format(
                        geom["coordinates"][0],geom["coordinates"][1],
                        row["properties"]["Asset Id"])
                    asseticsdk.logger.warning(msg)
    polyfile = "c:/temp/assets_poly.geojson"
    if six.PY2 == True:
        with open(polyfile, "wb") as out_file:
            json.dump(nearby, out_file)
    else:
        with open(polyfile, "w", newline="" ) as out_file:
            json.dump(nearby, out_file)   
    #for row in nearby["features"]:
    #    msg = "Nearby asset {0}, Category {1}".format(
    #        row["properties"]["Asset Name"],row["properties"]["Asset Category"])
    #    asseticsdk.logger.info(msg)
        
    

def get_assets_by_spatial(latitude,longitude,radius,uom):
    """
    Get nearby assets based on a goegraphic point and radius
    :param latitude: latitude of point
    :param longitude: longitude of point
    :param radius: search radius
    :param uom: radius unit of measure - meter or kilometer
    :return: asset array
    """
    asseticsdk.logger.info("Get the assets near point {0},{1}".format(
        latitude,longitude))

    kw = {"request_params_longitude":longitude,
        "request_params_latitude":latitude,
        "request_params_condition":"near_point",
        "request_params_range":radius,
        "request_params_unit":uom,
        "request_params_page":1,
        "request_params_page_size": 500}
    try:
        assets = assetapi.asset_search_asset_spatial_locations(**kw)
    except assetic.rest.ApiException as e:
        msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
        asseticsdk.logger.error(msg)
        return []

    if assets["TotalResults"] == 0:
        asseticsdk.logger.warning("No results from search")
        return []
    
    numpages = assets["TotalPages"]
    msg = "Total Results: {0}, Total Pages: {1}".format(
        str(assets["TotalResults"]),numpages)
    asseticsdk.logger.info(msg)
    geojson = assets["ResourceList"][0]["Data"].copy()
    if numpages > 1:
        for pagenum in range(2,int(numpages) + 1):
            ##set page number to get
            kw["request_params_page"]=pagenum
            try:
                assets = assetapi.asset_search_asset_spatial_locations(**kw)
            except assetic.rest.ApiException as e:
                msg = "Status {0}, Reason: {1} {2}".format(
                    e.status,e.reason,e.body)
                asseticsdk.logger.error(msg)
                return []
            msg = "Retrieved page {0}".format(str(pagenum))
            asseticsdk.logger.info(msg)
            geojson["features"] = geojson["features"] + \
                        assets["ResourceList"][0]["Data"]["features"][:]
                

    return geojson

##This will run the method main() to kickstart it all
if __name__ == "__main__":
    ##get some nearby assets
    #melton
    latitude = -37.724534
    longitude = 144.736631
    #margaret river
    #latitude = -33.950525
    #longitude = 115.074997
    radius = 5
    uom = "kilometer"
    main(latitude,longitude,radius,uom)
