"""
Get an asset including component and dimensions (Assetic.AssetsGetComplete.py)
"""
import assetic
# Assetic SDK instance
asseticsdk=assetic.AsseticSDK("c:/users/you/assetic.ini", None,"Debug")
#Asset API
assetapi = assetic.AssetApi()
#Component API
componentapi = assetic.ComponentApi()
#Tool API
assettools = assetic.AssetTools()

def main(assetid,attributes = None):
    """
    Given an asset ID get the asset, components, and component dimensions
    :param assetid: Assetic asset GUID or user friendly asset ID    
    """
    ##get the asset
    #asset = get_asset(assetid)
    if asset == None:
        return
    
    #get some of the returned info
    assetid = asset.get("AssetId")
    assetguid = asset.get("Id")
    attributes = asset.get("Attributes")
    zone = attributes.get("Zone")
    locality = attributes.get("Locality")

    msg = "Asset - guid: {0}, Asset: {1}, Zone: {2}, Locality: {3}"\
          .format(assetguid,assetid,zone,locality)
    asseticsdk.logger.info(msg)
    
    ##get the components of the asset
    searchfilter = "AssetId='{0}'".format(assetid)
    componentlist = get_component_list_by_filter(searchfilter)
    if componentlist == None:
        return
    for row in componentlist:
        msg = "Asset has component {0}".format(row["Name"])
        asseticsdk.logger.info(msg)
        
    ##now get a single component by ID
    componentid = componentlist[0]["Id"]
    component = get_component(componentid)

    #get some of the returned info
    if component != None:
        clabel = component["Label"]
        ctype = component["ComponentType"]

        msg = "Component - Label: {0}, Type: {1}".format(clabel,ctype)
        asseticsdk.logger.info(msg)
        
    ##get dimensions
    dimensions = get_dimensions(componentid)

    #get some of the returned info
    if dimensions != None:
        total = dimensions["TotalResults"]
        asseticsdk.logger.info("Number of dimension records: {0}".format(total))
        for dim in dimensions["ResourceList"]:
            dimguid = dim["Id"]
            dmeasure = dim["NetworkMeasure"]
            dunit = dim["Unit"]

            msg = "Dimension - guid: {0}, Measure: {1}, Unit: {2}".format(
                dimguid,dmeasure,dunit)
            asseticsdk.logger.info(msg)
    
def get_asset(assetid):
    """
    Get an asset for the given asset ID
    :param assetid: Assetic asset GUID or user friendly asset ID
    :return: asset response object or None
    """
    asseticsdk.logger.info("Get the asset {0}".format(assetid))
    #api will always return a predefined set of core fields
    #define additional asset attribute fields, must define at least one
    #attribute field names are the internal field names, get using metadata api
    attributes = list()
    attributes.append("Zone")
    attributes.append("Locality")

    try:
        asset = assetapi.asset_get(assetid,attributes)
    except assetic.rest.ApiException as e:
        if e.status == 404:
            asseticsdk.logger.error("Asset ID {0} not found".format(assetid))
        else:
            asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
                e.status,e.reason,e.body))
        return None
    return asset

def get_component(componentid):
    """
    Get an component for the given component ID
    :param componentid: Assetic component GUID or user friendly component ID
    :return: component response object or None
    """
    asseticsdk.logger.info("Get the component {0}".format(componentid))
    try:
        component = componentapi.component_get(componentid)
    except assetic.rest.ApiException as e:
        if e.status == 404:
            msg = "Component for Component GUID {0} not found".format(
                componentid)
            asseticsdk.logger.error(msg)
        else:
            msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
            asseticsdk.logger.error(msg)
        return None
    return component

def get_component_list_by_filter(searchfilter):
    """
    Get an component for the given component ID 
    :param searchfilter: Search filter, same format as aset search filter
    :return: component array or None
    """
    kw = {'request_params_page':1,
    'request_params_page_size':500,
    'request_params_sorts':'Name-desc',
    'request_params_filters':searchfilter}
    asseticsdk.logger.info("Get the components for search filter {0}".format(
        searchfilter))
    try:
        component = componentapi.component_get_0(**kw)
    except assetic.rest.ApiException as e:
        msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
        asseticsdk.logger.error(msg)
        return None
    if component["TotalResults"] > 0:
        return component["ResourceList"]
    else:
        return None

def get_dimensions(componentid):
    """
    Get an dimension array for the given component ID
    :param componentid: Assetic component GUID or user friendly component ID
    :return: dimension response object or None
    """
    asseticsdk.logger.info("Get the dimensions for component {0}".format(
        componentid))
    try:
        dimensions = componentapi.component_get_dimension(componentid)
    except assetic.rest.ApiException as e:
        if e.status == 404:
            msg = "Dimensions for Component {0} not found".format(assetguid)
            asseticsdk.logger.error(msg)
        else:
            msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
            asseticsdk.logger.error(msg)
        return None
    return dimensions

##This will run the method main() to kickstart it all
if __name__ == "__main__":
    assetid = "411142055"
    main(assetid,["Zone","Comment"])
