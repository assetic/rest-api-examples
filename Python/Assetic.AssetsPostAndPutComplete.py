"""
Create an asset, component and dimensions, and then update
(Assetic.AssetsPostAndPutComplete.py)
"""
import assetic
from datetime import datetime  #use to generate a unique asset id
import webbrowser   #use for launching assetic
import subprocess   #use for launching assetic
import threading    #use for launching assetic

#Define location of configuration file, log file, log level
asseticsdk=assetic.AsseticSDK("C:/users/kwilton/assetic.ini",
                              None,"Debug")

assetapi = assetic.ComplexAssetApi()
componentapi = assetic.ComponentApi()
assettools = assetic.AssetTools()
    
def main():
    """
    Create the asset, components, and component dimensions
    Assumes the category does not have autonumber
    """


    ##generate an id based on the current time
    dt = datetime.now()
    assetid = "{0}{1}{2}{3}{4}".format(
        dt.month,dt.day,dt.hour,dt.minute,dt.second)
    ##instantiate the complete asset representation
    complete_asset_obj = assetic.AssetToolsCompleteAssetRepresentation()
    
    #create an instance of the complex asset object
    asset = assetic.Assetic3IntegrationRepresentationsComplexAssetRepresentation()
    ##mandatory fields
    asset.asset_category="Roads"
    asset.status = "Active"
    asset.asset_id = assetid
    asset.asset_name = "RD{0}".format(assetid)

    complete_asset_obj.asset_representation = asset

    ##array list to put component plus dimensions
    components_and_dims = []
    
    #instantiate complete component representation
    component_obj = assetic.AssetToolsComponentRepresentation()
    
    ##create an instance of the component representation
    component = assetic.Assetic3IntegrationRepresentationsComponentRepresentation()
    componentname = "{0}CMP1".format(assetid)
    component.name = componentname  #use to link to dimension
    component.asset_id = assetid
    component.label = "Main Label"  #"Component Name in UI
    component.component_type = "Main"
    component.dimension_unit = "Metre"
    component.network_measure_type = "Length"
    #optional fields
    component.design_life = 50
    component.external_identifier = "Ext{0}".format(componentname)
    component.material_type = "Concrete"  #must exist
    ##Add the component to the components
    component_obj.component_representation = component

    ##create an array for the dimensions to be added to the component
    dimlist = []
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = 75.7
    #dim.component_id = componentname
    dim.unit = "Metre"
    dim.record_type = "Info"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 2.5   #will default as 1 if undefined
    dimlist.append(dim)
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = 3
    #dim.component_id = componentname
    dim.unit = "Metre"
    dim.record_type = "Subtraction"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 1   #will default as 1 if undefined
    dimlist.append(dim)

    ##Add the dimension array to the component
    component_obj.dimensions = dimlist
    
    ##Add component to the list
    components_and_dims.append(component_obj)

    #instantiate complete component representation
    component_obj = assetic.AssetToolsComponentRepresentation()

    ##Create another component
    componentname = "{0}CMP2".format(assetid)
    component = assetic.Assetic3IntegrationRepresentationsComponentRepresentation()
    component.name = componentname
    component.asset_id = assetid
    component.label = "Formation"
    component.component_type = "Formation"
    component.dimension_unit = "Metre"
    component.network_measure_type = "Length"
    #optional fields
    component.design_life = 50
    component.external_identifier = "Ext{0}".format(componentname)
    #component.material_type = "Concrete"  #must exist
    component_obj.component_representation = component

    ##create an array for the dimensions to be added to the component
    dimlist = []
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = 50
    dim.component_id = componentname
    dim.unit = "Metre"
    dim.record_type = "Addition"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 2   #will default as 1 if undefined
    dimlist.append(dim)
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = 5
    dim.component_id = componentname
    dim.unit = "Metre"
    dim.record_type = "Subtraction"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 3   #will default as 1 if undefined
    dimlist.append(dim)

    ##Add the dimension array to the component
    component_obj.dimensions = dimlist
    
    ##Add component with dims to the list
    components_and_dims.append(component_obj)

    ##add the componet & dims array to the complete asset object
    complete_asset_obj.components = components_and_dims

    ##create the complete asset
    response = assettools.create_complete_asset(complete_asset_obj)

    #if response == None:
    #    return
    if len(response.components) > 0:
        #view in Assetic
        assetguid = response.asset_representation.id
        componentguid = response.components[0].component_representation.id
        launch_assetic_dimension(assetguid,componentguid)

        response.asset_representation.asset_external_identifier = "mod {0}".format(
            response.asset_representation.asset_id)
        response.components[0].component_representation.external_identifier = "upd"
        response.components[0].dimensions[0].comments = "upd"
        errcode = assettools.update_complete_asset(response)
        if errcode > 0:
            return
        launch_assetic_dimension(assetguid,componentguid)

    ##check the get components by filter api
    assetid = response.asset_representation.asset_id
    searchfilter = "AssetId='{0}'".format(assetid)
    kw = {'request_params_page':1,
    'request_params_page_size':500,
    'request_params_sorts':'AssetId-desc',
    'request_params_filters':searchfilter}
    compresponse = componentapi.component_get_0(**kw)
    print(compresponse)
    
def main_old():
    """
    Create the asset, components, and component dimensions
    Assumes the category does not have autonumber
    """
    ##generate an id based on the current time
    dt = datetime.now()
    assetid = id = "{0}{1}{2}{3}{4}".format(
        dt.month,dt.day,dt.hour,dt.minute,dt.second)    
    measure = 50  #this is the first measure we'll apply to the dimension
    newmeasure = 75  #this is the updated measure
    
    ##create the asset

    category = "Roads"
    name = "RD{0}".format(assetid)
    status = "Active"
    asset = create_asset(assetid,category,name,status)
    if asset == None:
        return

    #get some of the returned info
    assetid = asset["Data"][0]["AssetId"]
    assetguid = asset["Data"][0]["Id"]

    msg = "Asset - guid: {0}, Asset: {1}"\
          .format(assetguid,assetid)
    asseticsdk.logger.info(msg)
    
    ##create the component
    componentname = "{0}CMP1".format(assetid)
    #componentid = "e9de2584-c4bd-e611-814f-0666fcfde033"
    component = create_component(assetid,componentname)

    #get some of the returned info
    if component != None:
        componentguid = component["Data"][0]["Id"]
        clabel = component["Data"][0]["Label"]
        ctype = component["Data"][0]["ComponentType"]

        msg = "Component - Label: {0}, Type: {1}".format(clabel,ctype)
        asseticsdk.logger.info(msg)

        ##update component
        update_component(componentguid,"Gravel")
        
        ##create dimensions
        dimensions = create_dimension(componentguid,measure)

        #get some of the returned info
        if dimensions != None:
            dimguid = dimensions["Id"]
            dmeasure = dimensions["NetworkMeasure"]
            dunit = dimensions["Unit"]

            msg = "Dimension - guid: {0}, Measure: {1}, Unit: {2}".format(
                dimguid,dmeasure,dunit)
            asseticsdk.logger.info(msg)

            ##Now update the dimension
            errcode = update_dimension(componentguid,dimguid,newmeasure)
        #view in Assetic
        launch_assetic_dimension(assetguid,componentguid)
    
def create_asset(assetid,category,name,status):
    """
    Create an asset for the given asset ID and category
    :param assetid: Assetic user friendly asset ID
    :param assetid: asset category name
    :param assetid: asset name
    :param assetid: status - "Active" or "Pending"
    :return: asset response object or None
    """
    asseticsdk.logger.info("Create the asset {0}".format(assetid))

    #create an instance of the complex asset object
    asset = assetic.Assetic3IntegrationRepresentationsComplexAssetRepresentation()
    ##mandatory fields
    asset.asset_category=category
    asset.status = status
    asset.asset_id = assetid
    asset.asset_name = name

    ##now execute the request
    try:
        newasset = assetapi.complex_asset_post(asset)
    except assetic.rest.ApiException as e:
        if e.status == 400:
            msg = "Bad Request. Status {0}, Reason: {1} {2}".format(
                e.status,e.reason,e.body)
            asseticsdk.logger.error(msg)
        else:
            asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
                e.status,e.reason,e.body))
        return None
    return newasset
   
def create_component(assetid,componentname):
    """
    Create an component for the given asset ID and component name
    :param assetid: Assetic component GUID or user friendly component ID
    :param componentname: Assetic component name
    :return: component response object or None
    """
    asseticsdk.logger.info("Create the component {0}".format(componentname))

    #create instance of component and set minimum values
    component = assetic.Assetic3IntegrationRepresentationsComponentRepresentation()
    component.name = componentname
    component.asset_id = assetid
    component.label = "Main"
    component.component_type = "Main"
    component.dimension_unit = "Metre"
    component.network_measure_type = "Length"
    #optional fields
    component.design_life = 50
    component.external_identifier = "Ext{0}".format(componentname)
    component.material_type = "Concrete"  #must exist

    try:
        component = componentapi.component_post(component)
    except assetic.rest.ApiException as e:
        if e.status == 400:
            msg = "Component for Component GUID {0} not found".format(assetguid)
            asseticsdk.logger.error(msg)
        else:
            msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
            asseticsdk.logger.error(msg)
        return None
    return component

def update_component(componentid,material):
    """
    Update a component for the given component GUID
    :param componentid: Assetic component GUID
    :param dimensionid: Assetic dimension GUID
    :param material: The new material to apply to the dimension
    :return: return code 0=success,>0=error
    """
    asseticsdk.logger.info("Update the material for component {0}".format(
        componentid))

    #create instance of component and set minimum values
    component = assetic.Assetic3IntegrationRepresentationsComponentRepresentation()
    component.id = componentid
    component.material_type = material  #must exist
    
    return assettools.update_full_asset(None,component,None)
    
def create_dimension(componentid,measure):
    """
    Create a dimension for the given component ID
    :param assetid: Assetic component GUID or user friendly component ID
    :param measure: The measure to apply to the dimension
    :return: dimension response object or None
    """
    asseticsdk.logger.info("Create the dimensions for component {0}".format(
        componentid))

    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = measure
    dim.unit = "Metre"
    dim.record_type = "Addition"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 2   #will default as 1 if undefined

    try:
        newdim = componentapi.component_post_dimension(componentid,dim)
    except assetic.rest.ApiException as e:
        if e.status == 404:
            msg = "Dimensions for Component {0} not found".format(assetguid)
            asseticsdk.logger.error(msg)
        else:
            msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
            asseticsdk.logger.error(msg)
        return None
    return newdim

def update_dimension(componentid,dimensionid,measure):
    """
    Update a dimension for the given component and dimension GUID
    :param componentid: Assetic component GUID
    :param dimensionid: Assetic dimension GUID
    :param measure: The measure to apply to the dimension
    :return: return code 0=success,>0=error 
    """
    asseticsdk.logger.info("Update the dimensions for component {0}".format(
        componentid))

    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.id = dimensionid
    dim.component_id = componentid
    dim.network_measure = measure
    #dim.unit = "Metre"
    #dim.record_type = "Addition"   #could also be "Subtraction" or "Info"
    #dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Updated via API"
    dim.multiplier = 3   
    errcode = assettools.update_full_asset(None,None,dim)
    return errcode
    

def get_dimensions(componentid):
    """
    Get an dimension array for the given component ID
    :param assetid: Assetic component GUID or user friendly component ID
    :return: dimension response object or None
    """
    asseticsdk.logger.info("Get the dimensions for component {0}".format(
        componentid))
    try:
        dimensions = componentapi.component_get_dimension(componentid)
    except assetic.rest.ApiException as e:
        if e.status == 404:
            msg = "Dimensions for Component {0} not found".format(assetguid)
            asseticsdk.logger.error(msg)
        else:
            msg = "Status {0}, Reason: {1}".format(e.status,e.reason)
            asseticsdk.logger.error(msg)
        return None
    return dimensions

def launch_assetic_dimension(assetguid,componentguid):
    """
    Open assetic and display the dimension page for the component
    :param assetguid: The layer find selected assets.  Not layer name,layer object
    """
    url = "{0}/Assets/{1}/Component/Component/{2}/CPDimension/Default/".format(
        asseticsdk.client.host,assetguid,componentguid)
    # The code below will open the url in a new tab in a separate thread.
    threading.Thread(target=webbrowser.open, args=(url,0)).start()
    
##This will run the method main() to kickstart it all
if __name__ == "__main__":
    main()
