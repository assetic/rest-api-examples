"""
Create an asset, component and dimensions
(Assetic.AssetsPostComplete.py)
"""
import assetic
from datetime import datetime  #use to generate a unique asset id

# Assetic SDK instance
asseticsdk=assetic.AsseticSDK("c:/users/you/assetic.ini",None,"Debug")
#Asset Tools API
assettools = assetic.AssetTools()
    
def main():
    """
    Create the asset, components, and component dimensions
    Assumes the category does not have autonumber
    """
    ##generate an id based on the current time
    dt = datetime.now()
    assetid = "{0}{1}{2}{3}{4}".format(
        dt.month,dt.day,dt.hour,dt.minute,dt.second)
    ##instantiate the complete asset representation
    complete_asset_obj = assetic.AssetToolsCompleteAssetRepresentation()
    
    #create an instance of the complex asset object
    asset = assetic.Assetic3IntegrationRepresentationsComplexAssetRepresentation()
    ##mandatory fields
    asset.asset_category="Roads"
    asset.status = "Active"
    asset.asset_id = assetid
    asset.asset_name = "RD{0}".format(assetid)

    complete_asset_obj.asset_representation = asset

    ##array list to put component plus dimensions
    components_and_dims = []
    
    #instantiate complete component representation
    component_obj = assetic.AssetToolsComponentRepresentation()
    
    ##create an instance of the component representation
    component = assetic.Assetic3IntegrationRepresentationsComponentRepresentation()
    componentname = "{0}CMP1".format(assetid)
    component.asset_id = assetid
    component.label = "Main Label"  #"Component Name in UI
    component.component_type = "Main"
    component.dimension_unit = "Metre"
    component.network_measure_type = "Length"
    #optional fields
    component.design_life = 50
    component.external_identifier = "Ext{0}".format(componentname)
    component.material_type = "Concrete"  #must exist
    ##Add the component to the components
    component_obj.component_representation = component

    ##create an array for the dimensions to be added to the component
    dimlist = []
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = 75.7
    dim.unit = "Metre"
    dim.record_type = "Info"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 2.5   #will default as 1 if undefined
    dimlist.append(dim)
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = 3
    dim.unit = "Metre"
    dim.record_type = "Subtraction"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 1   #will default as 1 if undefined
    dimlist.append(dim)

    ##Add the dimension array to the component
    component_obj.dimensions = dimlist
    
    ##Add component to the list
    components_and_dims.append(component_obj)

    #instantiate complete component representation
    component_obj = assetic.AssetToolsComponentRepresentation()

    ##Create another component
    componentname = "{0}CMP2".format(assetid)
    component = assetic.Assetic3IntegrationRepresentationsComponentRepresentation()
    component.name = componentname
    component.asset_id = assetid
    component.label = "Formation"
    component.component_type = "Formation"
    component.dimension_unit = "Metre"
    component.network_measure_type = "Length"
    #optional fields
    component.design_life = 50
    component.external_identifier = "Ext{0}".format(componentname)
    #component.material_type = "Concrete"  #must exist
    component_obj.component_representation = component

    ##create an array for the dimensions to be added to the component
    dimlist = []
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = 50
    dim.component_id = componentname
    dim.unit = "Metre"
    dim.record_type = "Addition"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 2   #will default as 1 if undefined
    dimlist.append(dim)
    
    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = 5
    dim.component_id = componentname
    dim.unit = "Metre"
    dim.record_type = "Subtraction"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 3   #will default as 1 if undefined
    dimlist.append(dim)

    ##Add the dimension array to the component
    component_obj.dimensions = dimlist
    
    ##Add component with dims to the list
    components_and_dims.append(component_obj)

    ##add the component & dims array to the complete asset object
    complete_asset_obj.components = components_and_dims

    ##create the complete asset
    response = assettools.create_complete_asset(complete_asset_obj)

    if len(response.components) > 0:
        #get some ID's
        assetguid = response.asset_representation.id
        #first component
        componentguid = response.components[0].component_representation.id
        #first dimension
        dimensionguid = response.components[0].dimensions[0].id
    
def create_asset(assetid,category,name,status):
    """
    Create an asset for the given asset ID and category
    :param assetid: Assetic user friendly asset ID
    :param assetid: asset category name
    :param assetid: asset name
    :param assetid: status - "Active" or "Pending"
    :return: asset response object or None
    """
    asseticsdk.logger.info("Create the asset {0}".format(assetid))

    #create an instance of the complex asset object
    asset = assetic.Assetic3IntegrationRepresentationsComplexAssetRepresentation()
    ##mandatory fields
    asset.asset_category=category
    asset.status = status
    asset.asset_id = assetid
    asset.asset_name = name

    ##now execute the request
    try:
        newasset = assetapi.complex_asset_post(asset)
    except assetic.rest.ApiException as e:
        if e.status == 400:
            msg = "Bad Request. Status {0}, Reason: {1} {2}".format(
                e.status,e.reason,e.body)
            asseticsdk.logger.error(msg)
        else:
            asseticsdk.logger.error("Status {0}, Reason: {1} {2}".format(
                e.status,e.reason,e.body))
        return None
    return newasset
   
def create_component(assetid,componentname):
    """
    Create an component for the given asset ID and component name
    :param assetid: Assetic component GUID or user friendly component ID
    :param componentname: Assetic component name
    :return: component response object or None
    """
    asseticsdk.logger.info("Create the component {0}".format(componentname))

    #create instance of component and set minimum values
    component = assetic.Assetic3IntegrationRepresentationsComponentRepresentation()
    component.name = componentname
    component.asset_id = assetid
    component.label = "Main"
    component.component_type = "Main"
    component.dimension_unit = "Metre"
    component.network_measure_type = "Length"
    #optional fields
    component.design_life = 50
    component.external_identifier = "Ext{0}".format(componentname)
    component.material_type = "Concrete"  #must exist

    try:
        component = componentapi.component_post(component)
    except assetic.rest.ApiException as e:
        if e.status == 400:
            msg = "Component for Component GUID {0} not found".format(assetguid)
            asseticsdk.logger.error(msg)
        else:
            msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
            asseticsdk.logger.error(msg)
        return None
    return component

def create_dimension(componentid,measure):
    """
    Create a dimension for the given component ID
    :param assetid: Assetic component GUID or user friendly component ID
    :param measure: The measure to apply to the dimension
    :return: dimension response object or None
    """
    asseticsdk.logger.info("Create the dimensions for component {0}".format(
        componentid))

    ##Create an instance of the dimension and set minimum fields
    dim = assetic.Assetic3IntegrationRepresentationsComponentDimensionRepresentation()
    dim.network_measure = measure
    dim.unit = "Metre"
    dim.record_type = "Addition"   #could also be "Subtraction" or "Info"
    dim.network_measure_type = "Length"
    ##can also include additional fields
    dim.comments = "Created via API"
    dim.multiplier = 2   #will default as 1 if undefined

    try:
        newdim = componentapi.component_post_dimension(componentid,dim)
    except assetic.rest.ApiException as e:
        if e.status == 404:
            msg = "Dimensions for Component {0} not found".format(assetguid)
            asseticsdk.logger.error(msg)
        else:
            msg = "Status {0}, Reason: {1} {2}".format(e.status,e.reason,e.body)
            asseticsdk.logger.error(msg)
        return None
    return newdim

##This will run the method main() to kickstart it all
if __name__ == "__main__":
    main()
