# Requires Powershell 7
# Tool to bulk upload documents to Brightly Assetic Cloud
# Iterates over folder containing a subfolder per asset
# The folder name is the Asset Id
# All files in the subfolders are uploaded
# Edit the script to set metadata such as document group, category and subcategory

# Get Assetic login. 
# Username is the email used to log into Assetic
# Password is the Assetic password or integration token

# Edit here to set your site URL
# ----------------------------------------------
$assetic_site_uri = "https://xxx.assetic.net"
# ----------------------------------------------

# Edit here to set your username or remove username parameter to allow you
# to be prompted to enter user name each time the script is run
$Cred = Get-Credential -UserName "person@abc.com" -Title "Assetic login"

#check the connection
$Url = "$($assetic_site_uri)/api/v2/version"
try {
  Invoke-RestMethod -Method 'Get' -Uri $url -Authentication Basic -Credential $Cred  
}
catch {
  Write-Host "Error Occurred testing login.  Check URL and credentials"
  Write-Host $_
  return
}

$Url = "$($assetic_site_uri)/api/v2/document"

# iterate the folders
$files = Get-ChildItem "C:\temp\document_uploads" -Recurse
foreach ($f in $files)
{
  if($f.PSIsContainer)
  {
    continue
  }
  $asset =$f.Directory.Name
  $fileUtf8 = Get-Content -path $f.FullName -AsByteStream
  $b64 = [Convert]::ToBase64String($fileUtf8)
  $Body = @{
    ParentType = 'ComplexAsset'
    ParentIdentifier = $asset
    DocumentGroup = 1
    DocumentSubCategory = 2
    DocumentCategory = 3
    FileProperty = @(
      @{
        Name = $f.Name
        FileSize = $fileUtf8.Length
        mimetype = $f.Extension.TrimStart(".")
        filecontent = $b64
      }
    )
  }
  $jsonBody = $Body | ConvertTo-Json
  # upload the document
  try {
    Invoke-RestMethod -Method 'Post' -Uri $url -Authentication Basic -Credential $Cred -Body $jsonBody -ContentType "application/json"  
  }
  catch {
    Write-Host "Error Occurred uploading document $($f.FullName)"
    Write-Host $_
  }
  
}

